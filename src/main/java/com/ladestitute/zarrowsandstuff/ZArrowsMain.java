package com.ladestitute.zarrowsandstuff;

import com.ladestitute.zarrowsandstuff.entities.arrows.*;
import com.ladestitute.zarrowsandstuff.registries.*;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import com.ladestitute.zarrowsandstuff.util.events.ZBlocksHandler;
import com.ladestitute.zarrowsandstuff.util.events.ZMobDropsHandler;
import com.ladestitute.zarrowsandstuff.util.events.ZMobEffectsHandler;
import com.ladestitute.zarrowsandstuff.util.events.ZStaminaHandler;
import com.ladestitute.zarrowsandstuff.util.events.mealeffects.ZMealEffectsHandlerCore;
import com.ladestitute.zarrowsandstuff.util.events.mealeffects.fruit.ZMealEffectsRoastedFruit;
import com.ladestitute.zarrowsandstuff.util.events.mealeffects.fruit.ZMealEffectsSauteedPeppers;
import com.ladestitute.zarrowsandstuff.util.events.mealeffects.fruit.ZMealEffectsSimmeredFruit;
import com.ladestitute.zarrowsandstuff.util.events.mealeffects.meat.ZMealEffectsSpicySteak;
import com.ladestitute.zarrowsandstuff.util.events.mealeffects.meat.meatskewers.ZMealEffectsChillyMeatSkewers;
import com.ladestitute.zarrowsandstuff.util.events.mealeffects.meat.meatskewers.ZMealEffectsMeatSkewers;
import com.ladestitute.zarrowsandstuff.util.events.mealeffects.meat.meatskewers.ZMealEffectsSpicyMeatSkewers;
import com.ladestitute.zarrowsandstuff.util.events.mealeffects.mushroom.ZMealEffectsGlazedMushrooms;
import com.ladestitute.zarrowsandstuff.util.events.mealeffects.mushroom.ZMealEffectsRoastedMushrooms;
import com.ladestitute.zarrowsandstuff.util.events.mealeffects.mushroom.mushroomskewers.ZMealEffectsMushroomSkewers;
import com.ladestitute.zarrowsandstuff.util.events.mealeffects.sweets.ZMealEffectsHoneyItems;
import com.ladestitute.zarrowsandstuff.util.events.mealeffects.sweets.ZMealEffectsHoneyedApples;
import com.ladestitute.zarrowsandstuff.util.events.mealeffects.sweets.ZMealEffectsHoneyedFruit;
import com.ladestitute.zarrowsandstuff.util.events.mealeffects.sweets.cake.ZMealEffectsCake;
import com.ladestitute.zarrowsandstuff.util.events.mealeffects.sweets.pastry.crepes.ZMealEffectsCrepes;
import com.ladestitute.zarrowsandstuff.util.events.mealeffects.sweets.pastry.crepes.ZMealEffectsModifierCrepes;
import com.ladestitute.zarrowsandstuff.util.events.tempsystem.ZColdBiomesHandler;
import com.ladestitute.zarrowsandstuff.util.events.tempsystem.ZHotBiomesHandler;
import com.ladestitute.zarrowsandstuff.world.gen.ZGeneration;
import com.ladestitute.zarrowsandstuff.world.gen.ZOreGen;
import net.minecraft.block.ComposterBlock;
import net.minecraft.block.DispenserBlock;
import net.minecraft.dispenser.IPosition;
import net.minecraft.dispenser.ProjectileDispenseBehavior;
import net.minecraft.entity.merchant.villager.VillagerProfession;
import net.minecraft.entity.merchant.villager.VillagerTrades;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.item.*;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.placement.Placement;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.village.VillagerTradesEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.IForgeRegistry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SuppressWarnings("deprecation")
@Mod(ZArrowsMain.MOD_ID)
@EventBusSubscriber(modid = ZArrowsMain.MOD_ID, bus = Bus.MOD)
public class ZArrowsMain
{
    public static ZArrowsMain instance;
    public static final String NAME = "Z Arrows and Stuff";
    public static final String MOD_ID = "zarrowsandstuff";
    public static final Logger LOGGER = LogManager.getLogger();

    public ZArrowsMain()
    {
        instance = this;

        final IEventBus modEventBus = FMLJavaModLoadingContext.get().getModEventBus();
        MinecraftForge.EVENT_BUS.addListener(this::villagerTrades);
        MinecraftForge.EVENT_BUS.register(new ZMobDropsHandler());
        MinecraftForge.EVENT_BUS.register(new ZBlocksHandler());
        MinecraftForge.EVENT_BUS.register(new ZStaminaHandler());

        //Call this when a player loads, to attach our temperature-capability to the player
        MinecraftForge.EVENT_BUS.register(new ZColdBiomesHandler());
        MinecraftForge.EVENT_BUS.register(new ZHotBiomesHandler());

        MinecraftForge.EVENT_BUS.register(new ZMealEffectsHandlerCore());
        MinecraftForge.EVENT_BUS.register(new ZMealEffectsMeatSkewers());
        MinecraftForge.EVENT_BUS.register(new ZMealEffectsSpicySteak());
        MinecraftForge.EVENT_BUS.register(new ZMealEffectsMushroomSkewers());
        MinecraftForge.EVENT_BUS.register(new ZMealEffectsCake());
        MinecraftForge.EVENT_BUS.register(new ZMealEffectsCrepes());
        MinecraftForge.EVENT_BUS.register(new ZMealEffectsModifierCrepes());
        MinecraftForge.EVENT_BUS.register(new ZMealEffectsHoneyItems());
        MinecraftForge.EVENT_BUS.register(new ZMealEffectsRoastedFruit());
        MinecraftForge.EVENT_BUS.register(new ZMealEffectsRoastedMushrooms());
        MinecraftForge.EVENT_BUS.register(new ZMealEffectsChillyMeatSkewers());
        MinecraftForge.EVENT_BUS.register(new ZMealEffectsSpicyMeatSkewers());
        MinecraftForge.EVENT_BUS.register(new ZMealEffectsGlazedMushrooms());
        MinecraftForge.EVENT_BUS.register(new ZMealEffectsSauteedPeppers());
        MinecraftForge.EVENT_BUS.register(new ZMealEffectsHoneyedApples());
        MinecraftForge.EVENT_BUS.register(new ZMealEffectsHoneyedFruit());
        MinecraftForge.EVENT_BUS.register(new ZMealEffectsSimmeredFruit());
        MinecraftForge.EVENT_BUS.register(new ZMobEffectsHandler());
        MinecraftForge.EVENT_BUS.register(new ZGeneration());

        modEventBus.addListener(this::setup);

        /* Register all of our deferred registries from our list/init classes, which get added to the IEventBus */
        // ParticleList.PARTICLES.register(modEventBus);
        SoundInit.SOUNDS.register(modEventBus);
        EffectInit.EFFECTS.register(modEventBus);
        EffectInit.POTIONS.register(modEventBus);
        FeatureInit.FEATURES.register(modEventBus);
        ItemInit.ITEMS.register(modEventBus);
        FoodInit.FOOD.register(modEventBus);
        BlockInit.BLOCKS.register(modEventBus);
        SpecialBlocksInit.BLOCKS.register(modEventBus);
        EntityInit.ENTITIES.register(modEventBus);
        DecoratorInit.DECORATORS.register(modEventBus);
        MinecraftForge.EVENT_BUS.addListener(EventPriority.HIGH, ZOreGen::generateOres);
    }

//    @Mod.EventBusSubscriber(modid = ZArrowsMain.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
//    public static class WorldGenRegistries {
//
//
//        @SubscribeEvent
//        public static void registerDecorators(RegistryEvent.Register<Placement<?>> event) {
//            DecoratorInit.init();
//        }
//    }

    @SubscribeEvent
    public static void createBlockItems(final RegistryEvent.Register<Item> event) {
        final IForgeRegistry<Item> registry = event.getRegistry();

        BlockInit.BLOCKS.getEntries().stream().map(RegistryObject::get).forEach(block -> {
            final Item.Properties properties = new Item.Properties().tab(ZArrowsMain.BLOCKS_TAB);
            final BlockItem blockItem = new BlockItem(block, properties);
            blockItem.setRegistryName(block.getRegistryName());
            registry.register(blockItem);
        });
    }

    private void villagerTrades(VillagerTradesEvent event)
    {
        if(event.getType() == VillagerProfession.FLETCHER)
        {
            event.getTrades().get(1).add(ZArrowsMain.buildTrade(new ItemStack(Items.EMERALD, 4),
                    new ItemStack(Items.ARROW, 5), 999999999, 25, 0.05F));
            event.getTrades().get(1).add(ZArrowsMain.buildTrade(new ItemStack(Items.EMERALD, 8),
                    new ItemStack(ItemInit.FIRE_ARROW.get(), 10), 999999999, 25, 0.05F));
            event.getTrades().get(1).add(ZArrowsMain.buildTrade(new ItemStack(Items.EMERALD, 8),
                    new ItemStack(ItemInit.ICE_ARROW.get(), 10), 999999999, 25, 0.05F));
            event.getTrades().get(1).add(ZArrowsMain.buildTrade(new ItemStack(Items.EMERALD, 8),
                    new ItemStack(ItemInit.SHOCK_ARROW.get(), 10), 999999999, 25, 0.05F));
            event.getTrades().get(1).add(ZArrowsMain.buildTrade(new ItemStack(Items.EMERALD, 16),
                    new ItemStack(ItemInit.BOMB_ARROW.get(), 20), 999999999, 25, 0.05F));
        }

        if(ZConfigManager.getInstance().addoptionaltrades())
        {
            if(event.getType() == VillagerProfession.FARMER)
            {
                event.getTrades().get(1).add(ZArrowsMain.buildTrade(new ItemStack(Items.EMERALD, 2),
                        new ItemStack(ItemInit.SPICY_PEPPER_SEED.get(), 1), 999999999, 25, 0.05F));
                event.getTrades().get(1).add(ZArrowsMain.buildTrade(new ItemStack(Items.EMERALD, 2),
                        new ItemStack(ItemInit.HYDROMELON_SEED.get(), 1), 999999999, 25, 0.05F));
            }

            if(event.getType() == VillagerProfession.MASON)
            {
                event.getTrades().get(1).add(ZArrowsMain.buildTrade(new ItemStack(Items.EMERALD, 5),
                        new ItemStack(ItemInit.AMBER_EARRINGS.get(), 1), 64, 25, 0.05F));
                event.getTrades().get(1).add(ZArrowsMain.buildTrade(new ItemStack(Items.EMERALD, 5),
                        new ItemStack(ItemInit.OPAL_EARRINGS.get(), 1), 64, 25, 0.05F));
                event.getTrades().get(1).add(ZArrowsMain.buildTrade(new ItemStack(Items.EMERALD, 5),
                        new ItemStack(ItemInit.TOPAZ_EARRINGS.get(), 1), 64, 25, 0.05F));
                event.getTrades().get(1).add(ZArrowsMain.buildTrade(new ItemStack(Items.EMERALD, 3),
                        new ItemStack(ItemInit.RUBY_CIRCLET.get(), 1), 64, 25, 0.05F));
                event.getTrades().get(1).add(ZArrowsMain.buildTrade(new ItemStack(Items.EMERALD, 3),
                        new ItemStack(ItemInit.SAPPHIRE_CIRCLET.get(), 1), 64, 25, 0.05F));
            }

            if(event.getType() == VillagerProfession.LEATHERWORKER)
            {
                event.getTrades().get(1).add(ZArrowsMain.buildTrade(new ItemStack(Items.EMERALD, 10),
                        new ItemStack(ItemInit.SNOWQUILL_HEADDRESS.get(), 1), 64, 25, 0.05F));
                event.getTrades().get(1).add(ZArrowsMain.buildTrade(new ItemStack(Items.EMERALD, 10),
                        new ItemStack(ItemInit.SNOWQUILL_TUNIC.get(), 1), 64, 25, 0.05F));
                event.getTrades().get(1).add(ZArrowsMain.buildTrade(new ItemStack(Items.EMERALD, 10),
                        new ItemStack(ItemInit.SNOWQUILL_TROUSERS.get(), 1), 64, 25, 0.05F));
                event.getTrades().get(1).add(ZArrowsMain.buildTrade(new ItemStack(Items.EMERALD, 10),
                        new ItemStack(ItemInit.SNOWQUILL_BOOTS.get(), 1), 64, 25, 0.05F));
                event.getTrades().get(1).add(ZArrowsMain.buildTrade(new ItemStack(Items.EMERALD, 10),
                        new ItemStack(ItemInit.DESERT_VOE_HEADBAND.get(), 1), 64, 25, 0.05F));
                event.getTrades().get(1).add(ZArrowsMain.buildTrade(new ItemStack(Items.EMERALD, 10),
                        new ItemStack(ItemInit.DESERT_VOE_SPAULDER.get(), 1), 64, 25, 0.05F));
                event.getTrades().get(1).add(ZArrowsMain.buildTrade(new ItemStack(Items.EMERALD, 10),
                        new ItemStack(ItemInit.DESERT_VOE_TROUSERS.get(), 1), 64, 25, 0.05F));
                event.getTrades().get(1).add(ZArrowsMain.buildTrade(new ItemStack(Items.EMERALD, 10),
                        new ItemStack(ItemInit.DESERT_VOE_BOOTS.get(), 1), 64, 25, 0.05F));

            }
        }
    }

    public static VillagerTrades.ITrade buildTrade(ItemStack wanted1, ItemStack given, int tradesUntilDisabled, int xpToVillagr, float priceMultiplier)
    {
        return ZArrowsMain.buildTrade(wanted1, ItemStack.EMPTY, given, tradesUntilDisabled, xpToVillagr, priceMultiplier);
    }

    public static VillagerTrades.ITrade buildTrade(ItemStack wanted1, ItemStack wanted2, ItemStack given, int tradesUntilDisabled, int xpToVillagr, float priceMultiplier)
    {
        return (entity, random) -> new MerchantOffer(wanted1, wanted2, given, tradesUntilDisabled, xpToVillagr, priceMultiplier);
    }

    /* The FMLCommonSetupEvent (FML - Forge Mod Loader) */
    private void setup(final FMLCommonSetupEvent event)
    {



        event.enqueueWork(() -> {
            FeatureInit.Configured.registerConfiguredFeatures();
            ConfiguredFeaturesInit.registerConfiguredFeatures();
            ComposterBlock.COMPOSTABLES.put(ItemInit.SPICY_PEPPER.get(), 0.32F);
            ComposterBlock.COMPOSTABLES.put(ItemInit.HYDROMELON_FRUIT.get(), 0.32F);
            ComposterBlock.COMPOSTABLES.put(BlockInit.HYDROMELON.get(), 0.32F);
            ComposterBlock.COMPOSTABLES.put(ItemInit.SPICY_PEPPER_SEED.get(), 0.30F);
            ComposterBlock.COMPOSTABLES.put(ItemInit.HYDROMELON_SEED.get(), 0.30F);
        });

        //Dispenser behaviors
        DispenserBlock.registerBehavior(ItemInit.FIRE_ARROW.get(), new ProjectileDispenseBehavior() {

            @Override
            protected ProjectileEntity getProjectile(World worldIn, IPosition position, ItemStack stackIn) {
                EntityFireArrow firearrowentity = new EntityFireArrow(worldIn, position.x(), position.y(),
                        position.z());
                if(ZConfigManager.getInstance().allowFireArrowDOT()) {
                    firearrowentity.setSecondsOnFire(600);
                }
                firearrowentity.pickup = AbstractArrowEntity.PickupStatus.DISALLOWED;
                firearrowentity.remove();
                return firearrowentity;
            }
        });
        DispenserBlock.registerBehavior(ItemInit.ICE_ARROW.get(), new ProjectileDispenseBehavior() {

            @Override
            protected ProjectileEntity getProjectile(World worldIn, IPosition position, ItemStack stackIn) {
                EntityIceArrow icearrowentity = new EntityIceArrow(worldIn, position.x(), position.y(),
                        position.z());
                icearrowentity.pickup = AbstractArrowEntity.PickupStatus.DISALLOWED;
                icearrowentity.remove();
                return icearrowentity;
            }
        });
        DispenserBlock.registerBehavior(ItemInit.SHOCK_ARROW.get(), new ProjectileDispenseBehavior() {

            @Override
            protected ProjectileEntity getProjectile(World worldIn, IPosition position, ItemStack stackIn) {
                EntityShockArrow shockarrowentity = new EntityShockArrow(worldIn, position.x(), position.y(),
                        position.z());
                shockarrowentity.pickup = AbstractArrowEntity.PickupStatus.DISALLOWED;
                shockarrowentity.remove();
                return shockarrowentity;
            }
        });
        DispenserBlock.registerBehavior(ItemInit.BOMB_ARROW.get(), new ProjectileDispenseBehavior() {

            @Override
            protected ProjectileEntity getProjectile(World worldIn, IPosition position, ItemStack stackIn) {
                EntityBombArrow bombarrowentity = new EntityBombArrow(worldIn, position.x(), position.y(),
                        position.z());
                bombarrowentity.pickup = AbstractArrowEntity.PickupStatus.DISALLOWED;
                bombarrowentity.remove();
                return bombarrowentity;
            }
        });
        DispenserBlock.registerBehavior(ItemInit.ANCIENT_ARROW.get(), new ProjectileDispenseBehavior() {

            @Override
            protected ProjectileEntity getProjectile(World worldIn, IPosition position, ItemStack stackIn) {
                EntityAncientArrow ancientarrowentity = new EntityAncientArrow(worldIn, position.x(), position.y(),
                        position.z());
                ancientarrowentity.pickup = AbstractArrowEntity.PickupStatus.ALLOWED;
                ancientarrowentity.remove();
                return ancientarrowentity;
            }
        });
        DispenserBlock.registerBehavior(ItemInit.LIGHT_ARROW.get(), new ProjectileDispenseBehavior() {

            @Override
            protected ProjectileEntity getProjectile(World worldIn, IPosition position, ItemStack stackIn) {
                EntityLightArrow lightarrowentity = new EntityLightArrow(worldIn, position.x(), position.y(),
                        position.z());
                lightarrowentity.pickup = AbstractArrowEntity.PickupStatus.DISALLOWED;
                lightarrowentity.remove();
                return lightarrowentity;
            }
        });
    }

    // Custom ItemGroup tab
    public static final ItemGroup BLOCKS_TAB = new ItemGroup("zbows_blocks") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(BlockInit.SNOWSTONE.get());
        }
    };
    public static final ItemGroup ANCIENT_TAB = new ItemGroup("zbows_ancient") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ItemInit.ANCIENT_CORE.get());
        }
    };

    public static final ItemGroup ANCIENTTECH_TAB = new ItemGroup("zbows_ancienttech") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(SpecialBlocksInit.ANCIENT_OVEN.get());
        }
    };

    public static final ItemGroup RESOURCES_TAB = new ItemGroup("zbows_natural") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ItemInit.RUBY.get());
        }
    };

    public static final ItemGroup ACCESSORIES_TAB = new ItemGroup("zbows_accessories") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ItemInit.RUBY_CIRCLET.get());
        }
    };

    public static final ItemGroup ARMOR_TAB = new ItemGroup("zbows_armor") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ItemInit.SNOWQUILL_HEADDRESS.get());
        }
    };

   public static final ItemGroup ARROWS_TAB = new ItemGroup("zbows_arrows") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ItemInit.FIRE_ARROW.get());
        }
    };

    public static final ItemGroup FOOD_TAB = new ItemGroup("zbows_food") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(FoodInit.PLAIN_CREPE.get());
        }
    };
}

