package com.ladestitute.zarrowsandstuff.blocks;

import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import com.ladestitute.zarrowsandstuff.util.KeyboardUtil;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.List;

public class AncientOvenBlock extends Block {
    public static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;

    public AncientOvenBlock(Properties properties) {
        super(properties.lightLevel((state) -> {
            return 10;
        }));
        this.registerDefaultState(this.stateDefinition.any().setValue(FACING, Direction.NORTH));
    }

    @Override
    protected void createBlockStateDefinition(StateContainer.Builder<Block, BlockState> builder) {
        super.createBlockStateDefinition(builder);
        builder.add(FACING);
    }

    @Override
    public BlockState mirror(BlockState state, Mirror mirrorIn) {
        return state.rotate(mirrorIn.getRotation(state.getValue(FACING)));
    }

    @Override
    public BlockState rotate(BlockState state, Rotation rot) {
        return state.setValue(FACING, rot.rotate(state.getValue(FACING)));
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return this.defaultBlockState().setValue(FACING, context.getHorizontalDirection().getOpposite());
    }

    @Override
    public void appendHoverText (ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag
            flagIn){
        tooltip.add(new
                StringTextComponent("An advanced furnace made from ancient wreckage and a partially broken Guidance Stone. Its capability to manufacture weaponry is beyond anything seen in this world."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public ActionResultType use(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        //Our item list

        //Ancient Arrow components
        ItemStack arrow = Items.ARROW.getDefaultInstance();
        Block stone = Blocks.STONE;
        ItemStack screw = ItemInit.ANCIENT_SCREW.get().getDefaultInstance();
        ItemStack shaft = ItemInit.ANCIENT_SHAFT.get().getDefaultInstance();
        ItemStack ancientarrow = ItemInit.ANCIENT_ARROW.get().getDefaultInstance();

        //Essences
        ItemStack raw_fire = ItemInit.RAW_FIRE_ESSENCE.get().getDefaultInstance();
        ItemStack raw_ice = ItemInit.RAW_ICE_ESSENCE.get().getDefaultInstance();
        ItemStack raw_thunder = ItemInit.RAW_THUNDER_ESSENCE.get().getDefaultInstance();
        ItemStack fire = ItemInit.FIRE_ESSENCE.get().getDefaultInstance();
        ItemStack ice = ItemInit.ICE_ESSENCE.get().getDefaultInstance();
        ItemStack thunder = ItemInit.THUNDER_ESSENCE.get().getDefaultInstance();
        ItemStack topazearringsupgraded = ItemInit.UPGRADED_TOPAZ_EARRINGS.get().getDefaultInstance();
        ItemStack rubyupgraded = ItemInit.UPGRADED_RUBY_CIRCLET.get().getDefaultInstance();
        ItemStack sapphireupgraded = ItemInit.UPGRADED_SAPPHIRE_CIRCLET.get().getDefaultInstance();

        ItemStack handstack = player.getItemBySlot(EquipmentSlotType.MAINHAND);

        if(ZConfigManager.getInstance().experimentalFeatures()) {
            if (player.inventory.contains(raw_fire) && player.experienceLevel >= 1)
                for (ItemStack raw_fire_essence : player.inventory.items)
                    if (player.hasItemInSlot(EquipmentSlotType.MAINHAND) &&
                            handstack.getItem() == ItemInit.RAW_FIRE_ESSENCE.get() && raw_fire_essence.getItem() == ItemInit.RAW_FIRE_ESSENCE.get()) {
                        player.displayClientMessage(ITextComponent.nullToEmpty("Refine a fire essence? Hold Enter and right-click the block again to confirm."), true);
                        if (KeyboardUtil.isHoldingR.isDown()) {
                            raw_fire_essence.shrink(1);
                            ItemHandlerHelper.giveItemToPlayer(player, fire);
                            player.giveExperienceLevels(-1);
                            break;
                        }
                    }

            if (player.inventory.contains(raw_ice) && player.experienceLevel >= 1)
                for (ItemStack raw_ice_essence : player.inventory.items)
                    if (player.hasItemInSlot(EquipmentSlotType.MAINHAND) &&
                            handstack.getItem() == ItemInit.RAW_ICE_ESSENCE.get() && raw_ice_essence.getItem() == ItemInit.RAW_ICE_ESSENCE.get()) {
                        player.displayClientMessage(ITextComponent.nullToEmpty("Refine an ice essence? Hold Enter and right-click the block again to confirm."), true);
                        if (KeyboardUtil.isHoldingR.isDown()) {
                            raw_ice_essence.shrink(1);
                            ItemHandlerHelper.giveItemToPlayer(player, ice);
                            player.giveExperienceLevels(-1);
                            break;
                        }
                    }

            if (player.inventory.contains(raw_thunder) && player.experienceLevel >= 1)
                for (ItemStack raw_thunder_essence : player.inventory.items)
                    if (player.hasItemInSlot(EquipmentSlotType.MAINHAND) &&
                            handstack.getItem() == ItemInit.RAW_THUNDER_ESSENCE.get() && raw_thunder_essence.getItem() == ItemInit.RAW_THUNDER_ESSENCE.get()) {
                        player.displayClientMessage(ITextComponent.nullToEmpty("Refine a thunder essence? Hold Enter and right-click the block again to confirm."), true);
                        if (KeyboardUtil.isHoldingR.isDown()) {
                            raw_thunder_essence.shrink(1);
                            ItemHandlerHelper.giveItemToPlayer(player, thunder);
                            player.giveExperienceLevels(-1);
                            break;
                        }
                    }

            if (player.inventory.contains(thunder) && player.experienceLevel >= 1)
                for (ItemStack thunder1 : player.inventory.items)
                    if (player.hasItemInSlot(EquipmentSlotType.MAINHAND) &&
                            handstack.getItem() == ItemInit.TOPAZ_EARRINGS.get() && thunder1.getItem() == ItemInit.THUNDER_ESSENCE.get()) {
                        player.displayClientMessage(ITextComponent.nullToEmpty("Upgrade Topaz Earrings? Hold Enter and right-click the block again to confirm."), true);
                        if (KeyboardUtil.isHoldingR.isDown()) {
                            for (ItemStack topaz : player.inventory.items)
                                if (topaz.getItem() == ItemInit.TOPAZ_EARRINGS.get())
                                {
                                    topaz.shrink(1);
                                }
                            for (ItemStack material : player.inventory.items)
                                if (material.getItem() == ItemInit.THUNDER_ESSENCE.get())
                                {
                                    material.shrink(1);
                                }
                            ItemHandlerHelper.giveItemToPlayer(player, topazearringsupgraded);
                            player.giveExperienceLevels(-1);
                            break;
                        }
                    }

            if (player.inventory.contains(fire) && player.experienceLevel >= 1)
                for (ItemStack check : player.inventory.items)
                    if (player.hasItemInSlot(EquipmentSlotType.MAINHAND) &&
                            handstack.getItem() == ItemInit.RUBY_CIRCLET.get() && check.getItem() == ItemInit.FIRE_ESSENCE.get()) {
                        player.displayClientMessage(ITextComponent.nullToEmpty("Upgrade Ruby Circlet? Hold Enter and right-click the block again to confirm."), true);
                        if (KeyboardUtil.isHoldingR.isDown()) {
                            for (ItemStack upgrade : player.inventory.items)
                                if (upgrade.getItem() == ItemInit.RUBY_CIRCLET.get())
                                {
                                    upgrade.shrink(1);
                                }
                            for (ItemStack material : player.inventory.items)
                                if (material.getItem() == ItemInit.FIRE_ESSENCE.get())
                                {
                                    material.shrink(1);
                                }
                            ItemHandlerHelper.giveItemToPlayer(player, rubyupgraded);
                            player.giveExperienceLevels(-1);
                            break;
                        }
                    }

            if (player.inventory.contains(ice) && player.experienceLevel >= 1)
                for (ItemStack check : player.inventory.items)
                    if (player.hasItemInSlot(EquipmentSlotType.MAINHAND) &&
                            handstack.getItem() == ItemInit.SAPPHIRE_CIRCLET.get() && check.getItem() == ItemInit.ICE_ESSENCE.get()) {
                        player.displayClientMessage(ITextComponent.nullToEmpty("Upgrade Sapphire Circlet? Hold Enter and right-click the block again to confirm."), true);
                        if (KeyboardUtil.isHoldingR.isDown()) {
                            for (ItemStack upgrade : player.inventory.items)
                                if (upgrade.getItem() == ItemInit.SAPPHIRE_CIRCLET.get())
                                {
                                    upgrade.shrink(1);
                                }
                            for (ItemStack material : player.inventory.items)
                                if (material.getItem() == ItemInit.ICE_ESSENCE.get())
                                {
                                    material.shrink(1);
                                }
                            ItemHandlerHelper.giveItemToPlayer(player, sapphireupgraded);
                            player.giveExperienceLevels(-1);
                            break;
                        }
                    }
        }

        if (player.hasItemInSlot(EquipmentSlotType.MAINHAND) &&
                handstack.getItem() == Items.ARROW && player.inventory.contains(arrow) && player.inventory.contains(shaft) &&
                player.inventory.contains(screw) && player.experienceLevel >= 2)
        {
            player.displayClientMessage(ITextComponent.nullToEmpty("Craft an ancient arrow? Hold Enter and right-click the block again to confirm."), true);

            if(KeyboardUtil.isHoldingR.isDown())
            {
                //Check for the screws
                for (ItemStack screws : player.inventory.items)
                    if (screws.getItem() == ItemInit.ANCIENT_SCREW.get())
                    {
                        screws.shrink(1);
                    }
                //Check for the arrows
                for (ItemStack arrows : player.inventory.items)
                    if (arrows.getItem() == Items.ARROW)
                    {
                        arrows.shrink(1);
                    }
                //Check for the shafts
                for (ItemStack shafts : player.inventory.items)
                    if (shafts.getItem() == ItemInit.ANCIENT_SHAFT.get())
                    {
                        shafts.shrink(1);
                        break;
                    }
                player.giveExperienceLevels(-2);
                ItemHandlerHelper.giveItemToPlayer(player, ancientarrow);

            }
                }
        //Check if the player does not meet requirements
        if (player.experienceLevel == 0)
        {
            player.displayClientMessage(ITextComponent.nullToEmpty("You do not have the required experience cost."), true);
        }
                return ActionResultType.SUCCESS;

            }
        }

