package com.ladestitute.zarrowsandstuff.blocks.ancient;

import net.minecraft.block.FallingBlock;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.IBlockReader;

import java.util.List;

public class AncientWreckageDesertBlock extends FallingBlock {

    public AncientWreckageDesertBlock(Properties properties) {
        super(properties);
    }

    @Override
    public void appendHoverText(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("Ancient wreckage of a long dead guardian, it has decayed beyond recognition"));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}
