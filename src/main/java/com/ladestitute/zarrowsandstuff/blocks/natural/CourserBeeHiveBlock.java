package com.ladestitute.zarrowsandstuff.blocks.natural;

import com.ladestitute.zarrowsandstuff.blocks.natural.BaseHorizontalBlock;
import com.ladestitute.zarrowsandstuff.entities.mob.CourserBeeEntity;
import com.ladestitute.zarrowsandstuff.registries.EntityInit;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.state.properties.SlabType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.List;
import java.util.stream.Stream;

public class CourserBeeHiveBlock extends BaseHorizontalBlock {
    public static final BooleanProperty WATERLOGGED = BlockStateProperties.WATERLOGGED;

    private static final VoxelShape SHAPE = Stream
            .of(Block.box(6.5, 14.7, 7.5, 9.5, 15.7, 10.5),
                    Block.box(5.5, 13.7, 6.5, 10.5, 14.7, 11.5),
                    Block.box(4.5, 11.7, 5.5, 11.5, 13.7, 12.5),
                    Block.box(6.5, 10.7, 10.5, 9.5, 11.7, 11.5),
                    Block.box(6.5, 10.7, 6.5, 9.5, 11.7, 7.5),
                    Block.box(9.5, 10.7, 6.5, 10.5, 11.7, 11.5),
                    Block.box(5.5, 10.7, 6.5, 6.5, 11.7, 11.5),
                    Block.box(8.5, 10.7, 7.5, 9.5, 11.7, 8.5),
                    Block.box(7.5, 10.7, 7.5, 8.5, 11.7, 8.5),
                    Block.box(6.5, 10.7, 8.5, 7.5, 11.7, 9.5),
                    Block.box(6.5, 10.7, 9.5, 7.5, 11.7, 10.5),
                    Block.box(7.5, 10.7, 9.5, 8.5, 11.7, 10.5),
                    Block.box(7.5, 10.7, 8.5, 8.5, 11.7, 9.5),
                    Block.box(8.5, 10.7, 8.5, 9.5, 11.7, 9.5),
                    Block.box(8.5, 10.7, 9.5, 9.5, 11.7, 10.5),
                    Block.box(6.5, 10.7, 7.5, 7.5, 11.7, 8.5))
            .reduce((v1, v2) -> {
                return VoxelShapes.join(v1, v2, IBooleanFunction.OR);
            }).get();


    public CourserBeeHiveBlock(AbstractBlock.Properties properties) {
        super(properties);
        runCalculation(SHAPE);
        this.registerDefaultState(this.defaultBlockState().setValue(WATERLOGGED, Boolean.FALSE));
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return SHAPES.get(this).get(state.getValue(HORIZONTAL_FACING));
    }

    @Override
    public void appendHoverText (ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag
            flagIn){
        tooltip.add(new StringTextComponent("This deposit contains a good deal of ore. Breaking the rock will yield rock salt, flint, and other materials of varying value."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    protected void createBlockStateDefinition(StateContainer.Builder<Block, BlockState> builder) {
        super.createBlockStateDefinition(builder);
        builder.add(WATERLOGGED);
    }

    @Override
    public BlockState updateShape(BlockState p_196271_1_, Direction p_196271_2_, BlockState p_196271_3_, IWorld p_196271_4_, BlockPos p_196271_5_, BlockPos p_196271_6_) {
        if (p_196271_1_.getValue(WATERLOGGED)) {
            p_196271_4_.getLiquidTicks().scheduleTick(p_196271_5_, Fluids.WATER, Fluids.WATER.getTickDelay(p_196271_4_));
        }

        return super.updateShape(p_196271_1_, p_196271_2_, p_196271_3_, p_196271_4_, p_196271_5_, p_196271_6_);
    }

    @Override
    public FluidState getFluidState(BlockState p_204507_1_) {
        return p_204507_1_.getValue(WATERLOGGED) ? Fluids.WATER.getSource(false) : super.getFluidState(p_204507_1_);
    }

    @Override
    public boolean removedByPlayer(BlockState state, World world, BlockPos pos, PlayerEntity player, boolean willHarvest, FluidState fluid) {
        ItemHandlerHelper.giveItemToPlayer(player, ItemInit.COURSER_BEE_HONEY.get().getDefaultInstance());
        CourserBeeEntity courser_bee = new CourserBeeEntity(EntityInit.COURSER_BEE.get(), world);
        courser_bee.setPos(pos.getX(), pos.getY(), pos.getZ());
        world.addFreshEntity(courser_bee);
        return super.removedByPlayer(state, world, pos, player, willHarvest, fluid);
    }
}

