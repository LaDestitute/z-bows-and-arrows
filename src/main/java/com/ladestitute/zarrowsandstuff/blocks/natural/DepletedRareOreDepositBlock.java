package com.ladestitute.zarrowsandstuff.blocks.natural;
import com.ladestitute.zarrowsandstuff.registries.BlockInit;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.server.ServerWorld;

import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

public class DepletedRareOreDepositBlock extends BaseHorizontalBlock {

    private static final VoxelShape SHAPE = Stream
            .of(Block.box(5, -0.5, 4.125, 11, 3.5, 10.125),
                    Block.box(3, 3.5, 5.125, 12, 7.5, 11.125),
                    Block.box(5, 6.5, 7.125, 9, 10.5, 12.125))
            .reduce((v1, v2) -> {
                return VoxelShapes.join(v1, v2, IBooleanFunction.OR);
            }).get();

    public DepletedRareOreDepositBlock(int i, AbstractBlock.Properties properties) {
        super(properties.randomTicks());
        runCalculation(SHAPE);
    }




    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return SHAPES.get(this).get(state.getValue(HORIZONTAL_FACING));
    }

    @Override
    public void randomTick(BlockState state, ServerWorld worldIn, BlockPos pos, Random random) {
        worldIn.setBlockAndUpdate(pos, BlockInit.RARE_ORE_DEPOSIT.get().defaultBlockState());
        super.randomTick(state, worldIn, pos, random);
    }

    @Override
    public void appendHoverText (ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag
            flagIn){
        tooltip.add(new StringTextComponent("This deposit used to contain a good deal of ore."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}

