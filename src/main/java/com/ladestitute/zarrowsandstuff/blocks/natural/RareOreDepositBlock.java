package com.ladestitute.zarrowsandstuff.blocks.natural;

import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import com.ladestitute.zarrowsandstuff.registries.SoundInit;
import com.ladestitute.zarrowsandstuff.registries.SpecialBlocksInit;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.FluidState;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

public class RareOreDepositBlock extends BaseHorizontalBlock {

    private static final VoxelShape SHAPE = Stream
            .of(Block.box(5, -0.5, 4.125, 11, 3.5, 10.125),
                    Block.box(3, 3.5, 5.125, 12, 7.5, 11.125),
                    Block.box(5, 6.5, 7.125, 9, 10.5, 12.125))
            .reduce((v1, v2) -> {
                return VoxelShapes.join(v1, v2, IBooleanFunction.OR);
            }).get();


    public RareOreDepositBlock(AbstractBlock.Properties properties) {
        super(properties);
        runCalculation(SHAPE);
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return SHAPES.get(this).get(state.getValue(HORIZONTAL_FACING));
    }

    @Override
    public boolean removedByPlayer(BlockState state, World world, BlockPos pos, PlayerEntity player, boolean willHarvest, FluidState fluid) {
        Random rand1 = new Random();
        int oredrop = rand1.nextInt(11);
        ItemStack rubystack = new ItemStack(ItemInit.RUBY.get());
        ItemEntity ruby = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), rubystack);
        ItemStack sapphirestack = new ItemStack(ItemInit.SAPPHIRE.get());
        ItemEntity sapphire = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), sapphirestack);
        ItemStack topazstack = new ItemStack(ItemInit.TOPAZ.get());
        ItemEntity topaz = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), topazstack);
        ItemStack opalstack = new ItemStack(ItemInit.OPAL.get());
        ItemEntity opal = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), opalstack);
        ItemStack amberstack = new ItemStack(ItemInit.AMBER.get());
        ItemEntity amber = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), amberstack);
        ItemStack flintstack = new ItemStack(Items.FLINT);
        ItemEntity flint = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), flintstack);
        ItemStack saltstack = new ItemStack(ItemInit.ROCK_SALT.get());
        ItemEntity salt = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), saltstack);
        ItemStack diamondstack = new ItemStack(Items.DIAMOND);
        ItemEntity diamond = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), diamondstack);
        world.playSound(null, player.getX(), player.getY(), player.getZ(),
                SoundInit.MINERALBREAK.get(), SoundCategory.PLAYERS, 1.5f, 1f);

        if(oredrop == 0)
        {
            world.addFreshEntity(ruby);
        }
        if(oredrop == 1)
        {
            world.addFreshEntity(sapphire);
        }
        if(oredrop == 2)
        {
            world.addFreshEntity(topaz);
        }
        if(oredrop == 3)
        {
            world.addFreshEntity(opal);
        }
        if(oredrop == 4)
        {
            world.addFreshEntity(amber);
        }
        if(oredrop == 5)
        {
            world.addFreshEntity(ruby);
        }
        if(oredrop == 6)
        {
            world.addFreshEntity(sapphire);
        }
        if(oredrop == 7)
        {
            world.addFreshEntity(topaz);
        }
        if(oredrop == 8)
        {
            world.addFreshEntity(opal);
        }
        if(oredrop == 9)
        {
            world.addFreshEntity(amber);
        }
        if(oredrop == 10)
        {
            world.addFreshEntity(diamond);
        }

        if(ZConfigManager.getInstance().renewableOreDeposits() == true)
        {    world.setBlockAndUpdate(pos, SpecialBlocksInit.DEPLETED_RARE_ORE_DEPOSIT.get().defaultBlockState());
        }
        else   world.setBlockAndUpdate(pos, Blocks.AIR.defaultBlockState());
        return false;
    }

    @Override
    public void appendHoverText (ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag
            flagIn){
        tooltip.add(new StringTextComponent("This deposit contains a good deal of precious ore and the occasional rare mineral, like ruby and sapphire. Break it open to see what it has to offer!"));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}

