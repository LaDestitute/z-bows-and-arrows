package com.ladestitute.zarrowsandstuff.blocks.natural;

import net.minecraft.block.Block;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.IBlockReader;

import java.util.List;

public class SnowstoneBlock extends Block {
    public SnowstoneBlock(Properties properties) {
        super(properties);
    }

    @Override
    public void appendHoverText (ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag
            flagIn){
        tooltip.add(new StringTextComponent("Compressed snow found under the top-snow layers from Hebra Mountain. Its hard as ice."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}
