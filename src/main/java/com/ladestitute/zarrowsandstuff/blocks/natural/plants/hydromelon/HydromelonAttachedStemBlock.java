package com.ladestitute.zarrowsandstuff.blocks.natural.plants.hydromelon;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.ladestitute.zarrowsandstuff.registries.BlockInit;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import net.minecraft.block.*;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.util.Direction;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;

import java.util.Map;

//Code copied and tweaked from the vanilla melon classes
public class HydromelonAttachedStemBlock extends AttachedStemBlock {
        public static final DirectionProperty FACING = HorizontalBlock.FACING;
        private final StemGrownBlock grownFruit;
        private static final Map<Direction, VoxelShape> SHAPES = Maps.newEnumMap(ImmutableMap.of(Direction.SOUTH, Block.box(6.0D, 0.0D, 6.0D, 10.0D, 10.0D, 16.0D), Direction.WEST, Block.box(0.0D, 0.0D, 6.0D, 10.0D, 10.0D, 10.0D), Direction.NORTH, Block.box(6.0D, 0.0D, 0.0D, 10.0D, 10.0D, 10.0D), Direction.EAST, Block.box(6.0D, 0.0D, 6.0D, 16.0D, 10.0D, 10.0D)));

        public HydromelonAttachedStemBlock(StemGrownBlock grownFruit, Properties properties) {
            super(grownFruit, properties);
            this.registerDefaultState(this.stateDefinition.any().setValue(FACING, Direction.NORTH));
            this.grownFruit = grownFruit;
        }

        public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
            return SHAPES.get(state.getValue(FACING));
        }

        /**
         * Update the provided state given the provided neighbor facing and neighbor state, returning a new state.
         * For example, fences make their connections to the passed in state if possible, and wet concrete powder immediately
         * returns its solidified counterpart.
         * Note that this method should ideally consider only the specific face passed in.
         */
        public BlockState updateShape(BlockState stateIn, Direction facing, BlockState facingState, IWorld worldIn, BlockPos currentPos, BlockPos facingPos) {
            return !facingState.is(this.grownFruit) && facing == stateIn.getValue(FACING) ? this.grownFruit.getStem().defaultBlockState().setValue(StemBlock.AGE, Integer.valueOf(7)) : super.updateShape(stateIn, facing, facingState, worldIn, currentPos, facingPos);
        }

    protected boolean isValidFarmGround(BlockState state, IBlockReader worldIn, BlockPos pos) {
        return state.is(Blocks.FARMLAND);
    }

    protected boolean mayPlaceOn(BlockState state, IBlockReader worldIn, BlockPos pos) {
        return state.is(Blocks.GRASS);
    }

    protected boolean isValidSandGround(BlockState state, IBlockReader worldIn, BlockPos pos) {
        return state.is(Blocks.SAND);
    }

        protected Item getSeedItem() {
                return this.grownFruit == BlockInit.HYDROMELON.get() ? ItemInit.HYDROMELON_SEED.get() : Items.AIR;
            }

        public ItemStack getCloneItemStack(IBlockReader worldIn, BlockPos pos, BlockState state) {
            return new ItemStack(this.getSeedItem());
        }

        /**
         * Returns the blockstate with the given rotation from the passed blockstate. If inapplicable, returns the passed
         * blockstate.
         * fine.
         */
        public BlockState rotate(BlockState state, Rotation rot) {
            return state.setValue(FACING, rot.rotate(state.getValue(FACING)));
        }

        /**
         * Returns the blockstate with the given mirror of the passed blockstate. If inapplicable, returns the passed
         * blockstate.
         */
        public BlockState mirror(BlockState state, Mirror mirrorIn) {
            return state.rotate(mirrorIn.getRotation(state.getValue(FACING)));
        }

        protected void createBlockStateDefinition(StateContainer.Builder<Block, BlockState> builder) {
            builder.add(FACING);
        }
}
