package com.ladestitute.zarrowsandstuff.blocks.natural.plants.hydromelon;

import com.ladestitute.zarrowsandstuff.registries.SpecialBlocksInit;
import net.minecraft.block.AttachedStemBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.StemBlock;
import net.minecraft.block.StemGrownBlock;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootContext;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.IBlockReader;

import java.util.List;

//Code copied and tweaked from the vanilla melon classes
public class HydromelonBlock extends StemGrownBlock {
    public HydromelonBlock(Properties properties) {
        super(properties);
    }

    public StemBlock getStem() {
        return (StemBlock) SpecialBlocksInit.HYDROMELON_STEM.get();
    }

    public AttachedStemBlock getAttachedStem() {
        return (AttachedStemBlock) SpecialBlocksInit.HATTACHED_STEM.get();
    }

    @Override
    public List<ItemStack> getDrops(BlockState state, LootContext.Builder builder) {
        return super.getDrops(state, builder);
    }

    @Override
    public void appendHoverText(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag
            flagIn) {
        tooltip.add(new StringTextComponent("This resilient fruit can flourish even in the heat of the desert. The hydrating liquid inside provides a cooling effect that, when cooked, increases your heat resistance."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}
