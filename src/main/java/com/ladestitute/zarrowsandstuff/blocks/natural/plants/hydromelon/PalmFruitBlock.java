package com.ladestitute.zarrowsandstuff.blocks.natural.plants;

import com.ladestitute.zarrowsandstuff.registries.BlockInit;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import com.ladestitute.zarrowsandstuff.registries.SoundInit;
import com.ladestitute.zarrowsandstuff.registries.SpecialBlocksInit;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.block.*;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.FluidState;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.pathfinding.PathType;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

import javax.annotation.Nullable;
import java.util.Random;

public class PalmFruitBlock extends HorizontalBlock implements IGrowable {
    public static final IntegerProperty AGE = BlockStateProperties.AGE_2;
    protected static final VoxelShape[] EAST_AABB = new VoxelShape[]{
            Block.box(5.5, 7.75, 5.5, 10.5, 12.75, 10.5),
            Block.box(7.9, 12.75, 7.5, 7.9, 30.45, 8.5),
            Block.box(7.4, 12.75, 8, 8.4, 30.45, 8)
            };
    protected static final VoxelShape[] WEST_AABB = new VoxelShape[]{
            Block.box(5.5, 7.75, 5.5, 10.5, 12.75, 10.5),
            Block.box(7.9, 12.75, 7.5, 7.9, 30.45, 8.5),
            Block.box(7.4, 12.75, 8, 8.4, 30.45, 8)};
    protected static final VoxelShape[] NORTH_AABB = new VoxelShape[]{
            Block.box(5.5, 7.75, 5.5, 10.5, 12.75, 10.5),
            Block.box(7.9, 12.75, 7.5, 7.9, 30.45, 8.5),
            Block.box(7.4, 12.75, 8, 8.4, 30.45, 8)};
    protected static final VoxelShape[] SOUTH_AABB = new VoxelShape[]{
            Block.box(5.5, 7.75, 5.5, 10.5, 12.75, 10.5),
            Block.box(7.9, 12.75, 7.5, 7.9, 30.45, 8.5),
            Block.box(7.4, 12.75, 8, 8.4, 30.45, 8)};

//    protected static final VoxelShape[] EAST_AABB = new VoxelShape[]{
//    Block.box(11.0D, 7.0D, 6.0D, 15.0D, 12.0D, 10.0D), Block.box(9.0D, 5.0D, 5.0D, 15.0D, 12.0D, 11.0D),
//    Block.box(7.0D, 3.0D, 4.0D, 15.0D, 12.0D, 12.0D)};
//    protected static final VoxelShape[] WEST_AABB = new VoxelShape[]{
//    Block.box(1.0D, 7.0D, 6.0D, 5.0D, 12.0D, 10.0D), Block.box(1.0D, 5.0D, 5.0D, 7.0D, 12.0D, 11.0D),
//    Block.box(1.0D, 3.0D, 4.0D, 9.0D, 12.0D, 12.0D)};
//    protected static final VoxelShape[] NORTH_AABB = new VoxelShape[]{
//    Block.box(6.0D, 7.0D, 1.0D, 10.0D, 12.0D, 5.0D), Block.box(5.0D, 5.0D, 1.0D, 11.0D, 12.0D, 7.0D),
//    Block.box(4.0D, 3.0D, 1.0D, 12.0D, 12.0D, 9.0D)};
//    protected static final VoxelShape[] SOUTH_AABB = new VoxelShape[]{
//    Block.box(6.0D, 7.0D, 11.0D, 10.0D, 12.0D, 15.0D), Block.box(5.0D, 5.0D, 9.0D, 11.0D, 12.0D, 15.0D),
//    Block.box(4.0D, 3.0D, 7.0D, 12.0D, 12.0D, 15.0D)};


    public PalmFruitBlock(AbstractBlock.Properties properties) {
        super(properties);
        this.registerDefaultState(this.stateDefinition.any().setValue(FACING, Direction.NORTH).setValue(AGE, Integer.valueOf(0)));
    }

    @Override
    public boolean isRandomlyTicking(BlockState p_149653_1_) {
        return p_149653_1_.getValue(AGE) < 2;
    }

    @Override
    public void randomTick(BlockState p_225542_1_, ServerWorld p_225542_2_, BlockPos p_225542_3_, Random p_225542_4_) {
        if (true) {
            int i = p_225542_1_.getValue(AGE);
            if (i < 2 && net.minecraftforge.common.ForgeHooks.onCropsGrowPre(p_225542_2_, p_225542_3_, p_225542_1_, p_225542_2_.random.nextInt(5) == 0)) {
                p_225542_2_.setBlock(p_225542_3_, p_225542_1_.setValue(AGE, Integer.valueOf(i + 1)), 2);
                net.minecraftforge.common.ForgeHooks.onCropsGrowPost(p_225542_2_, p_225542_3_, p_225542_1_);
            }
        }

    }

    @Override
    public boolean canSurvive(BlockState p_196260_1_, IWorldReader p_196260_2_, BlockPos p_196260_3_) {
        Block block = p_196260_2_.getBlockState(p_196260_3_.relative(p_196260_1_.getValue(FACING))).getBlock();
        return block.is(BlockInit.PALM_LOG.get())||block.is(BlockInit.MIGHTY_BANANAS_LEAVES.get());
    }

    @Override
    public VoxelShape getShape(BlockState p_220053_1_, IBlockReader p_220053_2_, BlockPos p_220053_3_, ISelectionContext p_220053_4_) {
        int i = p_220053_1_.getValue(AGE);
        switch((Direction)p_220053_1_.getValue(FACING)) {
            case SOUTH:
                return SOUTH_AABB[i];
            case NORTH:
            default:
                return NORTH_AABB[i];
            case WEST:
                return WEST_AABB[i];
            case EAST:
                return EAST_AABB[i];
        }
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockItemUseContext p_196258_1_) {
//        BlockState blockstate = this.defaultBlockState();
//        IWorldReader iworldreader = p_196258_1_.getLevel();
//        BlockPos blockpos = p_196258_1_.getClickedPos();
//
//        for(Direction direction : p_196258_1_.getNearestLookingDirections()) {
//            if (direction.getAxis().isHorizontal()) {
//                blockstate = blockstate.setValue(FACING, direction.getOpposite());
//                if (blockstate.canSurvive(iworldreader, blockpos)) {
//                    return blockstate;
//                }
//            }
//        }
        return this.defaultBlockState().setValue(FACING, p_196258_1_.getHorizontalDirection());
    }

    @Override
    public BlockState updateShape(BlockState p_196271_1_, Direction p_196271_2_, BlockState p_196271_3_, IWorld p_196271_4_, BlockPos p_196271_5_, BlockPos p_196271_6_) {
        return p_196271_2_ == p_196271_1_.getValue(FACING) && !p_196271_1_.canSurvive(p_196271_4_, p_196271_5_) ? Blocks.AIR.defaultBlockState() : super.updateShape(p_196271_1_, p_196271_2_, p_196271_3_, p_196271_4_, p_196271_5_, p_196271_6_);
    }

    @Override
    public boolean isValidBonemealTarget(IBlockReader p_176473_1_, BlockPos p_176473_2_, BlockState p_176473_3_, boolean p_176473_4_) {
        return p_176473_3_.getValue(AGE) < 2;
    }

    @Override
    public boolean isBonemealSuccess(World p_180670_1_, Random p_180670_2_, BlockPos p_180670_3_, BlockState p_180670_4_) {
        return true;
    }

    @Override
    public void performBonemeal(ServerWorld p_225535_1_, Random p_225535_2_, BlockPos p_225535_3_, BlockState p_225535_4_) {
        p_225535_1_.setBlock(p_225535_3_, p_225535_4_.setValue(AGE, Integer.valueOf(p_225535_4_.getValue(AGE) + 1)), 2);
    }

    @Override
    protected void createBlockStateDefinition(StateContainer.Builder<Block, BlockState> p_206840_1_) {
        p_206840_1_.add(FACING, AGE);
    }

    @Override
    public boolean isPathfindable(BlockState p_196266_1_, IBlockReader p_196266_2_, BlockPos p_196266_3_, PathType p_196266_4_) {
        return false;
    }

    @Override
    public boolean removedByPlayer(BlockState state, World world, BlockPos pos, PlayerEntity player, boolean willHarvest, FluidState fluid) {
        ItemStack fruitstack = new ItemStack(ItemInit.PALM_FRUIT.get());
        ItemEntity fruit = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), fruitstack);
//        world.playSound(null, player.getX(), player.getY(), player.getZ(),
//                SoundInit.MINERALBREAK.get(), SoundCategory.PLAYERS, 1.5f, 1f);

            world.addFreshEntity(fruit);

        return false;
    }
}
