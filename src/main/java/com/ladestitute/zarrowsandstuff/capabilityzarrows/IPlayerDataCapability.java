package com.ladestitute.zarrowsandstuff.capabilityzarrows;

public interface IPlayerDataCapability {
    //Pretty simple, just an interface with an integer and some methods
    int getColdResistCooldown();
    int getHeatResistCooldown();
    int getFoodCooldown();
    float getStamina();
    float getMaxStamina();

    void setColdResistCooldown(int coldresistcooldown);

    void addColdResistCooldown(int coldresistcooldown);

    void subtractColdResistCooldown(int coldresistcooldown);

    void setHeatResistCooldown(int heatresistcooldown);

    void addHeatResistCooldown(int heatresistcooldown);

    void subtractHeatResistCooldown(int heatresistcooldown);

    void setFoodCooldown(int foodcooldown);

    void addFoodCooldown(int foodcooldown);

    void subtractFoodCooldown(int foodcooldown);

    void setStamina(float stamina);

    void addStamina(float stamina);

    void subtractStamina(float stamina);

    void setMaxStamina(float maxstamina);

    void addMaxStamina(float maxstamina);

    void subtractMaxStamina(float maxstamina);

}
