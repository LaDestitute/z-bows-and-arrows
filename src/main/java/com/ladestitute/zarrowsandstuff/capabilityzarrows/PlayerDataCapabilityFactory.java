package com.ladestitute.zarrowsandstuff.capabilityzarrows;

public class PlayerDataCapabilityFactory implements IPlayerDataCapability {

    private int coldresistcooldown = 0;
    private int heatresistcooldown = 0;
    private int foodcooldown = 0;
    private float stamina = 20.0F;
    private float maxstamina = 20.0F;

    public PlayerDataCapabilityFactory() {
    }

    //Get our integer
    @Override
    public int getColdResistCooldown() {
        return coldresistcooldown;
    }

    //Set the value to a specified amount
    @Override
    public void setColdResistCooldown(int coldresistcooldown) {
        this.coldresistcooldown = coldresistcooldown;
    }

    //Add to the value
    @Override
    public void addColdResistCooldown(int coldresistcooldown) {
        this.coldresistcooldown += coldresistcooldown;
    }

    //Subtract from the value
    @Override
    public void subtractColdResistCooldown(int coldresistcooldown) {
        this.coldresistcooldown -= coldresistcooldown;
    }

    //Get our integer
    @Override
    public int getHeatResistCooldown() {
        return heatresistcooldown;
    }

    //Set the value to a specified amount
    @Override
    public void setHeatResistCooldown(int heatresistcooldown) {
        this.heatresistcooldown = heatresistcooldown;
    }

    //Add to the value
    @Override
    public void addHeatResistCooldown(int heatresistcooldown) {
        this.heatresistcooldown += heatresistcooldown;
    }

    //Subtract from the value
    @Override
    public void subtractHeatResistCooldown(int heatresistcooldown) {
        this.heatresistcooldown -= heatresistcooldown;
    }

    //Get our integer
    @Override
    public int getFoodCooldown() {
        return foodcooldown;
    }

    //Set the value to a specified amount
    @Override
    public void setFoodCooldown(int foodcooldown) {
        this.foodcooldown = foodcooldown;
    }

    //Add to the value
    @Override
    public void addFoodCooldown(int foodcooldown) {
        this.foodcooldown += foodcooldown;
    }

    //Subtract from the value
    @Override
    public void subtractFoodCooldown(int foodcooldown) {
        this.foodcooldown -= foodcooldown;
    }


    //Get our integer
    @Override
    public float getStamina() {
        return stamina;
    }

    //Set the value to a specified amount
    @Override
    public void setStamina(float amount) {
        stamina = Math.max(0, Math.min(maxstamina, amount));
    }

    //Add to the value
    @Override
    public void addStamina(float amount) {
        stamina = Math.min(maxstamina, stamina + amount);
    }

    //Subtract from the value
    @Override
    public void subtractStamina(float amount) {
        stamina = Math.min(maxstamina, stamina - amount);
    }

    //Get our integer
    @Override
    public float getMaxStamina() {
        return maxstamina;
    }

    //Set the value to a specified amount
    @Override
    public void setMaxStamina(float stamina) {
        this.maxstamina = maxstamina;
    }

    //Add to the value
    @Override
    public void addMaxStamina(float stamina) {
        this.maxstamina += maxstamina;
    }

    //Subtract from the value
    @Override
    public void subtractMaxStamina(float stamina) {
        this.maxstamina -= maxstamina;
    }
}