package com.ladestitute.zarrowsandstuff.capabilityzarrows;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = ZArrowsMain.MOD_ID, bus = Mod.EventBusSubscriber.Bus.FORGE)
public class PlayerDataCapabilityHandler {

    //Make sure this isn't invalid, if it, it will cause a crash
    //We provide a mod ID so it can be recognized with a specified path for the factory class
    public static final ResourceLocation ZPLAYERDATA_CAP = new ResourceLocation(ZArrowsMain.MOD_ID, "zarrowscap");

    //As explained below, this event is responsible for attaching and also removing the capability if the entity/item/etc no longer exists
    @SubscribeEvent
    public static void attachCapability(AttachCapabilitiesEvent<Entity> event) {
        //Do nothing if the entity is not the player
        if ((!(event.getObject() instanceof PlayerEntity))) {
            return;
        }
        if (event.getObject() instanceof PlayerEntity) {
            //We attach our capability here to the player, another entity, a chunk, blocks, items, etc
            PlayerDataCapabilityProvider provider = new PlayerDataCapabilityProvider();


            event.addCapability(ZPLAYERDATA_CAP, new PlayerDataCapabilityProvider());
            event.addListener(provider::invalidate);
            //The invalidate listener is a listener that removes the capability when the entity/itemstack/etc is destroyed, so we can clear our lazyoptional
            //This is important, if something else gets our cap and stores the value, it becomes invalid and is not usable for safety reasons
            //Otherwise, this creates a dangling reference, which is a reference to an object that no longer exists
            //As a result, make sure not to have the invalidate line missing when attaching a capability
            //You are responsible for handling your own mod's proper lazy-val cleanup
        }
    }

    @SubscribeEvent
    public static void onDeathEvent(LivingDeathEvent event) {
        //Also look into onplayercloned events if you wish to keep cap-data persisted between death or dimension transfers
        if ((!(event.getEntityLiving() instanceof PlayerEntity))) {
            return;
        }
        if (event.getEntityLiving() instanceof PlayerEntity) {
            Entity player = event.getEntityLiving();
            //START NOTE
            //This is the proper way to check/modify/edit/etc capability values
            //Verify if it's present then act on via the varible-name we provide, such as "h"
            player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                {
                    h.setColdResistCooldown(0);
                    h.setHeatResistCooldown(0);
                    h.setFoodCooldown(0);
                    h.setStamina(h.getMaxStamina());
                    // System.out.println("Cooldown reset!");
                }
            });
            //END NOTE
        }

        //Not related exactly but written down for reference and cause its important:
        //Important tile-entity notes about attaching capabilities and proper handling

        // @Nonnull
        // @Override
        // public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        //     if (cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
        //         return handler.cast();
        //     }
        //     if (cap == CapabilityItemHandler.FUEL) {
        //         return fuel.cast();
        //     }
        //     return super.getCapability(cap, side);
        //  }
        //}

        // Doing it like this is improper: if(cap.equals(CapabilityMoltenFuel.FUEL)
        // It is better to do the method of  if-checks as like below:
        // if (cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
        // if (cap == CapabilityMoltenFuel.FUEL)
        // This is because capabilities are singletons so it is better and more efficient to compare them directly
        // This is highly recommended because it is more efficient and faster
        // Lastly, storing a lazy-val there instead of casting the value to the lazy-val stored is improper
        // You should properly store a TE's handlers for capabilities via private LazyOptionals
        // Then cast them from said LazyOptional when doing if(cap == ) checks
        // Returning a lazyoptional there creates a new lazyoptional every time another mod asks for your capability for your TE
        // That is very heavy on object-creation
        // It also defeats the purpose of lazyoptional caching (and thus why they're different than normal values which execute when defined)
        // Also don't forget this particular line in the commented out example above, thats important too:
        // return super.getCapability(cap, side);

    }
}

