package com.ladestitute.zarrowsandstuff.client.render.arrows;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.entities.arrows.EntityFireArrow;
import net.minecraft.client.renderer.entity.ArrowRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class FireArrowRender extends ArrowRenderer<EntityFireArrow> {

    protected static final ResourceLocation TEXTURE = new ResourceLocation(ZArrowsMain.MOD_ID, "textures/entity/arrows/fire_arrow.png");

    public FireArrowRender(EntityRendererManager renderManagerIn) {
        super(renderManagerIn);
    }

    @Override
    public ResourceLocation getTextureLocation(EntityFireArrow entity) {
        return TEXTURE;
    }


}

