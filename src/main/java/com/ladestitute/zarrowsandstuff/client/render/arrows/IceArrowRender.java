package com.ladestitute.zarrowsandstuff.client.render.arrows;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.entities.arrows.EntityIceArrow;
import net.minecraft.client.renderer.entity.ArrowRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class IceArrowRender extends ArrowRenderer<EntityIceArrow> {

    protected static final ResourceLocation TEXTURE = new ResourceLocation(ZArrowsMain.MOD_ID, "textures/entity/arrows/ice_arrow.png");

    public IceArrowRender(EntityRendererManager renderManagerIn) {
        super(renderManagerIn);
    }

    @Override
    public ResourceLocation getTextureLocation(EntityIceArrow entity) {
        return TEXTURE;
    }


}

