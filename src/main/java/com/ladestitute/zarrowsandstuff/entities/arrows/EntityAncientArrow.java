package com.ladestitute.zarrowsandstuff.entities.arrows;

import com.ladestitute.zarrowsandstuff.registries.EntityInit;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import com.ladestitute.zarrowsandstuff.registries.SoundInit;
import com.ladestitute.zarrowsandstuff.util.ZDamageSource;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.network.IPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

import javax.annotation.Nullable;

//The EntityConstructor suppress warning is required for them to function in +1.16
@SuppressWarnings("EntityConstructor")
public class EntityAncientArrow extends AbstractArrowEntity {
    //Credit goes to ToMe25 for some arrow code

    public EntityAncientArrow(EntityType<? extends EntityAncientArrow> type, World world) {
        super(type, world);
    }

    public EntityAncientArrow(World worldIn, LivingEntity shooter) {
        super(EntityInit.ANCIENT_ARROW.get(), shooter, worldIn);
        this.setBaseDamage(this.getBaseDamage() + 50F);
    }

    public EntityAncientArrow(World worldIn, double x, double y, double z) {
        super(EntityInit.ANCIENT_ARROW.get(), x, y, z, worldIn);
    }

    @Override
    protected ItemStack getPickupItem() {
        return new ItemStack(ItemInit.ANCIENT_ARROW.get());
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void tick() {
        super.tick();

        if(this.isInWater())
        {
            this.remove();
        }

        if(!this.hasImpulse || !this.isInWaterOrRain() || !this.isInWater())
        {
            BlockPos currentPos = this.blockPosition();
            this.level.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                    SoundInit.ANCIENTARROWFLYING.get(), SoundCategory.PLAYERS, 0.38f, 1f);
        }

        if (this.inGround) {
            if (!this.isInWater() || !this.isInWaterOrRain()) {
                BlockPos currentPos = this.blockPosition();
            }
        }

        if (!this.inGround || !this.isInWaterOrRain() || !this.isInWater()) {
            this.level.addParticle(ParticleTypes.SOUL_FIRE_FLAME, this.getX(), this.getY(), this.getZ(), 0.0D, 0.0D,
                    0.0D);
            this.level.addParticle(ParticleTypes.SOUL_FIRE_FLAME, this.getX(), this.getY(), this.getZ(), 0.0D, 0.0D,
                    0.0D);
        }
    }

    @Override
    public IPacket<?> getAddEntityPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    @Override
    protected void doPostHurtEffects(LivingEntity living) {
        living.hurt(ZDamageSource.ancient_arrow, 25F);
        if(living.equals(EntityType.RAVAGER)||living.equals(EntityType.ELDER_GUARDIAN)||living.equals(EntityType.IRON_GOLEM))
        {
            this.setBaseDamage((this.getBaseDamage() + 50F)*2);
        }
        BlockPos currentPos = this.blockPosition();
        this.level.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                SoundInit.ANCIENTARROWHIT.get(), SoundCategory.PLAYERS, 0.5f, 1f);
        super.doPostHurtEffects(living);
    }

}

