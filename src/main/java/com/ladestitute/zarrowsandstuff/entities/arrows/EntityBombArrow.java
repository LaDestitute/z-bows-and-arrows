package com.ladestitute.zarrowsandstuff.entities.arrows;

import com.ladestitute.zarrowsandstuff.registries.EntityInit;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import com.ladestitute.zarrowsandstuff.registries.SoundInit;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.IPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.Dimension;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

//The EntityConstructor suppress warning is required for them to function in +1.16
@SuppressWarnings("EntityConstructor")
public class EntityBombArrow extends AbstractArrowEntity {
    //Credit goes to ToMe25 for some arrow code

    float damage = 25F;
    public EntityBombArrow(EntityType<? extends EntityBombArrow> type, World world) {
        super(type, world);
    }

    public EntityBombArrow(World worldIn, LivingEntity shooter) {
        super(EntityInit.BOMB_ARROW.get(), shooter, worldIn);
        if(ZConfigManager.getInstance().damageScaling() == 0) {
            this.setBaseDamage(this.getBaseDamage() + damage);
        }
        if(ZConfigManager.getInstance().damageScaling() == 1) {
            this.setBaseDamage(this.getBaseDamage() + (damage)/2);
        }
        if(ZConfigManager.getInstance().damageScaling() == 2) {
            this.setBaseDamage(this.getBaseDamage() + (damage)/4);
        }
    }

    public EntityBombArrow(World worldIn, double x, double y, double z) {
        super(EntityInit.BOMB_ARROW.get(), x, y, z, worldIn);
    }

    @Override
    protected ItemStack getPickupItem() {
        return new ItemStack(ItemInit.BOMB_ARROW.get());
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void tick() {
        super.tick();
        if(this.level.dimension().getRegistryName() == Dimension.NETHER.getRegistryName())
        {
            this.createExplosion();
        }

        if(this.isInWater())
        {
            this.remove();
        }

        if(!this.hasImpulse || !this.isInWaterOrRain() || !this.isInWater())
        {
            BlockPos currentPos = this.blockPosition();
            this.level.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                    SoundInit.BOMBARROWFLYING.get(), SoundCategory.PLAYERS, 0.38f, 1f);
        }

        if (!this.inGround || !this.isInWaterOrRain() || !this.isInWater()) {
            this.level.addParticle(ParticleTypes.CAMPFIRE_COSY_SMOKE, this.getX(), this.getY(), this.getZ(), 0.0D, 0.0D,
                    0.0D);
            this.level.addParticle(ParticleTypes.CAMPFIRE_COSY_SMOKE, this.getX(), this.getY(), this.getZ(), 0.0D, 0.0D,
                    0.0D);
        }
    }

    @Override
    public IPacket<?> getAddEntityPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }



    @Override
    protected void doPostHurtEffects(LivingEntity living) {
        if(!this.isInWaterOrRain()||!this.isInWater()) {
            this.createExplosion();
        }
    }

    @Override
    protected void onInsideBlock(BlockState state) {
        if (this.hasImpulse && !this.wasTouchingWater||this.hasImpulse && !this.isInWaterOrRain()) {
            BlockState block = level.getBlockState(blockPosition());
            if (block != Blocks.WATER.defaultBlockState()||block != Blocks.AIR.defaultBlockState()||block != Blocks.CAVE_AIR.defaultBlockState()||block != Blocks.VOID_AIR.defaultBlockState()) {
                this.createExplosion();
            }
        }
    }


    public void createExplosion() {
        if(!level.isClientSide) {
            level.explode(null, this.getX(), this.getY(), this.getZ(), 3F, true, Explosion.Mode.DESTROY);
            this.remove();
        }
    }

    }



