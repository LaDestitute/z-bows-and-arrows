package com.ladestitute.zarrowsandstuff.entities.arrows;

import com.ladestitute.zarrowsandstuff.registries.EntityInit;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import com.ladestitute.zarrowsandstuff.registries.SoundInit;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.network.IPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

//The EntityConstructor suppress warning is required for them to function in +1.16
@SuppressWarnings("EntityConstructor")
public class EntityFireArrow extends AbstractArrowEntity {
    //Credit goes to ToMe25 for some arrow code

    public EntityFireArrow(EntityType<? extends EntityFireArrow> type, World world) {
        super(type, world);
    }

    float damage = 5f;
    public EntityFireArrow(World worldIn, LivingEntity shooter) {
        super(EntityInit.FIRE_ARROW.get(), shooter, worldIn);
        if (ZConfigManager.getInstance().experimentalFeatures()) {
            if (this.getEntity() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) this.getEntity();
                ItemStack handstack = player.getItemBySlot(EquipmentSlotType.MAINHAND);
                if (player.hasItemInSlot(EquipmentSlotType.MAINHAND) && handstack.getItem() == ItemInit.GOLDEN_BOW.get()) {
                    if (ZConfigManager.getInstance().damageScaling() == 0) {
                        if (!ZConfigManager.getInstance().allowFireArrowDOT()) {
                            this.setBaseDamage(this.getBaseDamage() + damage * 2);
                        } else this.setBaseDamage(this.getBaseDamage() * 2);
                    }
                    if (ZConfigManager.getInstance().damageScaling() == 1) {
                        if (!ZConfigManager.getInstance().allowFireArrowDOT()) {
                            this.setBaseDamage(this.getBaseDamage() + ((damage) / 2) * 2);
                        } else this.setBaseDamage(this.getBaseDamage() / 2 * 2);
                    }
                    if (ZConfigManager.getInstance().damageScaling() == 2) {
                        if (!ZConfigManager.getInstance().allowFireArrowDOT()) {
                            this.setBaseDamage(this.getBaseDamage() + ((damage) / 4) * 2);
                        } else this.setBaseDamage(this.getBaseDamage() / 4 * 2);
                    }
                }

                if (ZConfigManager.getInstance().damageScaling() == 0) {
                    if (!ZConfigManager.getInstance().allowFireArrowDOT()) {
                        this.setBaseDamage(this.getBaseDamage() + damage);
                    } else this.setBaseDamage(this.getBaseDamage());
                }
                if (ZConfigManager.getInstance().damageScaling() == 1) {
                    if (!ZConfigManager.getInstance().allowFireArrowDOT()) {
                        this.setBaseDamage(this.getBaseDamage() + ((damage) / 2));
                    } else this.setBaseDamage(this.getBaseDamage() / 2);
                }
                if (ZConfigManager.getInstance().damageScaling() == 2) {
                    if (!ZConfigManager.getInstance().allowFireArrowDOT()) {
                        this.setBaseDamage(this.getBaseDamage() + ((damage) / 4));
                    } else this.setBaseDamage(this.getBaseDamage() / 4);
                }
            }
        }
    }

    public EntityFireArrow(World worldIn, double x, double y, double z) {
        super(EntityInit.FIRE_ARROW.get(), x, y, z, worldIn);
    }

    @Override
    protected ItemStack getPickupItem() {
        return new ItemStack(ItemInit.FIRE_ARROW.get());
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void tick() {
        super.tick();

        if(this.isInWater())
        {
            BlockPos currentPos = this.blockPosition();
            this.level.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                    SoundInit.FIREARROWWATER.get(), SoundCategory.PLAYERS, 2f, 1f);
            this.remove();
        }

        if(!this.hasImpulse || !this.isInWaterOrRain() || !this.isInWater())
        {
            BlockPos currentPos = this.blockPosition();
            this.level.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                    SoundInit.FIREARROWFLYING.get(), SoundCategory.PLAYERS, 0.25f, 1f);
        }

        if (this.inGround) {
            if (!this.isInWater() || !this.isInWaterOrRain()) {
                BlockPos currentPos = this.blockPosition();
                this.level.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                        SoundInit.FIREARROWHIT.get(), SoundCategory.PLAYERS, 1.5f, 1f);

                this.remove();
            }
        }

        if (!this.inGround || !this.isInWaterOrRain() || !this.isInWater()) {
            this.level.addParticle(ParticleTypes.FLAME, this.getX(), this.getY(), this.getZ(), 0.0D, 0.0D,
                    0.0D);
            this.level.addParticle(ParticleTypes.FLAME, this.getX(), this.getY(), this.getZ(), 0.0D, 0.0D,
                    0.0D);
        }
    }


    @Override
    protected void onInsideBlock(BlockState state) {
        BlockState block = level.getBlockState(blockPosition());
        if (this.inGround) {
            if(block == Blocks.SNOW.defaultBlockState()||block == Blocks.COBWEB.defaultBlockState()||
                    block == Blocks.BLACK_WALL_BANNER.defaultBlockState()||
                    block == Blocks.BLUE_WALL_BANNER.defaultBlockState()||
                    block == Blocks.BROWN_WALL_BANNER.defaultBlockState()||block == Blocks.BLACK_BANNER.defaultBlockState()||
                    block == Blocks.BLUE_BANNER.defaultBlockState()||block == Blocks.BROWN_BANNER.defaultBlockState()||
                    block == Blocks.CYAN_BANNER.defaultBlockState()||block == Blocks.GRAY_BANNER.defaultBlockState()||
                    block == Blocks.GREEN_BANNER.defaultBlockState()||block == Blocks.LIGHT_BLUE_BANNER.defaultBlockState()||
                    block == Blocks.LIGHT_GRAY_BANNER.defaultBlockState()||block == Blocks.PURPLE_BANNER.defaultBlockState()||
                    block == Blocks.PINK_BANNER.defaultBlockState()||block == Blocks.ORANGE_BANNER.defaultBlockState()||
                    block == Blocks.LIME_BANNER.defaultBlockState()||block == Blocks.MAGENTA_BANNER.defaultBlockState()||
                    block == Blocks.YELLOW_BANNER.defaultBlockState()||block == Blocks.RED_BANNER.defaultBlockState()||
                    block == Blocks.WHITE_BANNER.defaultBlockState()||block == Blocks.BLACK_CARPET.defaultBlockState()||
                    block == Blocks.BLUE_CARPET.defaultBlockState()||block == Blocks.BROWN_CARPET.defaultBlockState()||
                    block == Blocks.CYAN_CARPET.defaultBlockState()||block == Blocks.GRAY_CARPET.defaultBlockState()||
                    block == Blocks.GREEN_CARPET.defaultBlockState()||block == Blocks.LIGHT_BLUE_CARPET.defaultBlockState()||
                    block == Blocks.LIGHT_GRAY_CARPET.defaultBlockState()||block == Blocks.PURPLE_CARPET.defaultBlockState()||
                    block == Blocks.PINK_CARPET.defaultBlockState()||block == Blocks.ORANGE_CARPET.defaultBlockState()||
                    block == Blocks.LIME_CARPET.defaultBlockState()||block == Blocks.MAGENTA_CARPET.defaultBlockState()||
                    block == Blocks.YELLOW_CARPET.defaultBlockState()||block == Blocks.RED_CARPET.defaultBlockState()||
                    block == Blocks.WHITE_CARPET.defaultBlockState()||
                    block == Blocks.CYAN_WALL_BANNER.defaultBlockState()||block == Blocks.GRAY_WALL_BANNER.defaultBlockState()||
                    block == Blocks.GREEN_WALL_BANNER.defaultBlockState()||block == Blocks.LIGHT_BLUE_WALL_BANNER.defaultBlockState()||
                    block == Blocks.LIGHT_GRAY_WALL_BANNER.defaultBlockState()||block == Blocks.PURPLE_WALL_BANNER.defaultBlockState()||
                    block == Blocks.PINK_WALL_BANNER.defaultBlockState()||block == Blocks.ORANGE_WALL_BANNER.defaultBlockState()||
                    block == Blocks.LIME_WALL_BANNER.defaultBlockState()||block == Blocks.MAGENTA_WALL_BANNER.defaultBlockState()||
                    block == Blocks.YELLOW_WALL_BANNER.defaultBlockState()||block == Blocks.RED_WALL_BANNER.defaultBlockState()||
                    block == Blocks.WHITE_WALL_BANNER.defaultBlockState()||
                    block == Blocks.DEAD_BUSH.defaultBlockState()||block == Blocks.FERN.defaultBlockState()||
                    block == Blocks.GRASS.defaultBlockState()||block == Blocks.VINE.defaultBlockState()||
                    block == Blocks.RED_MUSHROOM.defaultBlockState()||block == Blocks.BROWN_MUSHROOM.defaultBlockState()||
                    block == Blocks.TRIPWIRE.defaultBlockState()||block == Blocks.PINK_TULIP.defaultBlockState()||
                    block == Blocks.ACACIA_SIGN.defaultBlockState()||block == Blocks.BIRCH_SIGN.defaultBlockState()||
                    block == Blocks.DARK_OAK_SIGN.defaultBlockState()||block == Blocks.JUNGLE_SIGN.defaultBlockState()||
                    block == Blocks.OAK_SIGN.defaultBlockState()||block == Blocks.SPRUCE_SIGN.defaultBlockState()||
                    block == Blocks.ACACIA_WALL_SIGN.defaultBlockState()||block == Blocks.BIRCH_WALL_SIGN.defaultBlockState()||
                    block == Blocks.DARK_OAK_WALL_SIGN.defaultBlockState()||block == Blocks.JUNGLE_WALL_SIGN.defaultBlockState()||
                    block == Blocks.OAK_WALL_SIGN.defaultBlockState()||block == Blocks.SPRUCE_WALL_SIGN.defaultBlockState()||
                    block == Blocks.DANDELION.defaultBlockState()||block == Blocks.POPPY.defaultBlockState()||
                    block == Blocks.PEONY.defaultBlockState()||block == Blocks.ROSE_BUSH.defaultBlockState()||
                    block == Blocks.LILAC.defaultBlockState()||block == Blocks.SUNFLOWER.defaultBlockState()||
                    block == Blocks.LILY_OF_THE_VALLEY.defaultBlockState()||block == Blocks.CORNFLOWER.defaultBlockState()||
                    block == Blocks.OXEYE_DAISY.defaultBlockState()||block == Blocks.BLUE_ORCHID.defaultBlockState()||
                    block == Blocks.ALLIUM.defaultBlockState()||block == Blocks.AZURE_BLUET.defaultBlockState()||
                    block == Blocks.ORANGE_TULIP.defaultBlockState()||block == Blocks.RED_TULIP.defaultBlockState()||
                    block == Blocks.WHITE_TULIP.defaultBlockState())
            {
                BlockPos currentPos = this.blockPosition();
                this.level.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                        SoundInit.FIREARROWHIT.get(), SoundCategory.PLAYERS, 1.5f, 1f);
                level.setBlockAndUpdate(this.getOnPos().above(), Blocks.FIRE.defaultBlockState());
                this.remove();
            }
        }
    }

    @Override
    public IPacket<?> getAddEntityPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    @Override
    protected void doPostHurtEffects(LivingEntity living) {
        BlockPos currentPos = this.blockPosition();
        living.addEffect(new EffectInstance(Effects.MOVEMENT_SLOWDOWN, 40, 10));
        this.level.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                SoundInit.FIREARROWHIT.get(), SoundCategory.PLAYERS, 1f, 1f);
        if(living.equals(EntityType.BLAZE)||living.equals(EntityType.MAGMA_CUBE)||living.equals(EntityType.HUSK))
        {
            this.setBaseDamage(this.getBaseDamage()/2);
        }
        if(living.equals(EntityType.POLAR_BEAR)||living.equals(EntityType.STRAY)||living.equals(EntityType.SNOW_GOLEM))
        {
            this.setBaseDamage(this.getBaseDamage()*2);
        }
        if(living.equals(EntityInit.ICE_CHUCHU.get()))
        {
            this.setBaseDamage(this.getBaseDamage()*100);
        }
        super.doPostHurtEffects(living);
    }
}
