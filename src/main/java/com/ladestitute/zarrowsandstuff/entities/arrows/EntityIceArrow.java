package com.ladestitute.zarrowsandstuff.entities.arrows;

import com.ladestitute.zarrowsandstuff.registries.EntityInit;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import com.ladestitute.zarrowsandstuff.registries.SoundInit;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.network.IPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

//The EntityConstructor suppress warning is required for them to function in +1.16
@SuppressWarnings("EntityConstructor")
public class EntityIceArrow extends AbstractArrowEntity {

    public EntityIceArrow(EntityType<? extends EntityIceArrow> type, World world) {
        super(type, world);
    }

    float damage = 5F;
    public EntityIceArrow(World worldIn, LivingEntity shooter) {
        super(EntityInit.ICE_ARROW.get(), shooter, worldIn);
        if (ZConfigManager.getInstance().experimentalFeatures()) {
            if (this.getEntity() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) this.getEntity();
                ItemStack handstack = player.getItemBySlot(EquipmentSlotType.MAINHAND);
                if (player.hasItemInSlot(EquipmentSlotType.MAINHAND) && handstack.getItem() == ItemInit.SILVER_BOW.get()) {
                    if (ZConfigManager.getInstance().damageScaling() == 0) {
                        this.setBaseDamage(this.getBaseDamage() + damage * 2);
                    }
                    if (ZConfigManager.getInstance().damageScaling() == 1) {
                        this.setBaseDamage(this.getBaseDamage() + ((damage) / 2) * 2);
                    }
                    if (ZConfigManager.getInstance().damageScaling() == 2) {
                        this.setBaseDamage(this.getBaseDamage() + ((damage) / 4) * 2);
                    }
                } else if (ZConfigManager.getInstance().damageScaling() == 0) {
                    this.setBaseDamage(this.getBaseDamage() + damage);
                }
                if (ZConfigManager.getInstance().damageScaling() == 1) {
                    this.setBaseDamage(this.getBaseDamage() + damage / 2);
                }
                if (ZConfigManager.getInstance().damageScaling() == 2) {
                    this.setBaseDamage(this.getBaseDamage() + damage / 4);
                }
            }
        }
    }

    public EntityIceArrow(World worldIn, double x, double y, double z) {
        super(EntityInit.ICE_ARROW.get(), x, y, z, worldIn);
    }

    @Override
    protected ItemStack getPickupItem() {
        return new ItemStack(ItemInit.ICE_ARROW.get());
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void tick() {
        super.tick();

        if(!level.isClientSide() && this.isInWater())
        {
            BlockPos currentPos = this.blockPosition();
            this.level.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                    SoundInit.ICEARROWWATER.get(), SoundCategory.PLAYERS, 2f, 1f);
            this.remove();
        }

        if(!level.isClientSide() && !this.hasImpulse || !this.isInWaterOrRain() || !this.isInWater())
        {
            BlockPos currentPos = this.blockPosition();
            this.level.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                    SoundInit.ICEARROWFLYING.get(), SoundCategory.PLAYERS, 0.38f, 1f);
        }

        if (!level.isClientSide() && this.inGround) {
            if (!this.isInWater() || !this.isInWaterOrRain()) {
                BlockPos currentPos = this.blockPosition();
                this.level.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                        SoundInit.ICEARROWHIT.get(), SoundCategory.PLAYERS, 1.5f, 1f);

                this.remove();
            }
        }

        if (!level.isClientSide() && !this.inGround || !level.isClientSide() && !this.isInWaterOrRain() || !level.isClientSide() && !this.isInWater()) {
            this.level.addParticle(ParticleTypes.ITEM_SNOWBALL, this.getX(), this.getY(), this.getZ(), 0.0D, 0.0D,
                    0.0D);
            this.level.addParticle(ParticleTypes.ITEM_SNOWBALL, this.getX(), this.getY(), this.getZ(), 0.0D, 0.0D,
                    0.0D);
        }
    }

    @Override
    public IPacket<?> getAddEntityPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    @Override
    protected void doPostHurtEffects(LivingEntity living) {
        BlockPos currentPos = this.blockPosition();
        this.level.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                SoundInit.ICEARROWHIT.get(), SoundCategory.PLAYERS, 1.5f, 1f);
        living.addEffect(new EffectInstance(Effects.MOVEMENT_SLOWDOWN, 120, 10));
        if(living.equals(EntityType.HUSK)||living.equals(EntityType.MAGMA_CUBE))
        {
            this.setBaseDamage(this.getBaseDamage()*2);
        }

        if(living.equals(EntityType.BLAZE))
        {
            if(!ZConfigManager.getInstance().oneshotblazes())
            {
                this.setBaseDamage(this.getBaseDamage()*2);
            }
        }
        if(living.equals(EntityType.POLAR_BEAR)||living.equals(EntityType.STRAY))
        {
            this.setBaseDamage(this.getBaseDamage()/2);
        }
        super.doPostHurtEffects(living);
    }

    @Override
    protected void onInsideBlock(BlockState state) {
        BlockState block = level.getBlockState(blockPosition());
        if (!this.hasImpulse) {

            if(block != Blocks.COMMAND_BLOCK.defaultBlockState() && block != Blocks.BARRIER.defaultBlockState() &&
                    block != Blocks.BEDROCK.defaultBlockState() && block != Blocks.STRUCTURE_BLOCK.defaultBlockState() &&
                    block != Blocks.NETHER_PORTAL.defaultBlockState() && block != Blocks.JIGSAW.defaultBlockState() &&
                    block != Blocks.END_PORTAL.defaultBlockState() && block != Blocks.END_PORTAL_FRAME.defaultBlockState() &&
                    block != Blocks.END_GATEWAY.defaultBlockState() && block == Blocks.WATER.defaultBlockState())
            {
                BlockPos currentPos = this.blockPosition();
                this.level.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                        SoundInit.ICEARROWWATER.get(), SoundCategory.PLAYERS, 1.5f, 1f);
                if(ZConfigManager.getInstance().icearrowsfrostedice())
                {
                    level.setBlockAndUpdate(this.getOnPos(), Blocks.FROSTED_ICE.defaultBlockState());
                }
                else level.setBlockAndUpdate(this.getOnPos(), Blocks.ICE.defaultBlockState());
                this.remove();
            }
            if(block != Blocks.COMMAND_BLOCK.defaultBlockState() && block != Blocks.BARRIER.defaultBlockState() &&
                    block != Blocks.BEDROCK.defaultBlockState() && block != Blocks.STRUCTURE_BLOCK.defaultBlockState() &&
                    block != Blocks.NETHER_PORTAL.defaultBlockState() && block != Blocks.JIGSAW.defaultBlockState() &&
                    block != Blocks.END_PORTAL.defaultBlockState() && block != Blocks.END_PORTAL_FRAME.defaultBlockState() &&
                    block != Blocks.END_GATEWAY.defaultBlockState() && block == Blocks.FIRE.defaultBlockState())
            {
                BlockPos currentPos = this.blockPosition();
                this.level.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                        SoundInit.ICEARROWWATER.get(), SoundCategory.PLAYERS, 1.5f, 1f);
                level.setBlockAndUpdate(this.getOnPos(), Blocks.AIR.defaultBlockState());
                this.remove();
            }
            if(block != Blocks.COMMAND_BLOCK.defaultBlockState() && block != Blocks.BARRIER.defaultBlockState() &&
                    block != Blocks.BEDROCK.defaultBlockState() && block != Blocks.STRUCTURE_BLOCK.defaultBlockState() &&
                    block != Blocks.NETHER_PORTAL.defaultBlockState() && block != Blocks.JIGSAW.defaultBlockState() &&
                    block != Blocks.END_PORTAL.defaultBlockState() && block != Blocks.END_PORTAL_FRAME.defaultBlockState() &&
                    block != Blocks.END_GATEWAY.defaultBlockState() && block == Blocks.LAVA.defaultBlockState())
            {
                BlockPos currentPos = this.blockPosition();
                this.level.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                        SoundInit.ICEARROWWATER.get(), SoundCategory.PLAYERS, 1.5f, 1f);
                level.setBlockAndUpdate(this.getOnPos(), Blocks.COBBLESTONE.defaultBlockState());
                this.remove();
            }
        }
    }

    }

