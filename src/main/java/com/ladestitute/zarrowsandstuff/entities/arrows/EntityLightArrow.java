package com.ladestitute.zarrowsandstuff.entities.arrows;

import com.ladestitute.zarrowsandstuff.registries.EntityInit;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import com.ladestitute.zarrowsandstuff.registries.SoundInit;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.IPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

//The EntityConstructor suppress warning is required for them to function in +1.16
@SuppressWarnings("EntityConstructor")
public class EntityLightArrow extends AbstractArrowEntity {
    //Credit goes to ToMe25 for some arrow code

    public EntityLightArrow(EntityType<? extends EntityLightArrow> type, World world) {
        super(type, world);
    }

    public EntityLightArrow(World worldIn, LivingEntity shooter) {
        super(EntityInit.LIGHT_ARROW.get(), shooter, worldIn);
        if(ZConfigManager.getInstance().damageScaling() == 0) {
            this.setBaseDamage(this.getBaseDamage() + 15F);
        }
        if(ZConfigManager.getInstance().damageScaling() == 1) {
            this.setBaseDamage(this.getBaseDamage() + (15F)/2);
        }
        if(ZConfigManager.getInstance().damageScaling() == 2) {
            this.setBaseDamage(this.getBaseDamage() + (15F)/4);
        }
    }

    public EntityLightArrow(World worldIn, double x, double y, double z) {
        super(EntityInit.LIGHT_ARROW.get(), x, y, z, worldIn);
    }

    @Override
    protected ItemStack getPickupItem() {
        return new ItemStack(ItemInit.LIGHT_ARROW.get());
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void tick() {
        super.tick();

        if(this.isInWater())
        {
            this.remove();
        }

        if(!this.hasImpulse || !this.isInWaterOrRain() || !this.isInWater())
        {
            BlockPos currentPos = this.blockPosition();
            this.level.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                    SoundInit.LIGHTARROWFLYING.get(), SoundCategory.PLAYERS, 0.38f, 1f);
        }

        if (this.inGround) {
            if (!this.isInWater() || !this.isInWaterOrRain()) {
                BlockPos currentPos = this.blockPosition();
                this.level.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                        SoundInit.LIGHTARROWHIT.get(), SoundCategory.PLAYERS, 1.5f, 1f);

                this.remove();
            }
        }

        if (!this.inGround || !this.isInWaterOrRain() || !this.isInWater()) {
            this.level.addParticle(ParticleTypes.END_ROD, this.getX(), this.getY(), this.getZ(), 0.0D, 0.0D,
                    0.0D);
            this.level.addParticle(ParticleTypes.END_ROD, this.getX(), this.getY(), this.getZ(), 0.0D, 0.0D,
                    0.0D);
        }
    }

    @Override
    public IPacket<?> getAddEntityPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    @Override
    protected void doPostHurtEffects(LivingEntity living) {
        if(living.equals(EntityType.DROWNED)||living.equals(EntityType.EVOKER)||living.equals(EntityType.HUSK)||living.equals(EntityType.PHANTOM)||
                living.equals(EntityType.PILLAGER)||living.equals(EntityType.SKELETON)||living.equals(EntityType.STRAY)||
        living.equals(EntityType.VEX)||living.equals(EntityType.VINDICATOR)||living.equals(EntityType.WITHER)||living.equals(EntityType.WITHER_SKELETON)
                ||living.equals(EntityType.ZOMBIE_VILLAGER)||living.equals(EntityType.ZOMBIE)||living.equals(EntityType.ZOMBIFIED_PIGLIN))
        {
            this.setBaseDamage(this.getBaseDamage()*2);
        }
        BlockPos currentPos = this.blockPosition();
        this.level.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                SoundInit.LIGHTARROWHIT.get(), SoundCategory.PLAYERS, 0.5f, 1f);
        super.doPostHurtEffects(living);
    }

}


