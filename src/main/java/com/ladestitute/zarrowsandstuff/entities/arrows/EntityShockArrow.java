package com.ladestitute.zarrowsandstuff.entities.arrows;

import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import com.ladestitute.zarrowsandstuff.registries.EntityInit;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import com.ladestitute.zarrowsandstuff.registries.SoundInit;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.network.IPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

import java.util.List;

//The EntityConstructor suppress warning is required for them to function in +1.16
@SuppressWarnings("EntityConstructor")
public class EntityShockArrow extends AbstractArrowEntity {
    //Credit goes to ToMe25 for some arrow code

    float fullshockwavedamage = 20F;
    float damage = 10F;
    public EntityShockArrow(EntityType<? extends EntityShockArrow> type, World world) {
        super(type, world);
    }

    public EntityShockArrow(World worldIn, LivingEntity shooter) {
        super(EntityInit.SHOCK_ARROW.get(), shooter, worldIn);
        if(ZConfigManager.getInstance().damageScaling() == 0) {
            this.setBaseDamage(this.getBaseDamage() + damage);
        }
        if(ZConfigManager.getInstance().damageScaling() == 1) {
            this.setBaseDamage(this.getBaseDamage() + damage/2);
        }
        if(ZConfigManager.getInstance().damageScaling() == 2) {
            this.setBaseDamage(this.getBaseDamage() + damage/4);
        }
    }

    public EntityShockArrow(World worldIn, double x, double y, double z) {
        super(EntityInit.SHOCK_ARROW.get(), x, y, z, worldIn);
    }

    @Override
    protected ItemStack getPickupItem() {
        return new ItemStack(ItemInit.SHOCK_ARROW.get());
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void tick() {
        super.tick();

        if(this.isInWater())
        {
            BlockPos currentPos = this.blockPosition();
            this.level.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                    SoundInit.SHOCKARROWWATER.get(), SoundCategory.PLAYERS, 2f, 1f);
            this.remove();
        }

        if(!this.hasImpulse || !this.isInWaterOrRain() || !this.isInWater())
        {
            BlockPos currentPos = this.blockPosition();
            this.level.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                    SoundInit.SHOCKARROWFLYING.get(), SoundCategory.PLAYERS, 0.38f, 1f);
        }

        if (this.inGround) {
            if (!this.isInWater() || !this.isInWaterOrRain()) {
                BlockPos currentPos = this.blockPosition();
                this.level.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                        SoundInit.SHOCKARROWHIT.get(), SoundCategory.PLAYERS, 1.5f, 1f);

                this.remove();
            }
        }

        if (!this.inGround || !this.isInWaterOrRain() || !this.isInWater()) {
            this.level.addParticle(ParticleTypes.ENCHANTED_HIT, this.getX(), this.getY(), this.getZ(), 0.0D, 0.0D,
                    0.0D);
            this.level.addParticle(ParticleTypes.ENCHANTED_HIT, this.getX(), this.getY(), this.getZ(), 0.0D, 0.0D,
                    0.0D);
        }
    }

    @Override
    public IPacket<?> getAddEntityPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    private List<LivingEntity> getPlayersAround() {
        float range = 5.5F;
        return level.getEntitiesOfClass(LivingEntity.class, new AxisAlignedBB(this.getX() + 0.5 - range, this.getY() + 0.5 - range, this.getX() + 0.5 - range, this.getX() + 0.5 - range, this.getY() + 0.5 - range, this.getZ() + 0.5 - range));
    }

    @Override
    protected void doPostHurtEffects(LivingEntity living) {
        BlockPos currentPos = this.blockPosition();
        this.level.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                SoundInit.SHOCKARROWHIT.get(), SoundCategory.PLAYERS, 1.5f, 1f);
        living.addEffect(new EffectInstance(Effects.MOVEMENT_SLOWDOWN, 40, 10));
        if (living.isInWaterOrRain() && !living.isInWater() || living.equals(EntityType.IRON_GOLEM)) {
            this.setBaseDamage(this.getBaseDamage() * 2);
        }
        ItemStack mainstack = living.getItemBySlot(EquipmentSlotType.MAINHAND);
        ItemStack offstack = living.getItemBySlot(EquipmentSlotType.OFFHAND);
        if (living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.IRON_SWORD ||
                living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.GOLDEN_SWORD ||
                living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.IRON_AXE ||
                living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.GOLDEN_AXE ||
                living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.IRON_PICKAXE ||
                living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.GOLDEN_PICKAXE ||
                living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.IRON_PICKAXE ||
                living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.IRON_SHOVEL ||
                living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.GOLDEN_PICKAXE ||
                living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.IRON_HOE ||
                living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.GOLDEN_HOE ||
                living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.CROSSBOW ||
                living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.SHIELD) {
            living.spawnAtLocation(mainstack);
            living.setItemSlot(EquipmentSlotType.MAINHAND, ItemStack.EMPTY);
        }
        if (living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.IRON_SWORD ||
                living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.GOLDEN_SWORD ||
                living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.IRON_AXE ||
                living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.GOLDEN_AXE ||
                living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.IRON_PICKAXE ||
                living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.GOLDEN_PICKAXE ||
                living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.IRON_PICKAXE ||
                living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.IRON_SHOVEL ||
                living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.GOLDEN_PICKAXE ||
                living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.IRON_HOE ||
                living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.GOLDEN_HOE ||
                living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.CROSSBOW ||
                living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.SHIELD) {
            living.spawnAtLocation(offstack);
            living.setItemSlot(EquipmentSlotType.OFFHAND, ItemStack.EMPTY);
        }
        if (living.isInWaterOrRain() && living.isInWater()) {
            double radius = 4;
            AxisAlignedBB axisalignedbb = new AxisAlignedBB(this.getOnPos()).inflate(radius).expandTowards(0.0D, this.level.getMaxBuildHeight(), 0.0D);
            List<LivingEntity> list = this.level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
                for (LivingEntity livingEntity : list) {
                    if (ZConfigManager.getInstance().selfShockArrowImmunity() == false)
                    {
                        ItemStack helmstack = living.getItemBySlot(EquipmentSlotType.HEAD);
                        if (livingEntity instanceof PlayerEntity && living.hasItemInSlot(EquipmentSlotType.HEAD) && helmstack.getItem() == ItemInit.TOPAZ_EARRINGS.get()) {
                            if(ZConfigManager.getInstance().damageScaling() == 0)
                            {
                                livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 1) {
                                livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 2) {
                                livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/8);
                            }
                        }
                        if (livingEntity instanceof PlayerEntity && livingEntity.hasItemInSlot(EquipmentSlotType.HEAD) && helmstack.getItem() == ItemInit.UPGRADED_TOPAZ_EARRINGS.get()) {
                            if(ZConfigManager.getInstance().damageScaling() == 0)
                            {
                                livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 1) {
                                livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/8);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 2) {
                                livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/16);
                            }
                        } else if(ZConfigManager.getInstance().damageScaling() == 0)
                        {
                            livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 1) {
                            livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 2) {
                            livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                        }
                        else if(ZConfigManager.getInstance().damageScaling() == 0)
                        {
                            livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 1) {
                            livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 2) {
                            livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                        }
                    }
                    if (ZConfigManager.getInstance().selfShockArrowImmunity() == true)
                    {
                        if (!(livingEntity instanceof PlayerEntity)) {
                            if(ZConfigManager.getInstance().damageScaling() == 0)
                            {
                                livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 1) {
                                livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 2) {
                                livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                            }
                        }
                    }
                }
            super.doPostHurtEffects(living);
        }
    }

    @Override
    protected void checkInsideBlocks() {
        BlockState block = level.getBlockState(blockPosition());
        double radius = 4;
        AxisAlignedBB axisalignedbb = new AxisAlignedBB(this.getOnPos()).inflate(radius).expandTowards(0.0D, this.level.getMaxBuildHeight(), 0.0D);
        List<LivingEntity> list = this.level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
        if (block == Blocks.ACTIVATOR_RAIL.defaultBlockState() || block == Blocks.ANCIENT_DEBRIS.defaultBlockState() ||
                block == Blocks.ANVIL.defaultBlockState() || block == Blocks.BELL.defaultBlockState() ||
                block == Blocks.CAULDRON.defaultBlockState() || block == Blocks.CHAIN.defaultBlockState() ||
                block == Blocks.CHIPPED_ANVIL.defaultBlockState() || block == Blocks.DAMAGED_ANVIL.defaultBlockState() ||
                block == Blocks.DETECTOR_RAIL.defaultBlockState() || block == Blocks.IRON_BLOCK.defaultBlockState() ||
                block == Blocks.GOLD_BLOCK.defaultBlockState() || block == Blocks.HOPPER.defaultBlockState() ||
                block == Blocks.HEAVY_WEIGHTED_PRESSURE_PLATE.defaultBlockState() || block == Blocks.IRON_BARS.defaultBlockState() ||
                block == Blocks.IRON_DOOR.defaultBlockState() || block == Blocks.IRON_TRAPDOOR.defaultBlockState() ||
                block == Blocks.LANTERN.defaultBlockState() || block == Blocks.LIGHT_WEIGHTED_PRESSURE_PLATE.defaultBlockState() ||
                block == Blocks.LODESTONE.defaultBlockState() || block == Blocks.MOVING_PISTON.defaultBlockState() ||
                block == Blocks.PISTON.defaultBlockState() || block == Blocks.PISTON_HEAD.defaultBlockState()) {
            for (LivingEntity livingEntity : list) {
                if (ZConfigManager.getInstance().selfShockArrowImmunity() == false)
                {
                    ItemStack helmstack = livingEntity.getItemBySlot(EquipmentSlotType.HEAD);
                    if (livingEntity instanceof PlayerEntity && livingEntity.hasItemInSlot(EquipmentSlotType.HEAD) && helmstack.getItem() == ItemInit.TOPAZ_EARRINGS.get()) {
                        if(ZConfigManager.getInstance().damageScaling() == 0)
                        {
                            livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 1) {
                            livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 2) {
                            livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/8);
                        }
                    }

                    if (livingEntity instanceof PlayerEntity && livingEntity.hasItemInSlot(EquipmentSlotType.HEAD) && helmstack.getItem() == ItemInit.UPGRADED_TOPAZ_EARRINGS.get()) {
                        if(ZConfigManager.getInstance().damageScaling() == 0)
                        {
                            livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 1) {
                            livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/8);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 2) {
                            livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/16);
                        }
                    } else if(ZConfigManager.getInstance().damageScaling() == 0)
                    {
                        livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage);
                    }
                    if(ZConfigManager.getInstance().damageScaling() == 1) {
                        livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                    }
                    if(ZConfigManager.getInstance().damageScaling() == 2) {
                        livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                    }

                    else if(ZConfigManager.getInstance().damageScaling() == 0)
                    {
                        livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage);
                    }
                    if(ZConfigManager.getInstance().damageScaling() == 1) {
                        livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                    }
                    if(ZConfigManager.getInstance().damageScaling() == 2) {
                        livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                    }
                }
                if (ZConfigManager.getInstance().selfShockArrowImmunity() == true)
                {
                    if (!(livingEntity instanceof PlayerEntity)) {
                        if(ZConfigManager.getInstance().damageScaling() == 0)
                        {
                            livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 1) {
                            livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 2) {
                            livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                        }
                    }
                }
                    this.remove();

            }
        }
        super.checkInsideBlocks();
    }

    @Override
        protected void onInsideBlock (BlockState state){
            BlockState block = level.getBlockState(blockPosition());
            double radius = 4;
            AxisAlignedBB axisalignedbb = new AxisAlignedBB(this.getOnPos()).inflate(radius).expandTowards(0.0D, this.level.getMaxBuildHeight(), 0.0D);
            List<LivingEntity> list = this.level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
            if (block == Blocks.WATER.defaultBlockState()) {
                BlockPos currentPos = this.blockPosition();
                this.level.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                        SoundInit.SHOCKARROWWATER.get(), SoundCategory.PLAYERS, 2.5f, 1f);
                for (LivingEntity livingEntity : list) {
                    if (ZConfigManager.getInstance().selfShockArrowImmunity() == false)
                    {
                        ItemStack helmstack = livingEntity.getItemBySlot(EquipmentSlotType.HEAD);
                        if (livingEntity instanceof PlayerEntity && livingEntity.hasItemInSlot(EquipmentSlotType.HEAD) && helmstack.getItem() == ItemInit.TOPAZ_EARRINGS.get()) {
                            if(ZConfigManager.getInstance().damageScaling() == 0)
                            {
                                livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 1) {
                                livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 2) {
                                livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/8);
                            }

                            if (livingEntity instanceof PlayerEntity && livingEntity.hasItemInSlot(EquipmentSlotType.HEAD) && helmstack.getItem() == ItemInit.UPGRADED_TOPAZ_EARRINGS.get()) {
                                if(ZConfigManager.getInstance().damageScaling() == 0)
                                {
                                    livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                                }
                                if(ZConfigManager.getInstance().damageScaling() == 1) {
                                    livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/8);
                                }
                                if(ZConfigManager.getInstance().damageScaling() == 2) {
                                    livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/16);
                                }
                            } else if(ZConfigManager.getInstance().damageScaling() == 0)
                            {
                                livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 1) {
                                livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 2) {
                                livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                            }
                        } else if(ZConfigManager.getInstance().damageScaling() == 0)
                        {
                            livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 1) {
                            livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 2) {
                            livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                        }

                        if (livingEntity instanceof PlayerEntity && livingEntity.hasItemInSlot(EquipmentSlotType.HEAD) && helmstack.getItem() == ItemInit.UPGRADED_TOPAZ_EARRINGS.get()) {
                            if(ZConfigManager.getInstance().damageScaling() == 0)
                            {
                                livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 1) {
                                livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/8);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 2) {
                                livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/16);
                            }
                        } else if(ZConfigManager.getInstance().damageScaling() == 0)
                        {
                            livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 1) {
                            livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                        }
                        if(ZConfigManager.getInstance().damageScaling() == 2) {
                            livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                        }
                    }
                    if (!ZConfigManager.getInstance().selfShockArrowImmunity() == true)
                    {
                        if (!(livingEntity instanceof PlayerEntity)) {
                            if(ZConfigManager.getInstance().damageScaling() == 0)
                            {
                                livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 1) {
                                livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/2);
                            }
                            if(ZConfigManager.getInstance().damageScaling() == 2) {
                                livingEntity.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage/4);
                            }
                        }
                    }
                        this.remove();
                }
                super.onInsideBlock(state);
            }
        }

    }





