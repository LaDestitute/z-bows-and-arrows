package com.ladestitute.zarrowsandstuff.entities.mob;

import net.minecraft.entity.*;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.controller.MovementController;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.particles.IParticleData;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.potion.Effects;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.EnumSet;

public class ChuchuEntity extends MobEntity implements IMob {
    public float targetSquish;
    public float squish;
    public float oSquish;
    private boolean wasOnGround;
    private int underWaterTicks;

    // !!!
    public ChuchuEntity(EntityType<? extends ChuchuEntity> p_i48552_1_, World p_i48552_2_) {
        super(p_i48552_1_, p_i48552_2_);
        // !!!
        this.moveControl = new ChuchuEntity.MoveHelperController(this);
        this.xpReward = 5;
    }

    protected void registerGoals() {
        this.goalSelector.addGoal(2, new ChuchuEntity.AttackGoal(this));
        this.goalSelector.addGoal(3, new ChuchuEntity.FaceRandomGoal(this));
        this.goalSelector.addGoal(5, new ChuchuEntity.HopGoal(this));
        this.targetSelector.addGoal(1, new NearestAttackableTargetGoal<>(this, PlayerEntity.class, 10, true, false, (p_213811_1_) -> {
            return Math.abs(p_213811_1_.getY() - this.getY()) <= 4.0D;
        }));
    }

    public static AttributeModifierMap.MutableAttribute createAttributes() {
        return MobEntity.createMobAttributes().add(Attributes.MAX_HEALTH, 15.0D).add(Attributes.MOVEMENT_SPEED, (double)0.3F).add(Attributes.ATTACK_DAMAGE, 2.0D).add(Attributes.FOLLOW_RANGE, 48.0D);
    }

    protected IParticleData getParticleType() {
        return ParticleTypes.ITEM_SLIME;
    }

    public void tick() {
        this.squish += (this.targetSquish - this.squish) * 0.5F;
        this.oSquish = this.squish;
        super.tick();
        if (this.onGround && !this.wasOnGround) {
            this.playSound(this.getSquishSound(), this.getSoundVolume(), ((this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F) / 0.8F);
            this.targetSquish = -0.5F;
        } else if (!this.onGround && this.wasOnGround) {
            this.targetSquish = 1.0F;
        }

        this.wasOnGround = this.onGround;
        this.decreaseSquish();
    }

    protected void decreaseSquish() {
        this.targetSquish *= 0.6F;
    }

    protected int getJumpDelay() {
        return this.random.nextInt(20) + 10;
    }

    public void refreshDimensions() {
        double d0 = this.getX();
        double d1 = this.getY();
        double d2 = this.getZ();
        super.refreshDimensions();
        this.setPos(d0, d1, d2);
    }

    public EntityType<? extends ChuchuEntity> getType() {
        return (EntityType<? extends ChuchuEntity>)super.getType();
    }

    public void playerTouch(PlayerEntity p_70100_1_) {
        if (this.isDealsDamage()) {
            this.dealDamage(p_70100_1_);
        }

    }

    protected void dealDamage(LivingEntity p_175451_1_) {
        if (this.isAlive()) {
            int i = 0;
            if (this.distanceToSqr(p_175451_1_) < 0.6D * (double)i * 0.6D * (double)i && this.canSee(p_175451_1_) && p_175451_1_.hurt(DamageSource.mobAttack(this), this.getAttackDamage())) {
                this.playSound(SoundEvents.SLIME_ATTACK, 1.0F, (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F);
                this.doEnchantDamageEffects(this, p_175451_1_);
            }
        }

    }

    protected float getStandingEyeHeight(Pose p_213348_1_, EntitySize p_213348_2_) {
        return 0.625F * p_213348_2_.height;
    }

    protected boolean isDealsDamage() {
        return this.isEffectiveAi();
    }

    protected float getAttackDamage() {
        return (float)this.getAttributeValue(Attributes.ATTACK_DAMAGE);
    }

    protected SoundEvent getHurtSound(DamageSource p_184601_1_) {
        return SoundEvents.SLIME_HURT;
    }

    protected SoundEvent getDeathSound() {
        return SoundEvents.SLIME_DEATH;
    }

    protected SoundEvent getSquishSound() {
        return SoundEvents.SLIME_SQUISH;
    }


    protected float getSoundVolume() {
        return 0.5F;
    }

    public int getMaxHeadXRot() {
        return 0;
    }

    protected boolean doPlayJumpSound() {
        return true;
    }

    protected void jumpFromGround() {
        Vector3d vector3d = this.getDeltaMovement();
        this.setDeltaMovement(vector3d.x, (double)this.getJumpPower(), vector3d.z);
        this.hasImpulse = true;
    }

    protected void customServerAiStep() {
        if (this.isInWaterOrBubble()) {
            ++this.underWaterTicks;
        } else {
            this.underWaterTicks = 0;
        }

        if (this.underWaterTicks > 20) {
            this.hurt(DamageSource.DROWN, 0.5F);
        }

    }

    @Nullable
    public ILivingEntityData finalizeSpawn(IServerWorld p_213386_1_, DifficultyInstance p_213386_2_, SpawnReason p_213386_3_, @Nullable ILivingEntityData p_213386_4_, @Nullable CompoundNBT p_213386_5_) {
        int i = this.random.nextInt(3);
        if (i < 2 && this.random.nextFloat() < 0.5F * p_213386_2_.getSpecialMultiplier()) {
            ++i;
        }

        int j = 1 << i;
        return super.finalizeSpawn(p_213386_1_, p_213386_2_, p_213386_3_, p_213386_4_, p_213386_5_);
    }


    protected SoundEvent getJumpSound() {
        return SoundEvents.SLIME_JUMP;
    }

    static class AttackGoal extends Goal {
        // !!!
        private final ChuchuEntity slime;
        private int growTiredTimer;

        // !!!
        public AttackGoal(ChuchuEntity p_i45824_1_) {
            this.slime = p_i45824_1_;
            this.setFlags(EnumSet.of(Goal.Flag.LOOK));
        }

        public boolean canUse() {
            LivingEntity livingentity = this.slime.getTarget();
            if (livingentity == null) {
                return false;
            } else if (!livingentity.isAlive()) {
                return false;
            } else {
                return (!(livingentity instanceof PlayerEntity) || !((PlayerEntity) livingentity).abilities.invulnerable) &&
                        this.slime.getMoveControl() instanceof MoveHelperController;
            }
        }

        public void start() {
            this.growTiredTimer = 300;
            super.start();
        }

        public boolean canContinueToUse() {
            LivingEntity livingentity = this.slime.getTarget();
            if (livingentity == null) {
                return false;
            } else if (!livingentity.isAlive()) {
                return false;
            } else if (livingentity instanceof PlayerEntity && ((PlayerEntity)livingentity).abilities.invulnerable) {
                return false;
            } else {
                return --this.growTiredTimer > 0;
            }
        }

        public void tick() {
            this.slime.lookAt(this.slime.getTarget(), 10.0F, 10.0F);
            ((ChuchuEntity.MoveHelperController)this.slime.getMoveControl()).setDirection(this.slime.yRot, this.slime.isDealsDamage());
        }
    }

    static class FaceRandomGoal extends Goal {
        // !!!
        private final ChuchuEntity slime;
        private float chosenDegrees;
        private int nextRandomizeTime;

        // !!!
        public FaceRandomGoal(ChuchuEntity p_i45820_1_) {
            this.slime = p_i45820_1_;
            this.setFlags(EnumSet.of(Goal.Flag.LOOK));
        }

        public boolean canUse() {
            return this.slime.getTarget() == null && (this.slime.onGround || this.slime.isInWater() || this.slime.isInLava() || this.slime.hasEffect(Effects.LEVITATION)) && this.slime.getMoveControl() instanceof ChuchuEntity.MoveHelperController;
        }

        public void tick() {
            if (--this.nextRandomizeTime <= 0) {
                this.nextRandomizeTime = 40 + this.slime.getRandom().nextInt(60);
                this.chosenDegrees = (float)this.slime.getRandom().nextInt(360);
            }

            ((ChuchuEntity.MoveHelperController)this.slime.getMoveControl()).setDirection(this.chosenDegrees, false);
        }
    }

    static class HopGoal extends Goal {
        // !!!
        private final ChuchuEntity slime;

        // !!!
        public HopGoal(ChuchuEntity p_i45822_1_) {
            this.slime = p_i45822_1_;
            this.setFlags(EnumSet.of(Goal.Flag.JUMP, Goal.Flag.MOVE));
        }

        public boolean canUse() {
            return !this.slime.isPassenger();
        }

        // !!!
        public void tick() {
            ((ChuchuEntity.MoveHelperController)this.slime.getMoveControl()).setWantedMovement(1.0D);
        }
    }

    static class MoveHelperController extends MovementController {
        private float yRot;
        private int jumpDelay;
        // !!!
        private final ChuchuEntity slime;
        private boolean isAggressive;

        // !!!
        public MoveHelperController(ChuchuEntity p_i45821_1_) {
            super(p_i45821_1_);
            this.slime = p_i45821_1_;
            this.yRot = 180.0F * p_i45821_1_.yRot / (float)Math.PI;
        }

        public void setDirection(float p_179920_1_, boolean p_179920_2_) {
            this.yRot = p_179920_1_;
            this.isAggressive = p_179920_2_;
        }

        public void setWantedMovement(double p_179921_1_) {
            this.speedModifier = p_179921_1_;
            this.operation = MovementController.Action.MOVE_TO;
        }

        public void tick() {
            this.mob.yRot = this.rotlerp(this.mob.yRot, this.yRot, 90.0F);
            this.mob.yHeadRot = this.mob.yRot;
            this.mob.yBodyRot = this.mob.yRot;
            if (this.operation != MovementController.Action.MOVE_TO) {
                this.mob.setZza(0.0F);
            } else {
                this.operation = MovementController.Action.WAIT;
                if (this.mob.isOnGround()) {
                    this.mob.setSpeed((float)(this.speedModifier * this.mob.getAttributeValue(Attributes.MOVEMENT_SPEED)));
                    if (this.jumpDelay-- <= 0) {
                        this.jumpDelay = this.slime.getJumpDelay();
                        if (this.isAggressive) {
                            this.jumpDelay /= 3;
                        }

                        this.slime.getJumpControl().jump();
                        if (this.slime.doPlayJumpSound()) {
                            this.slime.playSound(this.slime.getJumpSound(), this.slime.getSoundVolume(), 1f);
                        }
                    } else {
                        this.slime.xxa = 0.0F;
                        this.slime.zza = 0.0F;
                        this.mob.setSpeed(0.0F);
                    }
                } else {
                    this.mob.setSpeed((float)(this.speedModifier * this.mob.getAttributeValue(Attributes.MOVEMENT_SPEED)));
                }

            }
        }
    }
}

