package com.ladestitute.zarrowsandstuff.entities.mob.model;

import com.ladestitute.zarrowsandstuff.entities.mob.CourserBeeEntity;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;

public class CourserBeeModel <T extends CourserBeeEntity> extends EntityModel<T> {
    private final ModelRenderer bb_main;

    public CourserBeeModel() {
        texWidth = 8;
        texHeight = 8;

        bb_main = new ModelRenderer(this);
        bb_main.setPos(0.0F, 24.0F, 0.0F);
        bb_main.texOffs(3, 6).addBox(3.0F, -4.0F, -6.0F, 2.0F, 2.0F, 0.0F, 0.0F, false);
        bb_main.texOffs(3, 6).addBox(3.0F, -3.0F, 5.0F, 2.0F, 2.0F, 0.0F, 0.0F, false);
        bb_main.texOffs(3, 6).addBox(0.0F, -9.0F, 5.0F, 2.0F, 2.0F, 0.0F, 0.0F, false);
        bb_main.texOffs(3, 6).addBox(0.0F, -11.0F, 0.0F, 2.0F, 2.0F, 0.0F, 0.0F, false);
        bb_main.texOffs(3, 6).addBox(4.0F, -8.0F, 0.0F, 2.0F, 2.0F, 0.0F, 0.0F, false);
        bb_main.texOffs(3, 6).addBox(-4.0F, -8.0F, 3.0F, 2.0F, 2.0F, 0.0F, 0.0F, false);
        bb_main.texOffs(3, 6).addBox(-6.0F, -5.0F, 5.0F, 2.0F, 2.0F, 0.0F, 0.0F, false);
        bb_main.texOffs(3, 6).addBox(-5.0F, -6.0F, -1.0F, 2.0F, 2.0F, 0.0F, 0.0F, false);
        bb_main.texOffs(3, 6).addBox(-5.0F, -9.0F, -5.0F, 2.0F, 2.0F, 0.0F, 0.0F, false);
        bb_main.texOffs(3, 6).addBox(0.0F, -6.0F, -1.0F, 2.0F, 2.0F, 0.0F, 0.0F, false);
        bb_main.texOffs(3, 6).addBox(-4.0F, -4.0F, -6.0F, 2.0F, 2.0F, 0.0F, 0.0F, false);
    }

    @Override
    public void setupAnim(T p_225597_1_, float p_225597_2_, float p_225597_3_, float p_225597_4_, float p_225597_5_, float p_225597_6_) {

    }

    @Override
    public void renderToBuffer(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
        bb_main.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.xRot = x;
        modelRenderer.yRot = y;
        modelRenderer.zRot = z;
    }
}