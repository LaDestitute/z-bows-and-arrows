package com.ladestitute.zarrowsandstuff.entities.mob.model;

import com.ladestitute.zarrowsandstuff.entities.mob.ElectricChuchuEntity;
import com.ladestitute.zarrowsandstuff.entities.mob.IceChuchuEntity;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;

public class ElectricChuchuModel <T extends ElectricChuchuEntity> extends EntityModel<T> {
    private final ModelRenderer cube;
    private final ModelRenderer eye0;
    private final ModelRenderer eye4_r1;
    private final ModelRenderer eye1;
    private final ModelRenderer mouth;

    public ElectricChuchuModel() {
        texWidth = 40;
        texHeight = 16;

        cube = new ModelRenderer(this);
        cube.setPos(0.0F, 0.0F, 0.0F);
        cube.texOffs(0, 0).addBox(-4.0F, 16.0F, -4.0F, 8.0F, 8.0F, 8.0F, 0.0F, false);

        eye0 = new ModelRenderer(this);
        eye0.setPos(0.0F, 0.0F, -1.0F);
        cube.addChild(eye0);


        eye4_r1 = new ModelRenderer(this);
        eye4_r1.setPos(0.0F, 24.0F, 0.0F);
        eye0.addChild(eye4_r1);
        setRotationAngle(eye4_r1, 0.0873F, 0.0F, 0.0F);
        eye4_r1.texOffs(32, 0).addBox(-4.3F, -9.0F, -2.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
        eye4_r1.texOffs(32, 4).addBox(2.3F, -9.0F, -2.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);

        eye1 = new ModelRenderer(this);
        eye1.setPos(0.0F, 0.0F, 0.0F);
        cube.addChild(eye1);


        mouth = new ModelRenderer(this);
        mouth.setPos(0.0F, 0.0F, 0.0F);
        cube.addChild(mouth);
        mouth.texOffs(32, 8).addBox(0.0F, 21.0F, -3.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
    }

    @Override
    public void setupAnim(ElectricChuchuEntity p_225597_1_, float p_225597_2_, float p_225597_3_, float p_225597_4_, float p_225597_5_, float p_225597_6_) {

    }

    @Override
    public void renderToBuffer(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
        cube.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.xRot = x;
        modelRenderer.yRot = y;
        modelRenderer.zRot = z;
    }
}
