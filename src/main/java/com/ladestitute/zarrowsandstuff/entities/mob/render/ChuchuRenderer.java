package com.ladestitute.zarrowsandstuff.entities.mob.render;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.entities.mob.ChuchuEntity;
import com.ladestitute.zarrowsandstuff.entities.mob.model.ChuchuModel;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;

public class ChuchuRenderer extends MobRenderer<ChuchuEntity, ChuchuModel<ChuchuEntity>>
{
    protected static final ResourceLocation TEXTURE =
            new ResourceLocation(ZArrowsMain.MOD_ID, "textures/entity/chuchu.png");

    public ChuchuRenderer(EntityRendererManager renderManagerIn) {
        super(renderManagerIn, new ChuchuModel<>(), 0.5F);
    }

    @Override
    public ResourceLocation getTextureLocation(ChuchuEntity p_110775_1_) {
        return TEXTURE;
    }
}
