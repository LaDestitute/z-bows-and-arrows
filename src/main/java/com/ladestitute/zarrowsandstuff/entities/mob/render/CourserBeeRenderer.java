package com.ladestitute.zarrowsandstuff.entities.mob.render;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.entities.mob.CourserBeeEntity;
import com.ladestitute.zarrowsandstuff.entities.mob.model.CourserBeeModel;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;

public class CourserBeeRenderer extends MobRenderer<CourserBeeEntity, CourserBeeModel<CourserBeeEntity>>
{
    protected static final ResourceLocation TEXTURE =
            new ResourceLocation(ZArrowsMain.MOD_ID, "textures/entity/courser_bee.png");

    public CourserBeeRenderer(EntityRendererManager renderManagerIn) {
        super(renderManagerIn, new CourserBeeModel<>(), 0.2F);
    }

    @Override
    public ResourceLocation getTextureLocation(CourserBeeEntity p_110775_1_) {
        return TEXTURE;
    }
}
