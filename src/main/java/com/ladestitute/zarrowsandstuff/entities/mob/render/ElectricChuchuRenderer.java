package com.ladestitute.zarrowsandstuff.entities.mob.render;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.entities.mob.ElectricChuchuEntity;
import com.ladestitute.zarrowsandstuff.entities.mob.IceChuchuEntity;
import com.ladestitute.zarrowsandstuff.entities.mob.model.ElectricChuchuModel;
import com.ladestitute.zarrowsandstuff.entities.mob.model.IceChuchuModel;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;

public class ElectricChuchuRenderer extends MobRenderer<ElectricChuchuEntity, ElectricChuchuModel<ElectricChuchuEntity>>
{
    protected static final ResourceLocation TEXTURE =
            new ResourceLocation(ZArrowsMain.MOD_ID, "textures/entity/electric_chuchu.png");

    public ElectricChuchuRenderer(EntityRendererManager renderManagerIn) {
        super(renderManagerIn, new ElectricChuchuModel<>(), 0.5F);
    }

    @Override
    public ResourceLocation getTextureLocation(ElectricChuchuEntity p_110775_1_) {
        return TEXTURE;
    }
}
