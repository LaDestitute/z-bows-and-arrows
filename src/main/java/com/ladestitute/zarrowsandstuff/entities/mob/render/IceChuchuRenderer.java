package com.ladestitute.zarrowsandstuff.entities.mob.render;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.entities.mob.IceChuchuEntity;
import com.ladestitute.zarrowsandstuff.entities.mob.model.IceChuchuModel;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;

public class IceChuchuRenderer extends MobRenderer<IceChuchuEntity, IceChuchuModel<IceChuchuEntity>>
{
    protected static final ResourceLocation TEXTURE =
            new ResourceLocation(ZArrowsMain.MOD_ID, "textures/entity/ice_chuchu.png");

    public IceChuchuRenderer(EntityRendererManager renderManagerIn) {
        super(renderManagerIn, new IceChuchuModel<>(), 0.5F);
    }

    @Override
    public ResourceLocation getTextureLocation(IceChuchuEntity p_110775_1_) {
        return TEXTURE;
    }
}

