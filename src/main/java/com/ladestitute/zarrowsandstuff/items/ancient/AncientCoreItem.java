package com.ladestitute.zarrowsandstuff.items.ancient;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class AncientCoreItem extends Item {
    public AncientCoreItem(Properties properties)
    {
        super(properties.stacksTo(64));
    }

    @Override
    public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("This crystal was made using lost technology. At one time it was the power source for ancient machines. This item is very valuable to researchers."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}