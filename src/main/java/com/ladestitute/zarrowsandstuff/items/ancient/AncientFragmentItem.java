package com.ladestitute.zarrowsandstuff.items.ancient;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class AncientFragmentItem extends Item {
    public AncientFragmentItem(Properties properties)
    {
        super(properties.stacksTo(64));
    }

    @Override
    public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("A scrap from ancient machinery. Despite being rusted junk, its durability is centuries ahead of anything known."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}
