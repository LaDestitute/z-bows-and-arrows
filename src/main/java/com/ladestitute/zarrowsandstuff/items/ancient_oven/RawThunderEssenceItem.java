package com.ladestitute.zarrowsandstuff.items.ancient_oven;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class RawThunderEssenceItem extends Item {
    public RawThunderEssenceItem(Properties properties)
    {
        super(properties.stacksTo(64));
    }

    @Override
    public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("Raw thunder energy taken from several topazes. It needs refining."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}
