package com.ladestitute.zarrowsandstuff.items.arrows;

import com.ladestitute.zarrowsandstuff.entities.arrows.EntityFireArrow;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.ArrowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class FireArrowItem extends ArrowItem {
    public FireArrowItem(Properties properties)
    {
        super(properties.stacksTo(64));
    }

    @Override
    public AbstractArrowEntity createArrow(World worldIn, ItemStack stack, LivingEntity shooter) {
        EntityFireArrow arrow = new EntityFireArrow(worldIn, shooter);
        if(!arrow.isInWaterOrRain() || !arrow.isInWater()) {
            if(ZConfigManager.getInstance().allowFireArrowDOT()) {
                arrow.setSecondsOnFire(600);
            }
        }
        return arrow;
    }

    @Override
    public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("An arrow imbued with the power of fire. It breaks apart on impact, igniting objects in the immediate area. It's incredibly effective against cold things."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}
