package com.ladestitute.zarrowsandstuff.items.arrows;

import com.ladestitute.zarrowsandstuff.entities.arrows.EntityLightArrow;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.ArrowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class LightArrowItem extends ArrowItem {
    public LightArrowItem(Properties properties)
    {
        super(properties.stacksTo(64));
    }

    @Override
    public AbstractArrowEntity createArrow(World worldIn, ItemStack stack, LivingEntity shooter) {
        EntityLightArrow arrow = new EntityLightArrow(worldIn, shooter);
        return arrow;
    }

    @Override
    public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("Arrows empowered with sacred light that pierces and destroys evil itself (x2 damage against undead mobs and evil-aligned mobs such as Illagers). Few creatures have this as a weakness besides the Master Sword itself, which is known as the blade of evil's bane."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}
