package com.ladestitute.zarrowsandstuff.items.food;

import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.UseAction;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class ChillySimmeredFruitItem extends Item {
    public ChillySimmeredFruitItem(Properties properties)
    {
        super(properties.stacksTo(ZConfigManager.getInstance().modFoodStackSize.get()));
    }

    @Override
    public UseAction getUseAnimation(ItemStack stack) {
        return UseAction.EAT;
    }

    @Override
    public int getUseDuration(ItemStack stack)
    {
        return 10;
    }

    @Override
    public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("Grants low-level heat resistance. This sweet dish is made by heaping tasty fruits into a pan and simmering until tender."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}
