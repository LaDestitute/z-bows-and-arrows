package com.ladestitute.zarrowsandstuff.items.food;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.UseAction;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class SpicyPepperItem extends Item {
    public SpicyPepperItem(Properties properties)
    {
        super(properties.stacksTo(64));
    }

    @Override
    public UseAction getUseAnimation(ItemStack stack) {
        return UseAction.EAT;
    }

    @Override
    public int getUseDuration(ItemStack stack)
    {
        return 10;
    }

    @Override
    public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("This pepper is exploding with spice. Cook with it to create dishes that will raise your body temperature and help you withstand the cold."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}
