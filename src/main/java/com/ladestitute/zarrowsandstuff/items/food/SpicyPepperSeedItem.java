package com.ladestitute.zarrowsandstuff.items.food;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.passive.ChickenEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class SpicyPepperSeedItem extends Item {
    public SpicyPepperSeedItem(Properties properties)
    {
        super(properties.stacksTo(64));
    }

    @Override
    public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("Spicy pepper seeds. They are hardy, able to thrive in icy climates."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public ActionResultType interactLivingEntity(ItemStack stack, PlayerEntity p_111207_2_, LivingEntity p_111207_3_, Hand p_111207_4_) {
        if(p_111207_3_ instanceof ChickenEntity)
        {
            ((ChickenEntity) p_111207_3_).setInLove(p_111207_2_);
            stack.shrink(1);
        }
        return super.interactLivingEntity(stack, p_111207_2_, p_111207_3_, p_111207_4_);
    }
}
