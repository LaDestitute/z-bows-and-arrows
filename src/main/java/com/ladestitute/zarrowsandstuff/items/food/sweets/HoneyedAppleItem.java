package com.ladestitute.zarrowsandstuff.items.food.sweets;

import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.UseAction;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class HoneyedAppleItem extends Item {

    public HoneyedAppleItem(Item.Properties properties)
    {
        super(properties.stacksTo(ZConfigManager.getInstance().modFoodStackSize.get()));
    }

    @Override
    public UseAction getUseAnimation(ItemStack stack) {
        return UseAction.EAT;
    }

    @Override
    public int getUseDuration(ItemStack stack)
    {
        return 5;
    }

    @Override
    public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("A juicy sweet-and-sour dish combining newly ripened apples with honey."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}