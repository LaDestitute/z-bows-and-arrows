package com.ladestitute.zarrowsandstuff.items.food.sweets.pastry.modifiers;

import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.UseAction;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class SpicyPlainCrepeItem extends Item {

    public SpicyPlainCrepeItem(Item.Properties properties)
    {
        super(properties.stacksTo(ZConfigManager.getInstance().modFoodStackSize.get()));
    }

    @Override
    public UseAction getUseAnimation(ItemStack stack) {
        return UseAction.EAT;
    }

    @Override
    public int getUseDuration(ItemStack stack)
    {
        return 10;
    }

    @Override
    public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("Grants low-level cold resistance. The simplicity of this dish lets the flavor of its ingredients shine."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}