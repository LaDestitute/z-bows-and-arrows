package com.ladestitute.zarrowsandstuff.items.gear;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class TopazEarringsItem extends ArmorItem {
    public TopazEarringsItem(IArmorMaterial materialIn, EquipmentSlotType slot, Properties builder) {
        super(materialIn, slot, builder);
    }

    @Override
    public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("Earrings made by Gerudo craft workers. They're made with topaz, a gem that harnesses the power of lightning to increase electricity resistance."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}
