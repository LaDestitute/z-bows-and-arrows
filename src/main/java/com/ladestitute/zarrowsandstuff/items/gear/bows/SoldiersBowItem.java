package com.ladestitute.zarrowsandstuff.items.gear.bows;

import com.ladestitute.zarrowsandstuff.util.ZToolMaterials;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.BowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class SoldiersBowItem extends BowItem {

    public SoldiersBowItem(ZToolMaterials toolmaterial, Properties builder) {
        super(builder.stacksTo(1).durability((556)));
    }

    @Override
    public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("A bow designed for armed conflict. Inflicts more damage than a civilian bow, but it will still burn if it touches fire."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}

