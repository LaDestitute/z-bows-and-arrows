package com.ladestitute.zarrowsandstuff.items.gear.sets.snowquill;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class SnowquillBootsItem extends ArmorItem {
    public SnowquillBootsItem(IArmorMaterial materialIn, EquipmentSlotType slot, Properties builder) {
        super(materialIn, slot, builder);
    }

    @Override
    public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("Cold resistant leather boots made by Rito artisans for Hylians. The space between the leather and the inside is lined with feathers."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}
