package com.ladestitute.zarrowsandstuff.items.gear.sets.snowquill;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class SnowquillHeaddressItem extends ArmorItem {
    public SnowquillHeaddressItem(IArmorMaterial materialIn, EquipmentSlotType slot, Properties builder) {
        super(materialIn, slot, builder);
    }

    @Override
    public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("A Rito accessory made from snow-bird feathers. It's adorned with a ruby, a gem that harnesses the power of fire to make cold climates more tolerable."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}
