package com.ladestitute.zarrowsandstuff.items.natural;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class RockSaltItem extends Item {
    public RockSaltItem(Properties properties)
    {
        super(properties.stacksTo(64));
    }

    @Override
    public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("Crystallized salt from the ancient sea commonly used to season meals. Cannot be eaten in this form"));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}
