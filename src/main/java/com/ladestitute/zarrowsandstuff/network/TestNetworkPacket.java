package com.ladestitute.zarrowsandstuff.network;

import java.util.function.Supplier;

import net.minecraft.block.Blocks;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.network.PacketBuffer;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkEvent;

public class TestNetworkPackage {

    public int key;

    public TestNetworkPackage() {
    }

    public TestNetworkPackage(int key) {
        this.key = key;
    }

    public static void encode(TestNetworkPackage message, PacketBuffer buffer) {
        buffer.writeInt(message.key);
    }

    public static TestNetworkPackage decode(PacketBuffer buffer) {
        return new TestNetworkPackage(buffer.readInt());
    }

    public static void handle(TestNetworkPackage message, Supplier<NetworkEvent.Context> contextSupplier) {
        NetworkEvent.Context context = contextSupplier.get();
        context.enqueueWork(() -> {
            System.out.println(message.key);
        });
        context.setPacketHandled(true);
    }
}
