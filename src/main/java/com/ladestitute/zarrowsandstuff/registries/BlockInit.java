package com.ladestitute.zarrowsandstuff.registries;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.blocks.ancient.*;
import com.ladestitute.zarrowsandstuff.blocks.natural.CourserBeeHiveBlock;
import com.ladestitute.zarrowsandstuff.blocks.natural.OreDepositBlock;
import com.ladestitute.zarrowsandstuff.blocks.natural.RareOreDepositBlock;
import com.ladestitute.zarrowsandstuff.blocks.natural.SnowstoneBlock;
import com.ladestitute.zarrowsandstuff.blocks.natural.plants.*;
import com.ladestitute.zarrowsandstuff.blocks.natural.plants.PalmFruitBlock;
import com.ladestitute.zarrowsandstuff.blocks.natural.plants.hydromelon.HydromelonBlock;
import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class BlockInit {

    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS,
            ZArrowsMain.MOD_ID);

    //Stone
    public static final RegistryObject<Block> ANCIENT_WRECKAGE = BLOCKS.register("ancient_wreckage",
            () -> new AncientWreckageBlock(PropertiesInit.WRECKAGE));
    public static final RegistryObject<Block> ANCIENT_WRECKAGE_DESERT = BLOCKS.register("ancient_wreckage_desert",
            () -> new AncientWreckageDesertBlock(PropertiesInit.WRECKAGE));
    public static final RegistryObject<Block> ANCIENT_WRECKAGE_COLD = BLOCKS.register("ancient_wreckage_cold",
            () -> new AncientWreckageColdBlock(PropertiesInit.WRECKAGE));
    public static final RegistryObject<Block> ANCIENT_WRECKAGE_FOREST = BLOCKS.register("ancient_wreckage_forest",
            () -> new AncientWreckageForestBlock(PropertiesInit.WRECKAGE));
    public static final RegistryObject<Block> ANCIENT_WRECKAGE_SWAMP = BLOCKS.register("ancient_wreckage_swamp",
            () -> new AncientWreckageSwampBlock(PropertiesInit.WRECKAGE));
    public static final RegistryObject<Block> ANCIENT_WRECKAGE_JUNGLE = BLOCKS.register("ancient_wreckage_jungle",
            () -> new AncientWreckageJungleBlock(PropertiesInit.WRECKAGE));
    public static final RegistryObject<Block> SNOWSTONE = BLOCKS.register("snowstone",
            () -> new SnowstoneBlock(PropertiesInit.SNOWSTONE));

    //Ore
    public static final RegistryObject<Block> ORE_DEPOSIT = BLOCKS.register("ore_deposit", () ->
            new OreDepositBlock(PropertiesInit.ORE_DEPOSIT));
    public static final RegistryObject<Block> RARE_ORE_DEPOSIT = BLOCKS.register("rare_ore_deposit", () ->
            new RareOreDepositBlock(PropertiesInit.ORE_DEPOSIT));

    //Plants
    public static final RegistryObject<Block> HYDROMELON = BLOCKS.register("hydromelon",
            () -> new HydromelonBlock(PropertiesInit.HYDROMELON));
    public static final RegistryObject<Block> EMPTY_SPICY_PEPPER = BLOCKS.register("empty_spicy_pepper_plant",
            () -> new EmptySpicyPepperBlock(PropertiesInit.SPICY_PEPPER));
    public static final RegistryObject<Block> SPICY_PEPPER = BLOCKS.register("spicy_pepper_plant",
            () -> new SpicyPepperBlock(PropertiesInit.SPICY_PEPPER));
    public static final RegistryObject<Block> STAMELLA_SHROOM = BLOCKS.register("stamella_shroom_block",
            () -> new StamellaShroomBlock(AbstractBlock.Properties.copy(Blocks.RED_MUSHROOM)));
    public static final RegistryObject<Block> MIGHTY_BANANAS_LEAVES = BLOCKS.register("mighty_bananas_leaves",
            () -> new MightyBananaLeavesBlock(PropertiesInit.PALM_LEAVES));
    public static final RegistryObject<Block> MIGHTY_BANANAS_LEAVES_SIDE = BLOCKS.register("mighty_bananas_leaves_side",
            () -> new MightyBananaLeavesBlock(PropertiesInit.PALM_LEAVES));
    public static final RegistryObject<Block> MIGHTY_BANANAS_LOG = BLOCKS.register("mighty_bananas_log",
            () -> new RotatedPillarBlock(AbstractBlock.Properties.copy(Blocks.JUNGLE_LOG)));
    public static final RegistryObject<Block> MIGHTY_BANANAS_BLOCK = BLOCKS.register("mighty_bananas_block",
            () -> new MightyBananaBlock(PropertiesInit.SPICY_PEPPER));
    public static final RegistryObject<Block> PALM_LOG = BLOCKS.register("palm_log",
            () -> new PalmLogBlock(AbstractBlock.Properties.of(Material.WOOD)
                    .strength(2f, 2f)
                    .sound(SoundType.WOOD)
                    .harvestLevel(1)
                    .harvestTool(ToolType.AXE)
                    .requiresCorrectToolForDrops()
                    .noOcclusion()));
    public static final RegistryObject<Block> MIGHTY_BANANA_LOG_TOP = BLOCKS.register("mighty_banana_log_top",
            () -> new MightyBananaLogBlock(AbstractBlock.Properties.of(Material.WOOD)
                    .strength(2f, 2f)
                    .sound(SoundType.WOOD)
                    .harvestLevel(1)
                    .harvestTool(ToolType.AXE)
                    .requiresCorrectToolForDrops()
                    .noOcclusion()));
    public static final RegistryObject<Block> MIGHTY_BANANA_LOG_BOTTOM = BLOCKS.register("mighty_banana_log_bottom",
            () -> new MightyBananaLogBlock(AbstractBlock.Properties.of(Material.WOOD)
                    .strength(2f, 2f)
                    .sound(SoundType.WOOD)
                    .harvestLevel(1)
                    .harvestTool(ToolType.AXE)
                    .requiresCorrectToolForDrops()
                    .noOcclusion()));
    public static final RegistryObject<Block> OASIS_GRASS = BLOCKS.register("oasis_grass",
            () -> new Block(AbstractBlock.Properties.of(Material.GRASS)
                    .strength(0.6f, 0.7f)
                    .sound(SoundType.GRASS)
                    .harvestLevel(0)
                    .noCollission()
                    .noOcclusion()));
    public static final RegistryObject<Block> PALM_FRUIT_BLOCK = BLOCKS.register("palm_fruit_block",
            () -> new PalmFruitBlock(PropertiesInit.SPICY_PEPPER));
    public static final RegistryObject<Block> VOLTFRUIT_CACTUS = BLOCKS.register("voltfruit_cactus",
            () -> new VoltfruitCactusBlock(PropertiesInit.VOLTFRUIT_CACTI));
    public static final RegistryObject<Block> VOLTFRUIT_PLANT = BLOCKS.register("voltfruit_plant",
            () -> new VoltfruitPlantBlock(PropertiesInit.SPICY_PEPPER));
    public static final RegistryObject<Block> FLEET_LOTUS_SEEDS = BLOCKS.register("fleet_lotus_seeds", () ->
            new FleetLotusSeedsBlock(PropertiesInit.SPICY_PEPPER));
    public static final RegistryObject<Block> WILDBERRY_BUSH = BLOCKS.register("wildberry_bush",
            () -> new WildberryBushBlock(PropertiesInit.SPICY_PEPPER));

    public static final RegistryObject<Block> COURSER_BEE_HIVE = BLOCKS.register("courser_bee_hive", () ->
            new CourserBeeHiveBlock(PropertiesInit.SPICY_PEPPER));
}
