package com.ladestitute.zarrowsandstuff.registries;

import com.google.common.collect.ImmutableSet;
import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import net.minecraft.block.Blocks;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.WorldGenRegistries;
import net.minecraft.world.gen.blockplacer.SimpleBlockPlacer;
import net.minecraft.world.gen.blockstateprovider.SimpleBlockStateProvider;
import net.minecraft.world.gen.feature.*;
import net.minecraft.world.gen.placement.AtSurfaceWithExtraConfig;
import net.minecraft.world.gen.placement.Placement;

public class ConfiguredFeaturesInit {
    public static final ConfiguredFeature<?, ?> PILE_ANCIENT_WRECKAGE = Feature.FOREST_ROCK.configured(new BlockStateFeatureConfig(
            BlockInit.ANCIENT_WRECKAGE.get().defaultBlockState())).decorated(Features.Placements.HEIGHTMAP_SQUARE
    ).countRandom(1).range(40).chance(32);
    public static final ConfiguredFeature<?, ?> PILE_ANCIENT_WRECKAGE_DESERT = Feature.FOREST_ROCK.configured(new BlockStateFeatureConfig(
            BlockInit.ANCIENT_WRECKAGE_DESERT.get().defaultBlockState())).decorated(Features.Placements.HEIGHTMAP_SQUARE
    ).countRandom(1).range(40).chance(32);
    public static final ConfiguredFeature<?, ?> PILE_ANCIENT_WRECKAGE_COLD = Feature.FOREST_ROCK.configured(new BlockStateFeatureConfig(
            BlockInit.ANCIENT_WRECKAGE_COLD.get().defaultBlockState())).decorated(Features.Placements.HEIGHTMAP_SQUARE
    ).countRandom(1).range(40).chance(32);
    public static final ConfiguredFeature<?, ?> PILE_ANCIENT_WRECKAGE_FOREST = Feature.FOREST_ROCK.configured(new BlockStateFeatureConfig(
            BlockInit.ANCIENT_WRECKAGE_FOREST.get().defaultBlockState())).decorated(Features.Placements.HEIGHTMAP_SQUARE
    ).countRandom(1).range(40).chance(32);
    public static final ConfiguredFeature<?, ?> PILE_ANCIENT_WRECKAGE_SWAMP = Feature.FOREST_ROCK.configured(new BlockStateFeatureConfig(
            BlockInit.ANCIENT_WRECKAGE_SWAMP.get().defaultBlockState())).decorated(Features.Placements.HEIGHTMAP_SQUARE
    ).countRandom(1).range(40).chance(32);
    public static final ConfiguredFeature<?, ?> PILE_ANCIENT_WRECKAGE_JUNGLE = Feature.FOREST_ROCK.configured(new BlockStateFeatureConfig(
            BlockInit.ANCIENT_WRECKAGE_JUNGLE.get().defaultBlockState())).decorated(Features.Placements.HEIGHTMAP_SQUARE
    ).countRandom(1).range(40).chance(32);

    public static final ConfiguredFeature<?, ?> ORE_DEPOSIT =
            Feature.RANDOM_PATCH.configured(new BlockClusterFeatureConfig.Builder(new
                    SimpleBlockStateProvider(BlockInit.ORE_DEPOSIT.get().defaultBlockState()),
                    SimpleBlockPlacer.INSTANCE).xspread(1).yspread(1).zspread(1).tries(12).blacklist(ImmutableSet.of(Blocks.SNOW.defaultBlockState(),Blocks.SNOW_BLOCK.defaultBlockState())).whitelist(ImmutableSet.of(Blocks.STONE)).build())
                    .decorated(Features.Placements.HEIGHTMAP_SQUARE).chance(14);

    public static final ConfiguredFeature<?, ?> RARE_ORE_DEPOSIT =
            Feature.RANDOM_PATCH.configured(new BlockClusterFeatureConfig.Builder(new
                            SimpleBlockStateProvider(BlockInit.RARE_ORE_DEPOSIT.get().defaultBlockState()),
                            SimpleBlockPlacer.INSTANCE).xspread(1).yspread(1).zspread(1).tries(3).blacklist(ImmutableSet.of(Blocks.SNOW.defaultBlockState(),Blocks.SNOW_BLOCK.defaultBlockState())).whitelist(ImmutableSet.of(Blocks.STONE)).build())
                    .decorated(Features.Placements.HEIGHTMAP_SQUARE).chance(4);

    public static final ConfiguredFeature<?, ?> PATCH_WILD_HYDROMELONS =
            Feature.RANDOM_PATCH.configured(new BlockClusterFeatureConfig.Builder(new
                    SimpleBlockStateProvider(BlockInit.HYDROMELON.get().defaultBlockState()),
                    SimpleBlockPlacer.INSTANCE).xspread(4).yspread(1).zspread(4).tries(6).whitelist(ImmutableSet.of(Blocks.SAND)).build())
                    .decorated(Features.Placements.HEIGHTMAP_SQUARE).chance(6);

    public static final ConfiguredFeature<?, ?> PATCH_WILD_JUNGLEHYDROMELONS =
            Feature.RANDOM_PATCH.configured(new BlockClusterFeatureConfig.Builder(new
                    SimpleBlockStateProvider(BlockInit.HYDROMELON.get().defaultBlockState()),
                    SimpleBlockPlacer.INSTANCE).xspread(4).yspread(1).zspread(4).tries(6).whitelist(ImmutableSet.of(Blocks.GRASS_BLOCK)).build())
                    .decorated(Features.Placements.HEIGHTMAP_SQUARE).chance(8);

    public static final ConfiguredFeature<?, ?> PATCH_WILD_PEPPERS =
    Feature.RANDOM_PATCH.configured(new BlockClusterFeatureConfig.Builder(new
    SimpleBlockStateProvider(BlockInit.SPICY_PEPPER.get().defaultBlockState()),
     SimpleBlockPlacer.INSTANCE).xspread(4).yspread(1).zspread(4).tries(6).whitelist(ImmutableSet.of(Blocks.GRASS_BLOCK)).build())
     .decorated(Features.Placements.HEIGHTMAP_SQUARE).chance(6);

    public static final ConfiguredFeature<?, ?> PATCH_STAMELLA_MUSHROOMS =
            Feature.RANDOM_PATCH.configured(new BlockClusterFeatureConfig.Builder(new
                    SimpleBlockStateProvider(BlockInit.STAMELLA_SHROOM.get().defaultBlockState()),
                    SimpleBlockPlacer.INSTANCE).xspread(4).yspread(1).zspread(4).tries(4).whitelist(ImmutableSet.of(Blocks.GRASS_BLOCK)).build())
                    .decorated(Features.Placements.HEIGHTMAP_SQUARE).chance(4);

    public static final ConfiguredFeature<?, ?> PATCH_STAMELLA_MUSHROOMSDARK =
            Feature.RANDOM_PATCH.configured(new BlockClusterFeatureConfig.Builder(new
                    SimpleBlockStateProvider(BlockInit.STAMELLA_SHROOM.get().defaultBlockState()),
                    SimpleBlockPlacer.INSTANCE).xspread(4).yspread(1).zspread(4).tries(6).whitelist(ImmutableSet.of(Blocks.GRASS_BLOCK)).build())
                    .decorated(Features.Placements.HEIGHTMAP_SQUARE).chance(6);


    public static final ConfiguredFeature<?, ?> PALM_TREE =
            FeatureInit.PALM_PLANT.get().configured(NoFeatureConfig.INSTANCE).decorated(Features.Placements.HEIGHTMAP_SQUARE);




   // public static final ConfiguredFeature<?, ?> PALM_TREE_BEACH =
   //         PALM_TREE.decorated(Placement.COUNT_EXTRA.configured(new AtSurfaceWithExtraConfig(40, 1F, 1)));

    public static final ConfiguredFeature<?, ?> PALM_TREE_BEACH =
            PALM_TREE.decorated(Features.Placements.HEIGHTMAP_SQUARE)
                    .decorated(Placement.COUNT_EXTRA.configured(new AtSurfaceWithExtraConfig(50, 3F, 1)));


    private static <FC extends IFeatureConfig> void register(String name, ConfiguredFeature<FC, ?> configuredFeature) {
        Registry.register(WorldGenRegistries.CONFIGURED_FEATURE, new ResourceLocation(ZArrowsMain.MOD_ID, name), configuredFeature);
    }

    public static void registerConfiguredFeatures() {
        register("ancient_wreckage_pile", PILE_ANCIENT_WRECKAGE);
        register("ancient_wreckage_pile_desert", PILE_ANCIENT_WRECKAGE_DESERT);
        register("ancient_wreckage_pile_cold", PILE_ANCIENT_WRECKAGE_COLD);
        register("ancient_wreckage_pile_forest", PILE_ANCIENT_WRECKAGE_FOREST);
        register("ancient_wreckage_pile_swamp", PILE_ANCIENT_WRECKAGE_SWAMP);
        register("ancient_wreckage_pile_jungle", PILE_ANCIENT_WRECKAGE_JUNGLE);
        register("ore_deposit", ORE_DEPOSIT);
        register("patch_wild_hydromelons", PATCH_WILD_HYDROMELONS);
        register("patch_wild_junglehydromelons", PATCH_WILD_JUNGLEHYDROMELONS);
        register("patch_wild_peppers", PATCH_WILD_PEPPERS);
        register("patch_stamella_mushrooms", PATCH_STAMELLA_MUSHROOMS);
        register("patch_stamella_mushroomsdark", PATCH_STAMELLA_MUSHROOMSDARK);
        register("palm", PALM_TREE);
        register("palm_beach", PALM_TREE_BEACH);
    }
}
