package com.ladestitute.zarrowsandstuff.registries;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.world.decorators.CourserBeehiveTreeDecorator;
import net.minecraft.world.gen.treedecorator.TreeDecoratorType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class DecoratorInit {

    public static final DeferredRegister<TreeDecoratorType<?>> DECORATORS = DeferredRegister.create(ForgeRegistries.TREE_DECORATOR_TYPES, ZArrowsMain.MOD_ID);

    public static final RegistryObject<TreeDecoratorType<?>> COURSER_BEEHIVE = DECORATORS.register("courser_beehive",
            () -> new TreeDecoratorType<>(CourserBeehiveTreeDecorator.CODEC));
}
