package com.ladestitute.zarrowsandstuff.registries;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.entities.arrows.*;
import com.ladestitute.zarrowsandstuff.entities.mob.ChuchuEntity;
import com.ladestitute.zarrowsandstuff.entities.mob.CourserBeeEntity;
import com.ladestitute.zarrowsandstuff.entities.mob.ElectricChuchuEntity;
import com.ladestitute.zarrowsandstuff.entities.mob.IceChuchuEntity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class EntityInit {

    public static final DeferredRegister<EntityType<?>> ENTITIES = DeferredRegister.create(ForgeRegistries.ENTITIES, ZArrowsMain.MOD_ID);

    //Arrows
    public static final RegistryObject<EntityType<EntityFireArrow>> FIRE_ARROW = ENTITIES.register("fire_arrow",
            () -> EntityType.Builder.<EntityFireArrow>of(EntityFireArrow::new, EntityClassification.MISC).sized(0.5F, 0.5F)
                    .build(new ResourceLocation(ZArrowsMain.MOD_ID, "textures/entity/arrows").toString()));
    public static final RegistryObject<EntityType<EntityIceArrow>> ICE_ARROW = ENTITIES.register("ice_arrow",
            () -> EntityType.Builder.<EntityIceArrow>of(EntityIceArrow::new, EntityClassification.MISC).sized(0.5F, 0.5F)
                    .build(new ResourceLocation(ZArrowsMain.MOD_ID, "textures/entity/arrows").toString()));
    public static final RegistryObject<EntityType<EntityShockArrow>> SHOCK_ARROW = ENTITIES.register("shock_arrow",
            () -> EntityType.Builder.<EntityShockArrow>of(EntityShockArrow::new, EntityClassification.MISC).sized(0.5F, 0.5F)
                    .build(new ResourceLocation(ZArrowsMain.MOD_ID, "textures/entity/arrows").toString()));
    public static final RegistryObject<EntityType<EntityBombArrow>> BOMB_ARROW = ENTITIES.register("bomb_arrow",
            () -> EntityType.Builder.<EntityBombArrow>of(EntityBombArrow::new, EntityClassification.MISC).sized(0.5F, 0.5F)
                    .build(new ResourceLocation(ZArrowsMain.MOD_ID, "textures/entity/arrows").toString()));
    public static final RegistryObject<EntityType<EntityAncientArrow>> ANCIENT_ARROW = ENTITIES.register("ancient_arrow",
            () -> EntityType.Builder.<EntityAncientArrow>of(EntityAncientArrow::new, EntityClassification.MISC).sized(0.5F, 0.5F)
                    .build(new ResourceLocation(ZArrowsMain.MOD_ID, "textures/entity/arrows").toString()));
    public static final RegistryObject<EntityType<EntityLightArrow>> LIGHT_ARROW = ENTITIES.register("light_arrow",
            () -> EntityType.Builder.<EntityLightArrow>of(EntityLightArrow::new, EntityClassification.MISC).sized(0.5F, 0.5F)
                    .build(new ResourceLocation(ZArrowsMain.MOD_ID, "textures/entity/arrows").toString()));

    public static final RegistryObject<EntityType<CourserBeeEntity>> COURSER_BEE = ENTITIES.register("courser_bee",
            () -> EntityType.Builder.<CourserBeeEntity>of(CourserBeeEntity::new, EntityClassification.MONSTER).sized(1F, 1F)
                    .build(new ResourceLocation(ZArrowsMain.MOD_ID, "courser_bee").toString()));

    public static final RegistryObject<EntityType<ChuchuEntity>> CHUCHU = ENTITIES.register("chuchu",
            () -> EntityType.Builder.<ChuchuEntity>of(ChuchuEntity::new, EntityClassification.MONSTER).sized(1F, 1F)
                    .build(new ResourceLocation(ZArrowsMain.MOD_ID, "chuchu").toString()));

    public static final RegistryObject<EntityType<IceChuchuEntity>> ICE_CHUCHU = ENTITIES.register("ice_chuchu",
            () -> EntityType.Builder.<IceChuchuEntity>of(IceChuchuEntity::new, EntityClassification.MONSTER).sized(1F, 1F)
                    .build(new ResourceLocation(ZArrowsMain.MOD_ID, "ice_chuchu").toString()));

    public static final RegistryObject<EntityType<ElectricChuchuEntity>> ELECTRIC_CHUCHU = ENTITIES.register("electric_chuchu",
            () -> EntityType.Builder.<ElectricChuchuEntity>of(ElectricChuchuEntity::new, EntityClassification.MONSTER).sized(1F, 1F)
                    .build(new ResourceLocation(ZArrowsMain.MOD_ID, "electric_chuchu").toString()));

}
