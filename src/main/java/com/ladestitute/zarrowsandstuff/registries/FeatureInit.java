package com.ladestitute.zarrowsandstuff.registries;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.world.decorators.CourserBeehiveTreeDecorator;
import com.ladestitute.zarrowsandstuff.world.feature.MightyBananasTreeFeature;
import com.ladestitute.zarrowsandstuff.world.feature.PalmTreeFeature;
import com.ladestitute.zarrowsandstuff.world.feature.VoltfruitCactusFeature;
import net.minecraft.block.Blocks;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.WorldGenRegistries;
import net.minecraft.world.gen.blockplacer.SimpleBlockPlacer;
import net.minecraft.world.gen.blockstateprovider.SimpleBlockStateProvider;
import net.minecraft.world.gen.feature.*;
import net.minecraft.world.gen.foliageplacer.BlobFoliagePlacer;
import net.minecraft.world.gen.placement.AtSurfaceWithExtraConfig;
import net.minecraft.world.gen.placement.Placement;
import net.minecraft.world.gen.treedecorator.BeehiveTreeDecorator;
import net.minecraft.world.gen.treedecorator.LeaveVineTreeDecorator;
import net.minecraft.world.gen.trunkplacer.StraightTrunkPlacer;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class FeatureInit {

    public static final DeferredRegister<Feature<?>> FEATURES = DeferredRegister.create(ForgeRegistries.FEATURES, ZArrowsMain.MOD_ID);

    public static final RegistryObject<Feature<NoFeatureConfig>> PALM_PLANT =
            FEATURES.register("palm_plant", () -> new PalmTreeFeature(NoFeatureConfig.CODEC));
    public static final RegistryObject<Feature<NoFeatureConfig>> VOLTFRUIT_PLANT =
            FEATURES.register("voltfruit_plant", () -> new VoltfruitCactusFeature(NoFeatureConfig.CODEC));
    public static final RegistryObject<Feature<NoFeatureConfig>> BANANA_PLANT =
            FEATURES.register("banana_plant", () -> new MightyBananasTreeFeature(NoFeatureConfig.CODEC));

    public static final class Configured {
        public static final ConfiguredFeature<?, ?> PALM_PLANT =
                FeatureInit.PALM_PLANT.get().configured(NoFeatureConfig.INSTANCE).decorated(Features.Placements.HEIGHTMAP_SQUARE);
        public static final ConfiguredFeature<?, ?> PALM_PLANT_BEACH =
                PALM_PLANT.decorated(Placement.COUNT_EXTRA.configured(new AtSurfaceWithExtraConfig(0, 0.7F, 1)));
        public static final ConfiguredFeature<?, ?> PALM_PLANT_DESERT =
                PALM_PLANT.decorated(Placement.COUNT_EXTRA.configured(new AtSurfaceWithExtraConfig(0, 0.004F, 1)));

        public static final ConfiguredFeature<?, ?> VOLTFRUIT_PLANT =
                FeatureInit.VOLTFRUIT_PLANT.get().configured(NoFeatureConfig.INSTANCE).decorated(Features.Placements.HEIGHTMAP_SQUARE);
        public static final ConfiguredFeature<?, ?> VOLTFRUIT_PLANT_DESERT =
                VOLTFRUIT_PLANT.decorated(Placement.COUNT_EXTRA.configured(new AtSurfaceWithExtraConfig(0, 0.1F, 1)));

        public static final ConfiguredFeature<?, ?> BANANA_PLANT =
                FeatureInit.BANANA_PLANT.get().configured(NoFeatureConfig.INSTANCE).decorated(Features.Placements.HEIGHTMAP_SQUARE);
        public static final ConfiguredFeature<?, ?> BANANA_PLANT_JUNGLE =
                BANANA_PLANT.decorated(Placement.COUNT_EXTRA.configured(new AtSurfaceWithExtraConfig(2, 0.0F, 1)));

        //Non-plant features
        public static final ConfiguredFeature<?, ?> PILE_ANCIENT_WRECKAGE = Feature.FOREST_ROCK.configured(new BlockStateFeatureConfig(
                BlockInit.ANCIENT_WRECKAGE.get().defaultBlockState())).decorated(Features.Placements.HEIGHTMAP_SQUARE
        ).countRandom(1).range(40).chance(32);
        public static final ConfiguredFeature<?, ?> PILE_ANCIENT_WRECKAGE_DESERT = Feature.FOREST_ROCK.configured(new BlockStateFeatureConfig(
                BlockInit.ANCIENT_WRECKAGE_DESERT.get().defaultBlockState())).decorated(Features.Placements.HEIGHTMAP_SQUARE
        ).countRandom(1).range(40).chance(32);
        public static final ConfiguredFeature<?, ?> PILE_ANCIENT_WRECKAGE_COLD = Feature.FOREST_ROCK.configured(new BlockStateFeatureConfig(
                BlockInit.ANCIENT_WRECKAGE_COLD.get().defaultBlockState())).decorated(Features.Placements.HEIGHTMAP_SQUARE
        ).countRandom(1).range(40).chance(32);
        public static final ConfiguredFeature<?, ?> PILE_ANCIENT_WRECKAGE_FOREST = Feature.FOREST_ROCK.configured(new BlockStateFeatureConfig(
                BlockInit.ANCIENT_WRECKAGE_FOREST.get().defaultBlockState())).decorated(Features.Placements.HEIGHTMAP_SQUARE
        ).countRandom(1).range(40).chance(32);
        public static final ConfiguredFeature<?, ?> PILE_ANCIENT_WRECKAGE_SWAMP = Feature.FOREST_ROCK.configured(new BlockStateFeatureConfig(
                BlockInit.ANCIENT_WRECKAGE_SWAMP.get().defaultBlockState())).decorated(Features.Placements.HEIGHTMAP_SQUARE
        ).countRandom(1).range(40).chance(32);
        public static final ConfiguredFeature<?, ?> PILE_ANCIENT_WRECKAGE_JUNGLE = Feature.FOREST_ROCK.configured(new BlockStateFeatureConfig(
                BlockInit.ANCIENT_WRECKAGE_JUNGLE.get().defaultBlockState())).decorated(Features.Placements.HEIGHTMAP_SQUARE
        ).countRandom(1).range(40).chance(32);

        public static final ConfiguredFeature<?, ?> ORE_DEPOSIT =
                Feature.RANDOM_PATCH.configured(new BlockClusterFeatureConfig.Builder(new
                                SimpleBlockStateProvider(BlockInit.ORE_DEPOSIT.get().defaultBlockState()),
                                SimpleBlockPlacer.INSTANCE).xspread(1).yspread(1).zspread(1).tries(12).blacklist(ImmutableSet.of(Blocks.SNOW.defaultBlockState(),Blocks.SNOW_BLOCK.defaultBlockState())).whitelist(ImmutableSet.of(Blocks.STONE)).build())
                        .decorated(Features.Placements.HEIGHTMAP_SQUARE).chance(14);

        public static final ConfiguredFeature<?, ?> FLEET_LOTUS =
                Feature.RANDOM_PATCH.configured(new BlockClusterFeatureConfig.Builder(new
                                SimpleBlockStateProvider(BlockInit.FLEET_LOTUS_SEEDS.get().defaultBlockState()),
                                SimpleBlockPlacer.INSTANCE).xspread(1).yspread(1).zspread(1).tries(12).blacklist(ImmutableSet.of(Blocks.SNOW.defaultBlockState(),Blocks.SNOW_BLOCK.defaultBlockState())).whitelist(ImmutableSet.of(Blocks.WATER)).build())
                        .decorated(Features.Placements.HEIGHTMAP_SQUARE).chance(14);

        public static final ConfiguredFeature<?, ?> RARE_ORE_DEPOSIT =
                Feature.RANDOM_PATCH.configured(new BlockClusterFeatureConfig.Builder(new
                                SimpleBlockStateProvider(BlockInit.RARE_ORE_DEPOSIT.get().defaultBlockState()),
                                SimpleBlockPlacer.INSTANCE).xspread(1).yspread(1).zspread(1).tries(3).blacklist(ImmutableSet.of(Blocks.SNOW.defaultBlockState(),Blocks.SNOW_BLOCK.defaultBlockState())).whitelist(ImmutableSet.of(Blocks.STONE)).build())
                        .decorated(Features.Placements.HEIGHTMAP_SQUARE).chance(4);

        public static final ConfiguredFeature<?, ?> PATCH_WILD_HYDROMELONS =
                Feature.RANDOM_PATCH.configured(new BlockClusterFeatureConfig.Builder(new
                                SimpleBlockStateProvider(BlockInit.HYDROMELON.get().defaultBlockState()),
                                SimpleBlockPlacer.INSTANCE).xspread(4).yspread(1).zspread(4).tries(6).whitelist(ImmutableSet.of(Blocks.SAND)).build())
                        .decorated(Features.Placements.HEIGHTMAP_SQUARE).chance(6);

        public static final ConfiguredFeature<?, ?> PATCH_WILD_JUNGLEHYDROMELONS =
                Feature.RANDOM_PATCH.configured(new BlockClusterFeatureConfig.Builder(new
                                SimpleBlockStateProvider(BlockInit.HYDROMELON.get().defaultBlockState()),
                                SimpleBlockPlacer.INSTANCE).xspread(4).yspread(1).zspread(4).tries(6).whitelist(ImmutableSet.of(Blocks.GRASS_BLOCK)).build())
                        .decorated(Features.Placements.HEIGHTMAP_SQUARE).chance(8);

        public static final ConfiguredFeature<?, ?> PATCH_WILD_PEPPERS =
                Feature.RANDOM_PATCH.configured(new BlockClusterFeatureConfig.Builder(new
                                SimpleBlockStateProvider(BlockInit.SPICY_PEPPER.get().defaultBlockState()),
                                SimpleBlockPlacer.INSTANCE).xspread(4).yspread(1).zspread(4).tries(6).whitelist(ImmutableSet.of(Blocks.GRASS_BLOCK)).build())
                        .decorated(Features.Placements.HEIGHTMAP_SQUARE).chance(6);

        public static final ConfiguredFeature<?, ?> PATCH_WILDBERRY =
                Feature.RANDOM_PATCH.configured(new BlockClusterFeatureConfig.Builder(new
                                SimpleBlockStateProvider(BlockInit.WILDBERRY_BUSH.get().defaultBlockState()),
                                SimpleBlockPlacer.INSTANCE).xspread(4).yspread(1).zspread(4).tries(6).whitelist(ImmutableSet.of(Blocks.GRASS_BLOCK)).build())
                        .decorated(Features.Placements.HEIGHTMAP_SQUARE).chance(6);

        public static final ConfiguredFeature<?, ?> PATCH_STAMELLA_MUSHROOMS =
                Feature.RANDOM_PATCH.configured(new BlockClusterFeatureConfig.Builder(new
                                SimpleBlockStateProvider(BlockInit.STAMELLA_SHROOM.get().defaultBlockState()),
                                SimpleBlockPlacer.INSTANCE).xspread(4).yspread(1).zspread(4).tries(4).whitelist(ImmutableSet.of(Blocks.GRASS_BLOCK)).build())
                        .decorated(Features.Placements.HEIGHTMAP_SQUARE).chance(4);

        public static final ConfiguredFeature<?, ?> PATCH_STAMELLA_MUSHROOMSDARK =
                Feature.RANDOM_PATCH.configured(new BlockClusterFeatureConfig.Builder(new
                                SimpleBlockStateProvider(BlockInit.STAMELLA_SHROOM.get().defaultBlockState()),
                                SimpleBlockPlacer.INSTANCE).xspread(4).yspread(1).zspread(4).tries(6).whitelist(ImmutableSet.of(Blocks.GRASS_BLOCK)).build())
                        .decorated(Features.Placements.HEIGHTMAP_SQUARE).chance(6);

        public static final ConfiguredFeature<?, ?> COURSER_BEEHIVE_OAK =
                Feature.TREE.configured(
                        new BaseTreeFeatureConfig.Builder(
                                new SimpleBlockStateProvider(Blocks.OAK_LOG.defaultBlockState()),
                                new SimpleBlockStateProvider(Blocks.OAK_LEAVES.defaultBlockState()),
                                new BlobFoliagePlacer(FeatureSpread.fixed(2), FeatureSpread.fixed(0), 3),
                                new StraightTrunkPlacer(4, 2, 0),
                                new TwoLayerFeature(1, 0, 1)).ignoreVines()
                                .decorators((ImmutableList.of(new CourserBeehiveTreeDecorator(0.38f))))
                                .build());

        public static final ConfiguredFeature<?, ?> COURSER_BEEHIVE_BIRCH =
                Feature.TREE.configured(
                        new BaseTreeFeatureConfig.Builder(
                                new SimpleBlockStateProvider(Blocks.BIRCH_LOG.defaultBlockState()),
                                new SimpleBlockStateProvider(Blocks.BIRCH_LEAVES.defaultBlockState()),
                                new BlobFoliagePlacer(FeatureSpread.fixed(2), FeatureSpread.fixed(0), 3),
                                new StraightTrunkPlacer(5, 2, 0),
                                new TwoLayerFeature(1, 0, 1)).ignoreVines()
                                .decorators((ImmutableList.of(new CourserBeehiveTreeDecorator(0.38f))))
                                .build());

        // BlobFoliagePlacer(FeatureSpread.fixed(2), FeatureSpread.fixed(0), 3), new StraightTrunkPlacer(5, 2, 0), new TwoLayerFeature(1, 0, 1

        public static final ConfiguredFeature<?, ?> COURSER_PLAINS =
                COURSER_BEEHIVE_OAK.decorated(Placement.COUNT_EXTRA.configured(new AtSurfaceWithExtraConfig(0, 0.1F, 1)));

        //BlobFoliagePlacer(FeatureSpread.fixed(3), FeatureSpread.fixed(0), 3), new StraightTrunkPlacer(5, 3, 0),
        // new TwoLayerFeature(1, 0, 1))).maxWaterDepth(1).decorators(ImmutableList.of(LeaveVineTreeDecorator.INSTANCE)
        public static final ConfiguredFeature<?, ?> COURSER_BEEHIVE_SWAMP =
                Feature.TREE.configured(
                        new BaseTreeFeatureConfig.Builder(
                                new SimpleBlockStateProvider(Blocks.OAK_LOG.defaultBlockState()),
                                new SimpleBlockStateProvider(Blocks.OAK_LEAVES.defaultBlockState()),
                                new BlobFoliagePlacer(FeatureSpread.fixed(3), FeatureSpread.fixed(0), 3),
                                new StraightTrunkPlacer(5, 3, 0),
                                new TwoLayerFeature(1, 0, 1))
                                .maxWaterDepth(1)
                                .decorators((ImmutableList.of(LeaveVineTreeDecorator.INSTANCE, new CourserBeehiveTreeDecorator(0.25f))))
                                .build());

        private static <FC extends IFeatureConfig> void register(String name, ConfiguredFeature<FC, ?> configuredFeature) {
            Registry.register(WorldGenRegistries.CONFIGURED_FEATURE, new ResourceLocation(ZArrowsMain.MOD_ID, name), configuredFeature);
        }

        public static void registerConfiguredFeatures() {

            register("ancient_wreckage_pile", PILE_ANCIENT_WRECKAGE);
            register("ancient_wreckage_pile_desert", PILE_ANCIENT_WRECKAGE_DESERT);
            register("ancient_wreckage_pile_cold", PILE_ANCIENT_WRECKAGE_COLD);
            register("ancient_wreckage_pile_forest", PILE_ANCIENT_WRECKAGE_FOREST);
            register("ancient_wreckage_pile_swamp", PILE_ANCIENT_WRECKAGE_SWAMP);
            register("ancient_wreckage_pile_jungle", PILE_ANCIENT_WRECKAGE_JUNGLE);
            register("ore_deposit", ORE_DEPOSIT);
            register("rare_ore_deposit", RARE_ORE_DEPOSIT);
            register("patch_wild_hydromelons", PATCH_WILD_HYDROMELONS);
            register("patch_wild_junglehydromelons", PATCH_WILD_JUNGLEHYDROMELONS);
            register("patch_wild_peppers", PATCH_WILD_PEPPERS);
            register("patch_stamella_mushrooms", PATCH_STAMELLA_MUSHROOMS);
            register("patch_stamella_mushroomsdark", PATCH_STAMELLA_MUSHROOMSDARK);
            register("palm_plant", PALM_PLANT);
            register("palm_plant_beach", PALM_PLANT_BEACH);
            register("palm_plant_desert", PALM_PLANT_DESERT);
            register("voltfruit_plant", VOLTFRUIT_PLANT);
            register("voltfruit_plant_desert", VOLTFRUIT_PLANT_DESERT);
            register("banana_plant", BANANA_PLANT);
            register("banana_plant_jungle", BANANA_PLANT_JUNGLE);
            register("fleet_lotus", FLEET_LOTUS);
            register("wildberry_bush", PATCH_WILDBERRY);
            register("courser_beehive", COURSER_BEEHIVE_OAK);
            register("courser_beehive_birch", COURSER_BEEHIVE_BIRCH);
            register("courser_beehive_swamp", COURSER_BEEHIVE_SWAMP);
        }
    }

}
