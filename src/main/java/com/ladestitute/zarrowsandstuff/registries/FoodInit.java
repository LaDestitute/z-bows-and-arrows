package com.ladestitute.zarrowsandstuff.registries;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.items.food.*;
import com.ladestitute.zarrowsandstuff.items.food.meals.SpicyPepperSteakItem;
import com.ladestitute.zarrowsandstuff.items.food.meals.meat.skewers.ChillyMeatSkewerItem;
import com.ladestitute.zarrowsandstuff.items.food.meals.meat.skewers.MeatSkewerItem;
import com.ladestitute.zarrowsandstuff.items.food.meals.meat.skewers.SpicyMeatSkewerItem;
import com.ladestitute.zarrowsandstuff.items.food.meals.mushroom.glazed.GlazedMushroomsItem;
import com.ladestitute.zarrowsandstuff.items.food.meals.mushroom.skewers.EnergizingMushroomSkewerItem;
import com.ladestitute.zarrowsandstuff.items.food.meals.mushroom.skewers.MushroomSkewerItem;
import com.ladestitute.zarrowsandstuff.items.food.roasted.CharredPepperItem;
import com.ladestitute.zarrowsandstuff.items.food.roasted.RoastedHydromelonItem;
import com.ladestitute.zarrowsandstuff.items.food.roasted.SauteedPeppersItem;
import com.ladestitute.zarrowsandstuff.items.food.roasted.ToastyStamellaShroomItem;
import com.ladestitute.zarrowsandstuff.items.food.sweets.*;
import com.ladestitute.zarrowsandstuff.items.food.sweets.pastry.HoneyCrepeItem;
import com.ladestitute.zarrowsandstuff.items.food.sweets.pastry.PlainCrepeItem;
import com.ladestitute.zarrowsandstuff.items.food.sweets.pastry.WildberryCrepeItem;
import com.ladestitute.zarrowsandstuff.items.food.sweets.pastry.modifiers.ChillyPlainCrepeItem;
import com.ladestitute.zarrowsandstuff.items.food.sweets.pastry.modifiers.EnergizingPlainCrepeItem;
import com.ladestitute.zarrowsandstuff.items.food.sweets.pastry.modifiers.SpicyPlainCrepeItem;
import net.minecraft.item.Food;
import net.minecraft.item.Item;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class FoodInit {

    public static final DeferredRegister<Item> FOOD = DeferredRegister.create(ForgeRegistries.ITEMS,
            ZArrowsMain.MOD_ID);

    //Roasted items
    public static final RegistryObject<Item> ROASTED_HYDROMELON = FOOD.register("roasted_hydromelon",
            () -> new RoastedHydromelonItem(new Item.Properties().food(new Food.Builder().nutrition(4)
                    .saturationMod(0.6f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    public static final RegistryObject<Item> ROASTED_HYDROMELON_SLICE = FOOD.register("roasted_hydromelon_slice",
            () -> new RoastedHydromelonItem(new Item.Properties().food(new Food.Builder().nutrition(4)
                    .saturationMod(0.6f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    public static final RegistryObject<Item> CHARRED_PEPPER = FOOD.register("charred_pepper",
            () -> new CharredPepperItem(new Item.Properties().food(new Food.Builder().nutrition(3)
                    .saturationMod(0.6f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    public static final RegistryObject<Item> TOASTY_STAMELLA_SHROOM = FOOD.register("toasty_stamella_shroom",
            () -> new ToastyStamellaShroomItem(new Item.Properties().food(new Food.Builder().nutrition(3)
                    .saturationMod(0.6f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));

    //Recipe foods start

    //Simmered Fruit
    //One apple or berry
    public static final RegistryObject<Item> SIMMERED_FRUIT1 = FOOD.register("simmered_fruit1",
            () -> new SimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(2)
                    .saturationMod(0.3f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //One hydromelon
    public static final RegistryObject<Item> SIMMERED_FRUIT2 = FOOD.register("simmered_fruit2",
            () -> new ChillySimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(2)
                    .saturationMod(0.3f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Two apples or berries
    public static final RegistryObject<Item> SIMMERED_FRUIT3 = FOOD.register("simmered_fruit3",
            () -> new SimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(4)
                    .saturationMod(0.4f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //One generic fruit + pepper
    public static final RegistryObject<Item> SIMMERED_FRUIT4 = FOOD.register("simmered_fruit4",
            () -> new SpicySimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(4)
                    .saturationMod(0.4f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //One generic fruit + hydromelon
    public static final RegistryObject<Item> SIMMERED_FRUIT5 = FOOD.register("simmered_fruit5",
            () -> new ChillySimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(4)
                    .saturationMod(0.4f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Hydromelon + Hydromelon
    public static final RegistryObject<Item> SIMMERED_FRUIT6 = FOOD.register("simmered_fruit6",
            () -> new ChillySimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(4)
                    .saturationMod(0.4f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Generic fruit x3
    public static final RegistryObject<Item> SIMMERED_FRUIT7 = FOOD.register("simmered_fruit7",
            () -> new SimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(6)
                    .saturationMod(0.5f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Generic fruit x2 + pepper
    public static final RegistryObject<Item> SIMMERED_FRUIT8 = FOOD.register("simmered_fruit8",
            () -> new SpicySimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(6)
                    .saturationMod(0.5f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Generic fruit x2 + hydromelon
    public static final RegistryObject<Item> SIMMERED_FRUIT9 = FOOD.register("simmered_fruit9",
            () -> new ChillySimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(6)
                    .saturationMod(0.5f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Generic fruit + pepper + hydromelon
    public static final RegistryObject<Item> SIMMERED_FRUIT10 = FOOD.register("simmered_fruit10",
            () -> new SimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(6)
                    .saturationMod(0.5f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Generic fruit + hydromelon x2
    public static final RegistryObject<Item> SIMMERED_FRUIT11 = FOOD.register("simmered_fruit11",
            () -> new ChillySimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(6)
                    .saturationMod(0.5f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Hydromelon x3
    public static final RegistryObject<Item> SIMMERED_FRUIT12 = FOOD.register("simmered_fruit12",
            () -> new ChillySimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(6)
                    .saturationMod(0.5f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Generic fruit x4
    public static final RegistryObject<Item> SIMMERED_FRUIT13 = FOOD.register("simmered_fruit13",
            () -> new SimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(0.6f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Generic fruit x3 + pepper
    public static final RegistryObject<Item> SIMMERED_FRUIT14 = FOOD.register("simmered_fruit14",
            () -> new SpicySimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(0.6f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Generic fruit x3 + hydromelon
    public static final RegistryObject<Item> SIMMERED_FRUIT15 = FOOD.register("simmered_fruit15",
            () -> new ChillySimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(0.6f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Generic fruit x2 + pepper x2
    public static final RegistryObject<Item> SIMMERED_FRUIT16 = FOOD.register("simmered_fruit16",
            () -> new SpicySimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(0.6f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Generic fruit x2 + hydromelon x2
    public static final RegistryObject<Item> SIMMERED_FRUIT17 = FOOD.register("simmered_fruit17",
            () -> new ChillySimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(0.6f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Generic fruit x2 + hydromelon + pepper
    public static final RegistryObject<Item> SIMMERED_FRUIT18 = FOOD.register("simmered_fruit18",
            () -> new SimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(0.6f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Generic fruit + pepper x3
    public static final RegistryObject<Item> SIMMERED_FRUIT19 = FOOD.register("simmered_fruit19",
            () -> new SpicySimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(0.6f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Any combo of hydromelon AND pepper plus one or two additional fruits (max four fruits including hydromelon/pepper)
    public static final RegistryObject<Item> SIMMERED_FRUIT20 = FOOD.register("simmered_fruit20",
            () -> new SimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(0.6f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Generic fruit + hydromelon x3
    public static final RegistryObject<Item> SIMMERED_FRUIT21 = FOOD.register("simmered_fruit21",
            () -> new ChillySimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(0.6f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Hydromelon x4
    public static final RegistryObject<Item> SIMMERED_FRUIT22 = FOOD.register("simmered_fruit22",
            () -> new ChillySimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(0.6f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Generic fruit x5
    public static final RegistryObject<Item> SIMMERED_FRUIT23 = FOOD.register("simmered_fruit23",
            () -> new SauteedPeppersItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.7f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Generic fruit x4 + pepper
    public static final RegistryObject<Item> SIMMERED_FRUIT24 = FOOD.register("simmered_fruit24",
            () -> new SpicySimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.7f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Generic fruit x4 + hydromelon
    public static final RegistryObject<Item> SIMMERED_FRUIT25 = FOOD.register("simmered_fruit25",
            () -> new ChillySimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.7f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Generic fruit x3 + Pepper x2
    public static final RegistryObject<Item> SIMMERED_FRUIT26 = FOOD.register("simmered_fruit26",
            () -> new SpicySimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.7f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Generic fruit x3 + pepper + hydromelon
    public static final RegistryObject<Item> SIMMERED_FRUIT27 = FOOD.register("simmered_fruit27",
            () -> new SimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.7f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Generic fruit x3 + hydromelon x2
    public static final RegistryObject<Item> SIMMERED_FRUIT28 = FOOD.register("simmered_fruit28",
        () -> new ChillySimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                .saturationMod(0.7f).alwaysEat()
                .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Generic fruit x2 + pepper x3
    public static final RegistryObject<Item> SIMMERED_FRUIT29 = FOOD.register("simmered_fruit29",
            () -> new SpicySimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.7f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Generic fruit x2 + hydromelon x3
    public static final RegistryObject<Item> SIMMERED_FRUIT30 = FOOD.register("simmered_fruit30",
            () -> new ChillySimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.7f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Generic fruit + pepper x4
    public static final RegistryObject<Item> SIMMERED_FRUIT31 = FOOD.register("simmered_fruit31",
            () -> new SpicySimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.7f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Generic fruit + hydromelon x4
    public static final RegistryObject<Item> SIMMERED_FRUIT32 = FOOD.register("simmered_fruit32",
            () -> new ChillySimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.7f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Hydromelon x5
    public static final RegistryObject<Item> SIMMERED_FRUIT33 = FOOD.register("simmered_fruit33",
            () -> new ChillySimmeredFruitItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.7f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));


    //Sauteed Peppers
    public static final RegistryObject<Item> SAUTEED_PEPPERS = FOOD.register("sauteed_peppers",
            () -> new SauteedPeppersItem(new Item.Properties().food(new Food.Builder().nutrition(2)
                    .saturationMod(0.3f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    public static final RegistryObject<Item> SAUTEED_PEPPERS2 = FOOD.register("sauteed_peppers2",
            () -> new SauteedPeppersItem(new Item.Properties().food(new Food.Builder().nutrition(4)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> SAUTEED_PEPPERS3 = FOOD.register("sauteed_peppers3",
            () -> new SauteedPeppersItem(new Item.Properties().food(new Food.Builder().nutrition(6)
                    .saturationMod(0.5f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> SAUTEED_PEPPERS4 = FOOD.register("sauteed_peppers4",
            () -> new SauteedPeppersItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(0.6f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> SAUTEED_PEPPERS5 = FOOD.register("sauteed_peppers5",
            () -> new SauteedPeppersItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.7f).alwaysEat()
                    .build())));

    public static final RegistryObject<Item> HONEYED_APPLE1 = FOOD.register("honeyed_apple1",
            () -> new EnergizingHoneyedAppleItem(new Item.Properties().food(new Food.Builder().nutrition(12)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_APPLE2 = FOOD.register("honeyed_apple2",
            () -> new EnergizingHoneyedAppleItem(new Item.Properties().food(new Food.Builder().nutrition(15)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_APPLE3 = FOOD.register("honeyed_apple3",
            () -> new HoneyedAppleItem(new Item.Properties().food(new Food.Builder().nutrition(12)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_APPLE4 = FOOD.register("honeyed_apple4",
            () -> new EnergizingHoneyedAppleItem(new Item.Properties().food(new Food.Builder().nutrition(15)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_APPLE5 = FOOD.register("honeyed_apple5",
            () -> new EnergizingHoneyedAppleItem(new Item.Properties().food(new Food.Builder().nutrition(18)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_APPLE6 = FOOD.register("honeyed_apple6",
            () -> new HoneyedAppleItem(new Item.Properties().food(new Food.Builder().nutrition(15)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_APPLE7 = FOOD.register("honeyed_apple7",
            () -> new EnergizingHoneyedAppleItem(new Item.Properties().food(new Food.Builder().nutrition(21)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_APPLE8 = FOOD.register("honeyed_apple8",
            () -> new HoneyedAppleItem(new Item.Properties().food(new Food.Builder().nutrition(18)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_APPLE9 = FOOD.register("honeyed_apple9",
            () -> new HoneyedAppleItem(new Item.Properties().food(new Food.Builder().nutrition(15)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_APPLE10 = FOOD.register("honeyed_apple10",
            () -> new EnergizingHoneyedAppleItem(new Item.Properties().food(new Food.Builder().nutrition(18)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_APPLE11 = FOOD.register("honeyed_apple11",
            () -> new EnergizingHoneyedAppleItem(new Item.Properties().food(new Food.Builder().nutrition(21)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_APPLE12 = FOOD.register("honeyed_apple12",
            () -> new HoneyedAppleItem(new Item.Properties().food(new Food.Builder().nutrition(18)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_APPLE13 = FOOD.register("honeyed_apple13",
            () -> new EnergizingHoneyedAppleItem(new Item.Properties().food(new Food.Builder().nutrition(24)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_APPLE14 = FOOD.register("honeyed_apple14",
            () -> new HoneyedAppleItem(new Item.Properties().food(new Food.Builder().nutrition(21)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_APPLE15 = FOOD.register("honeyed_apple15",
            () -> new HoneyedAppleItem(new Item.Properties().food(new Food.Builder().nutrition(18)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_APPLE16 = FOOD.register("honeyed_apple16",
            () -> new EnergizingHoneyedAppleItem(new Item.Properties().food(new Food.Builder().nutrition(27)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_APPLE17 = FOOD.register("honeyed_apple17",
            () -> new HoneyedAppleItem(new Item.Properties().food(new Food.Builder().nutrition(24)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_APPLE18 = FOOD.register("honeyed_apple18",
            () -> new HoneyedAppleItem(new Item.Properties().food(new Food.Builder().nutrition(21)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_APPLE19 = FOOD.register("honeyed_apple19",
            () -> new HoneyedAppleItem(new Item.Properties().food(new Food.Builder().nutrition(18)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));

    //1 Apple + 1 Honey
    public static final RegistryObject<Item> HONEYED_APPLE20 = FOOD.register("honeyed_apple20",
            () -> new EnergizingHoneyedAppleItem(new Item.Properties().food(new Food.Builder().nutrition(9)
                    .saturationMod(0.4f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));

    //1 Fruit + 1 Honey
    public static final RegistryObject<Item> HONEYED_FRUITS1 = FOOD.register("honeyed_fruits1",
            () -> new HoneyedFruitsItem(new Item.Properties().food(new Food.Builder().nutrition(9)
                    .saturationMod(0.4f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    public static final RegistryObject<Item> HONEYED_FRUITS2 = FOOD.register("honeyed_fruits2",
            () -> new HoneyedFruitsItem(new Item.Properties().food(new Food.Builder().nutrition(15)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_FRUITS3 = FOOD.register("honeyed_fruits3",
            () -> new HoneyedFruitsItem(new Item.Properties().food(new Food.Builder().nutrition(12)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_FRUITS4 = FOOD.register("honeyed_fruits4",
            () -> new HoneyedFruitsItem(new Item.Properties().food(new Food.Builder().nutrition(18)
                    .saturationMod(0.2f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_FRUITS5 = FOOD.register("honeyed_fruits5",
            () -> new HoneyedFruitsItem(new Item.Properties().food(new Food.Builder().nutrition(15)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_FRUITS6 = FOOD.register("honeyed_fruits6",
            () -> new HoneyedFruitsItem(new Item.Properties().food(new Food.Builder().nutrition(15)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_FRUITS7 = FOOD.register("honeyed_fruits7",
            () -> new HoneyedFruitsItem(new Item.Properties().food(new Food.Builder().nutrition(18)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_FRUITS8 = FOOD.register("honeyed_fruits8",
            () -> new HoneyedFruitsItem(new Item.Properties().food(new Food.Builder().nutrition(21)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_FRUITS9 = FOOD.register("honeyed_fruits9",
            () -> new HoneyedFruitsItem(new Item.Properties().food(new Food.Builder().nutrition(24)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEYED_FRUITS10 = FOOD.register("honeyed_fruits10",
            () -> new HoneyedFruitsItem(new Item.Properties().food(new Food.Builder().nutrition(18)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));

    //Honeyed items, honey candy, etc
    public static final RegistryObject<Item> HONEY_CANDY1 = FOOD.register("honey_candy1",
            () -> new HoneyCandyItem(new Item.Properties().food(new Food.Builder().nutrition(6)
                    .saturationMod(1.2f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    public static final RegistryObject<Item> HONEY_CANDY2 = FOOD.register("honey_candy2",
            () -> new HoneyCandyItem(new Item.Properties().food(new Food.Builder().nutrition(7)
                    .saturationMod(1.8f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEY_CANDY3 = FOOD.register("honey_candy3",
            () -> new HoneyCandyItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(2.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEY_CANDY4 = FOOD.register("honey_candy4",
            () -> new HoneyCandyItem(new Item.Properties().food(new Food.Builder().nutrition(9)
                    .saturationMod(3f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> HONEY_CANDY5 = FOOD.register("honey_candy5",
            () -> new HoneyCandyItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(3.6f).alwaysEat()
                    .build())));

    //Crepes
    public static final RegistryObject<Item> PLAIN_CREPE = FOOD.register("plain_crepe",
            () -> new PlainCrepeItem(new Item.Properties().food(new Food.Builder().nutrition(6)
                    .saturationMod(0.6f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    public static final RegistryObject<Item> WILDBERRY_CREPE = FOOD.register("wildberry_crepe",
            () -> new WildberryCrepeItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(0.8f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    public static final RegistryObject<Item> HONEY_CREPE = FOOD.register("honey_crepe",
            () -> new HoneyCrepeItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.7f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    public static final RegistryObject<Item> CHILLY_PLAIN_CREPE = FOOD.register("chilly_plain_crepe",
            () -> new ChillyPlainCrepeItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.7f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    public static final RegistryObject<Item> SPICY_PLAIN_CREPE = FOOD.register("spicy_plain_crepe",
            () -> new SpicyPlainCrepeItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(0.7f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    public static final RegistryObject<Item> ENERGIZING_PLAIN_CREPE = FOOD.register("energizing_plain_crepe",
            () -> new EnergizingPlainCrepeItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(0.7f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));

    //Fruitcake
    // Apple/Sweetberry/wildberry + Sugar + Wheat + Melonslice/chorusfruit
    public static final RegistryObject<Item> FRUITCAKE = FOOD.register("fruitcake",
            () -> new FruitcakeItem(new Item.Properties().food(new Food.Builder().nutrition(5)
                    .saturationMod(0.1f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    // Apple/Sweetberry/wildberry + Sugar + Wheat + Melonslice/chorusfruit + hydromelon
    public static final RegistryObject<Item> FRUITCAKE1 = FOOD.register("fruitcake1",
            () -> new FruitcakeItem(new Item.Properties().food(new Food.Builder().nutrition(6)
                    .saturationMod(0.2f).alwaysEat()
                    .build())));
    // Apple/Sweetberry/wildberry + Sugar + Wheat + Melonslice/chorusfruit + spicy pepper
    public static final RegistryObject<Item> FRUITCAKE2 = FOOD.register("fruitcake2",
            () -> new FruitcakeItem(new Item.Properties().food(new Food.Builder().nutrition(6)
                    .saturationMod(0.2f).alwaysEat()
                    .build())));
    // Apple/Sweetberry/wildberry + Sugar + Wheat + Melonslice/chorusfruit + stamella
    public static final RegistryObject<Item> FRUITCAKE3 = FOOD.register("fruitcake3",
            () -> new FruitcakeItem(new Item.Properties().food(new Food.Builder().nutrition(6)
                    .saturationMod(0.2f).alwaysEat()
                    .build())));

    //Mushroom Skewer (x1 any normal mushroom)
    public static final RegistryObject<Item> MUSHROOM_SKEWER = FOOD.register("mushroom_skewer",
            () -> new MushroomSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(2)
                    .saturationMod(0.6f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Mushroom Skewer (x2 any normal mushroom)
    public static final RegistryObject<Item> MUSHROOM_SKEWER1 = FOOD.register("mushroom_skewer1",
            () -> new MushroomSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(4)
                    .saturationMod(0.6f).alwaysEat()
                    .build())));
    //Mushroom Skewer (x3 any normal mushroom)
    public static final RegistryObject<Item> MUSHROOM_SKEWER2 = FOOD.register("mushroom_skewer2",
            () -> new MushroomSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(6)
                    .saturationMod(0.7f).alwaysEat()
                    .build())));
    //Mushroom Skewer (x4 any normal mushroom)
    public static final RegistryObject<Item> MUSHROOM_SKEWER3 = FOOD.register("mushroom_skewer3",
            () -> new MushroomSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    //Mushroom Skewer (x4 any normal mushroom)
    public static final RegistryObject<Item> MUSHROOM_SKEWER4 = FOOD.register("mushroom_skewer4",
            () -> new MushroomSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));

    //Energizing Mushroom Skewer (x1 Stamella Shroom)
    public static final RegistryObject<Item> ENERGIZING_MUSHROOM_SKEWER = FOOD.register("energizing_mushroom_skewer",
            () -> new EnergizingMushroomSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(2)
                    .saturationMod(0.6f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Energizing Mushroom Skewer (x2 Stamella Shroom)
    public static final RegistryObject<Item> ENERGIZING_MUSHROOM_SKEWER2 = FOOD.register("energizing_mushroom_skewer2",
            () -> new EnergizingMushroomSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(4)
                    .saturationMod(0.6f).alwaysEat()
                    .build())));
    //Energizing Mushroom Skewer (x3 Stamella Shroom)
    public static final RegistryObject<Item> ENERGIZING_MUSHROOM_SKEWER3 = FOOD.register("energizing_mushroom_skewer3",
            () -> new EnergizingMushroomSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(6)
                    .saturationMod(0.7f).alwaysEat()
                    .build())));
    //Energizing Mushroom Skewer (x4 Stamella Shroom)
    public static final RegistryObject<Item> ENERGIZING_MUSHROOM_SKEWER4 = FOOD.register("energizing_mushroom_skewer4",
            () -> new EnergizingMushroomSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(0.7f).alwaysEat()
                    .build())));
    //Energizing Mushroom Skewer (x5 Stamella Shroom)
    public static final RegistryObject<Item> ENERGIZING_MUSHROOM_SKEWER5 = FOOD.register("energizing_mushroom_skewer5",
            () -> new EnergizingMushroomSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    //Energizing Mushroom Skewer (Stamella + Generic)
    public static final RegistryObject<Item> ENERGIZING_MUSHROOM_SKEWER6 = FOOD.register("energizing_mushroom_skewer6",
            () -> new EnergizingMushroomSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(4)
                    .saturationMod(0.6f).alwaysEat()
                    .build())));
    //Energizing Mushroom Skewer (Stamella + Generic + Generic)
    public static final RegistryObject<Item> ENERGIZING_MUSHROOM_SKEWER7 = FOOD.register("energizing_mushroom_skewer7",
            () -> new EnergizingMushroomSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(6)
                    .saturationMod(0.6f).alwaysEat()
                    .build())));
    //Energizing Mushroom Skewer (Stamella + Generic + Generic)
    public static final RegistryObject<Item> ENERGIZING_MUSHROOM_SKEWER8 = FOOD.register("energizing_mushroom_skewer8",
            () -> new EnergizingMushroomSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(0.6f).alwaysEat()
                    .build())));
    //Energizing Mushroom Skewer (Stamella + Red OR Brown + Red OR Brown + Red OR Brown + Red OR Brown)
    public static final RegistryObject<Item> ENERGIZING_MUSHROOM_SKEWER9 = FOOD.register("energizing_mushroom_skewer9",
            () -> new EnergizingMushroomSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    //Energizing Mushroom Skewer (Stamella + Stamella + Generic)
    public static final RegistryObject<Item> ENERGIZING_MUSHROOM_SKEWER10 = FOOD.register("energizing_mushroom_skewer10",
            () -> new EnergizingMushroomSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(6)
                    .saturationMod(0.6f).alwaysEat()
                    .build())));
    //Energizing Mushroom Skewer (Stamella x 2 + Generic + Generic)
    public static final RegistryObject<Item> ENERGIZING_MUSHROOM_SKEWER11 = FOOD.register("energizing_mushroom_skewer11",
            () -> new EnergizingMushroomSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    //Energizing Mushroom Skewer (Stamella x 3 + Generic)
    public static final RegistryObject<Item> ENERGIZING_MUSHROOM_SKEWER12 = FOOD.register("energizing_mushroom_skewer12",
            () -> new EnergizingMushroomSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    //Energizing Mushroom Skewer (Stamella x 4 + Generic)
    public static final RegistryObject<Item> ENERGIZING_MUSHROOM_SKEWER13 = FOOD.register("energizing_mushroom_skewer13",
            () -> new EnergizingMushroomSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    //Energizing Mushroom Skewer (Stamella x 2 + Generic + Generic + Generic)
    public static final RegistryObject<Item> ENERGIZING_MUSHROOM_SKEWER14 = FOOD.register("energizing_mushroom_skewer14",
            () -> new EnergizingMushroomSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    //Energizing Mushroom Skewer (Stamella x 1 + Generic + Generic + Generic + Generic)
    public static final RegistryObject<Item> ENERGIZING_MUSHROOM_SKEWER15 = FOOD.register("energizing_mushroom_skewer15",
            () -> new EnergizingMushroomSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    //Energizing Mushroom Skewer (Stamella x 3 + Generic + Generic)
    public static final RegistryObject<Item> ENERGIZING_MUSHROOM_SKEWER16 = FOOD.register("energizing_mushroom_skewer16",
            () -> new EnergizingMushroomSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.6f).alwaysEat()
                    .build())));

    //Glazed Mushrooms
    public static final RegistryObject<Item> GLAZED_MUSHROOMS1 = FOOD.register("glazed_mushrooms1",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(6)
                    .saturationMod(0.7f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS2 = FOOD.register("glazed_mushrooms2",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(6)
                    .saturationMod(0.7f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS3 = FOOD.register("glazed_mushrooms3",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS4 = FOOD.register("glazed_mushrooms4",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(0.6f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS5 = FOOD.register("glazed_mushrooms5",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.5f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS6 = FOOD.register("glazed_mushrooms6",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(0.4f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS7 = FOOD.register("glazed_mushrooms7",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.5f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS8 = FOOD.register("glazed_mushrooms8",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.5f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS9 = FOOD.register("glazed_mushrooms9",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.5f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS10 = FOOD.register("glazed_mushrooms10",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(12)
                    .saturationMod(0.6f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS11 = FOOD.register("glazed_mushrooms11",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.5f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS12 = FOOD.register("glazed_mushrooms12",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(12)
                    .saturationMod(0.6f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS13 = FOOD.register("glazed_mushrooms13",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(14)
                    .saturationMod(0.7f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS14 = FOOD.register("glazed_mushrooms14",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.5f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS15 = FOOD.register("glazed_mushrooms15",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(12)
                    .saturationMod(0.6f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS16 = FOOD.register("glazed_mushrooms16",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(14)
                    .saturationMod(0.7f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS17 = FOOD.register("glazed_mushrooms17",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(12)
                    .saturationMod(0.6f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS18 = FOOD.register("glazed_mushrooms18",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(12)
                    .saturationMod(0.6f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS19 = FOOD.register("glazed_mushrooms19",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.5f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS20 = FOOD.register("glazed_mushrooms20",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(12)
                    .saturationMod(0.6f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS21 = FOOD.register("glazed_mushrooms21",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(14)
                    .saturationMod(0.7f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS22 = FOOD.register("glazed_mushrooms22",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(16)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS23 = FOOD.register("glazed_mushrooms23",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(12)
                    .saturationMod(0.6f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS24 = FOOD.register("glazed_mushrooms24",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(14)
                    .saturationMod(0.7f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS25 = FOOD.register("glazed_mushrooms25",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(16)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS26 = FOOD.register("glazed_mushrooms26",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(18)
                    .saturationMod(0.9f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS27 = FOOD.register("glazed_mushrooms27",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(12)
                    .saturationMod(0.6f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS28 = FOOD.register("glazed_mushrooms28",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(14)
                    .saturationMod(0.7f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS29 = FOOD.register("glazed_mushrooms29",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(16)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    public static final RegistryObject<Item> GLAZED_MUSHROOMS30 = FOOD.register("glazed_mushrooms30",
            () -> new GlazedMushroomsItem(new Item.Properties().food(new Food.Builder().nutrition(18)
                    .saturationMod(0.9f).alwaysEat()
                    .build())));

    //Meat Skewers
    //1 Meat
    public static final RegistryObject<Item> MEAT_SKEWER = FOOD.register("meat_skewer",
            () -> new MeatSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(4)
                    .saturationMod(0.6f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //2 Meat
    public static final RegistryObject<Item> MEAT_SKEWER1 = FOOD.register("meat_skewer1",
            () -> new MeatSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(0.7f).alwaysEat()
                    .build())));
    //3 Meat
    public static final RegistryObject<Item> MEAT_SKEWER2 = FOOD.register("meat_skewer2",
            () -> new MeatSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(12)
                    .saturationMod(0.8f).alwaysEat().fast()
                    .build())));
    //4 Meat
    public static final RegistryObject<Item> MEAT_SKEWER3 = FOOD.register("meat_skewer3",
            () -> new MeatSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(16)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    //5 Meat
    public static final RegistryObject<Item> MEAT_SKEWER4 = FOOD.register("meat_skewer4",
            () -> new MeatSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(20)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));

    //Spicy Meat Skewer (Pepper + Bird + Beef)
    public static final RegistryObject<Item> SPICY_MEAT_SKEWER = FOOD.register("spicy_meat_skewer",
            () -> new SpicyMeatSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.8f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Spicy Meat Skewer (Pepper + Bird + Beef + Pepper)
    public static final RegistryObject<Item> SPICY_MEAT_SKEWER2 = FOOD.register("spicy_meat_skewer2",
            () -> new SpicyMeatSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(14)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    //Spicy Meat Skewer (Pepper + Bird + Beef + Beef/Bird)
    public static final RegistryObject<Item> SPICY_MEAT_SKEWER3 = FOOD.register("spicy_meat_skewer3",
            () -> new SpicyMeatSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(14)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    //Spicy Meat Skewer (Pepper + Bird + Beef + Pepper + Pepper)
    public static final RegistryObject<Item> SPICY_MEAT_SKEWER4 = FOOD.register("spicy_meat_skewer4",
            () -> new SpicyMeatSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(14)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    //Spicy Meat Skewer (Pepper + Bird + Beef + Beef + Bird)
    public static final RegistryObject<Item> SPICY_MEAT_SKEWER5 = FOOD.register("spicy_meat_skewer5",
            () -> new SpicyMeatSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(18)
                    .saturationMod(0.9f).alwaysEat()
                    .build())));
    //Spicy Meat Skewer (Pepper + Bird + Beef + Beef + Pepper)
    public static final RegistryObject<Item> SPICY_MEAT_SKEWER6 = FOOD.register("spicy_meat_skewer6",
            () -> new SpicyMeatSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(16)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));

    //1 Chilly-tagged item + 1 meat
    public static final RegistryObject<Item> CHILLY_MEAT_SKEWER1 = FOOD.register("chilly_meat_skewer1",
            () -> new ChillyMeatSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(6)
                    .saturationMod(0.8f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //2 Chilly-tagged items + 1 meat
    public static final RegistryObject<Item> CHILLY_MEAT_SKEWER2 = FOOD.register("chilly_meat_skewer2",
            () -> new ChillyMeatSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    //1 Chilly-tagged item + 2 meats
    public static final RegistryObject<Item> CHILLY_MEAT_SKEWER3 = FOOD.register("chilly_meat_skewer3",
            () -> new ChillyMeatSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    //3 Chilly-tagged items + 1 meats
    public static final RegistryObject<Item> CHILLY_MEAT_SKEWER4 = FOOD.register("chilly_meat_skewer4",
            () -> new ChillyMeatSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    //1 Chilly-tagged item + 2 meats
    public static final RegistryObject<Item> CHILLY_MEAT_SKEWER5 = FOOD.register("chilly_meat_skewer5",
            () -> new ChillyMeatSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(12)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    //2 Chilly-tagged items + 2 meats
    public static final RegistryObject<Item> CHILLY_MEAT_SKEWER6 = FOOD.register("chilly_meat_skewer6",
            () -> new ChillyMeatSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(14)
                    .saturationMod(0.8f).alwaysEat().meat()
                    .build())));
    //4 Chilly-tagged items + 1 meats
    public static final RegistryObject<Item> CHILLY_MEAT_SKEWER7 = FOOD.register("chilly_meat_skewer7",
            () -> new ChillyMeatSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(12)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    //3 Chilly-tagged items + 2 meats
    public static final RegistryObject<Item> CHILLY_MEAT_SKEWER8 = FOOD.register("chilly_meat_skewer8",
            () -> new ChillyMeatSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(14)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    //2 Chilly-tagged items + 3 meats
    public static final RegistryObject<Item> CHILLY_MEAT_SKEWER9 = FOOD.register("chilly_meat_skewer9",
            () -> new ChillyMeatSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(16)
                    .saturationMod(0.9f).alwaysEat()
                    .build())));
    //1 Chilly-tagged items + 4 meats
    public static final RegistryObject<Item> CHILLY_MEAT_SKEWER10 = FOOD.register("chilly_meat_skewer10",
            () -> new ChillyMeatSkewerItem(new Item.Properties().food(new Food.Builder().nutrition(18)
                    .saturationMod(0.9f).alwaysEat()
                    .build())));

    //Spicy Pepper Steak (Pepper + Beef)
    public static final RegistryObject<Item> SPICY_PEPPER_STEAK = FOOD.register("spicy_pepper_steak",
            () -> new SpicyPepperSteakItem(new Item.Properties().food(new Food.Builder().nutrition(6)
                    .saturationMod(0.8f).alwaysEat()
                    .build()).tab(ZArrowsMain.FOOD_TAB)));
    //Spicy Pepper Steak (Pepper + Beef + Pepper)
    public static final RegistryObject<Item> SPICY_PEPPER_STEAK1 = FOOD.register("spicy_pepper_steak1",
            () -> new SpicyPepperSteakItem(new Item.Properties().food(new Food.Builder().nutrition(8)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    //Spicy Pepper Steak (Pepper + Beef + Beef)
    public static final RegistryObject<Item> SPICY_PEPPER_STEAK2 = FOOD.register("spicy_pepper_steak2",
            () -> new SpicyPepperSteakItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    //Spicy Pepper Steak (Pepper + Beef + Beef + Pepper)
    public static final RegistryObject<Item> SPICY_PEPPER_STEAK3 = FOOD.register("spicy_pepper_steak3",
            () -> new SpicyPepperSteakItem(new Item.Properties().food(new Food.Builder().nutrition(12)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    //Spicy Pepper Steak (Pepper + Beef + Beef + Beef)
    public static final RegistryObject<Item> SPICY_PEPPER_STEAK4 = FOOD.register("spicy_pepper_steak4",
            () -> new SpicyPepperSteakItem(new Item.Properties().food(new Food.Builder().nutrition(14)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    //Spicy Pepper Steak (Pepper + Beef + Beef + Pepper + Pepper)
    public static final RegistryObject<Item> SPICY_PEPPER_STEAK5 = FOOD.register("spicy_pepper_steak5",
            () -> new SpicyPepperSteakItem(new Item.Properties().food(new Food.Builder().nutrition(14)
                    .saturationMod(0.9f).alwaysEat()
                    .build())));
    //Spicy Pepper Steak (Pepper + Beef + Beef + Beef + Beef)
    public static final RegistryObject<Item> SPICY_PEPPER_STEAK6 = FOOD.register("spicy_pepper_steak6",
            () -> new SpicyPepperSteakItem(new Item.Properties().food(new Food.Builder().nutrition(18)
                    .saturationMod(0.9f).alwaysEat()
                    .build())));
    //Spicy Pepper Steak (Pepper + Beef + Beef + Beef + Pepper)
    public static final RegistryObject<Item> SPICY_PEPPER_STEAK7 = FOOD.register("spicy_pepper_steak7",
            () -> new SpicyPepperSteakItem(new Item.Properties().food(new Food.Builder().nutrition(16)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    //Spicy Pepper Steak (Pepper + Pepper + Beef + Pepper)
    public static final RegistryObject<Item> SPICY_PEPPER_STEAK8 = FOOD.register("spicy_pepper_steak8",
            () -> new SpicyPepperSteakItem(new Item.Properties().food(new Food.Builder().nutrition(10)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));
    //Spicy Pepper Steak (Pepper + Pepper + Beef + Pepper + Pepper)
    public static final RegistryObject<Item> SPICY_PEPPER_STEAK9 = FOOD.register("spicy_pepper_steak9",
            () -> new SpicyPepperSteakItem(new Item.Properties().food(new Food.Builder().nutrition(12)
                    .saturationMod(0.8f).alwaysEat()
                    .build())));


}
