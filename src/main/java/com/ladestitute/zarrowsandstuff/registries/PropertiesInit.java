package com.ladestitute.zarrowsandstuff.registries;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraftforge.common.ToolType;

public class PropertiesInit {
    public static final AbstractBlock.Properties ANCIENTTECH = AbstractBlock.Properties.of(Material.STONE)
            .strength(5f, 5f)
            .sound(SoundType.STONE)
            .harvestLevel(1)
            .harvestTool(ToolType.PICKAXE)
            .requiresCorrectToolForDrops();

    public static final AbstractBlock.Properties WRECKAGE = AbstractBlock.Properties.of(Material.METAL)
            .strength(4f, 3.25f)
            .sound(SoundType.METAL)
            .harvestLevel(2)
            .harvestTool(ToolType.PICKAXE)
            .requiresCorrectToolForDrops();

    public static final AbstractBlock.Properties SNOWSTONE = AbstractBlock.Properties.of(Material.STONE)
            .strength(0.8f, 0.8f)
            .sound(SoundType.STONE)
            .harvestLevel(0)
            .harvestTool(ToolType.PICKAXE)
            .requiresCorrectToolForDrops();

    public static final AbstractBlock.Properties ORE_DEPOSIT = AbstractBlock.Properties.of(Material.STONE)
            .strength(5f, 5f)
            .sound(SoundType.STONE)
            .harvestLevel(1)
            .harvestTool(ToolType.PICKAXE)
            .requiresCorrectToolForDrops()
            .noOcclusion();

    public static final AbstractBlock.Properties PALM_WOOD = AbstractBlock.Properties.of(Material.WOOD)
            .strength(2f, 2f)
            .sound(SoundType.WOOD)
            .harvestLevel(1)
            .harvestTool(ToolType.AXE)
            .requiresCorrectToolForDrops()
            .noOcclusion();

    public static final AbstractBlock.Properties DEPLETED_ORE_DEPOSIT = AbstractBlock.Properties.of(Material.STONE)
            .strength(22f, 3f)
            .sound(SoundType.STONE)
            .harvestLevel(1)
            .harvestTool(ToolType.PICKAXE)
            .requiresCorrectToolForDrops()
            .noOcclusion();

    public static final AbstractBlock.Properties SPICY_PEPPER = AbstractBlock.Properties.of(Material.PLANT)
            .strength(0.0f, 0.0F)
            .sound(SoundType.GRASS)
            .noCollission()
            .randomTicks();

    public static final AbstractBlock.Properties H_STEM = AbstractBlock.Properties.of(Material.PLANT)
            .strength(0f, 0f)
            .sound(SoundType.HARD_CROP)
            .noCollission()
            .randomTicks();

    public static final AbstractBlock.Properties HYDROMELON = AbstractBlock.Properties.of(Material.PLANT)
            .strength(1f, 1f)
            .harvestLevel(0)
            .sound(SoundType.HARD_CROP)
            .harvestTool(ToolType.AXE);

    public static final AbstractBlock.Properties PALM_LEAVES = AbstractBlock.Properties.of(Material.GRASS)
            .strength(0.2f, 0.2f)
            .sound(SoundType.GRASS)
            .harvestLevel(0)
            .noOcclusion()
            .requiresCorrectToolForDrops();

    public static final AbstractBlock.Properties VOLTFRUIT_CACTI = AbstractBlock.Properties.of(Material.CACTUS)
            .strength(0.4f, 0.4f)
            .sound(SoundType.WOOL)
            .harvestLevel(0)
            .requiresCorrectToolForDrops();

}
