package com.ladestitute.zarrowsandstuff.registries;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.blocks.AncientCryostoneBlock;
import com.ladestitute.zarrowsandstuff.blocks.AncientOvenBlock;
import com.ladestitute.zarrowsandstuff.blocks.natural.DepletedOreDepositBlock;
import com.ladestitute.zarrowsandstuff.blocks.natural.DepletedRareOreDepositBlock;
import com.ladestitute.zarrowsandstuff.blocks.natural.plants.hydromelon.HydromelonAttachedStemBlock;
import com.ladestitute.zarrowsandstuff.blocks.natural.plants.hydromelon.HydromelonStemBlock;
import net.minecraft.block.Block;
import net.minecraft.block.StemGrownBlock;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class SpecialBlocksInit {
    //An alt block registering class for blocks we want to separate into other tabs, manually set itemblocks, etc

    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS,
            ZArrowsMain.MOD_ID);

    public static final RegistryObject<Block> DEPLETED_ORE_DEPOSIT =
            BLOCKS.register("depleted_ore_deposit", () -> new DepletedOreDepositBlock(-0x72675B, PropertiesInit.DEPLETED_ORE_DEPOSIT));
    public static final RegistryObject<Block> DEPLETED_RARE_ORE_DEPOSIT =
            BLOCKS.register("depleted_rare_ore_deposit", () -> new DepletedRareOreDepositBlock(-0x72675B, PropertiesInit.DEPLETED_ORE_DEPOSIT));

    //Natural/plants/etc
    public static final RegistryObject<Block> HYDROMELON_STEM = BLOCKS.register("hydromelon_stem",
            () -> new HydromelonStemBlock((StemGrownBlock) BlockInit.HYDROMELON.get(), PropertiesInit.H_STEM));
    public static final RegistryObject<Block> HATTACHED_STEM = BLOCKS.register("attached_hydromelon_stem",
            () -> new HydromelonAttachedStemBlock((StemGrownBlock) BlockInit.HYDROMELON.get(), PropertiesInit.H_STEM));

    //Utilities
    public static final RegistryObject<Block> ANCIENT_OVEN = BLOCKS.register("ancient_oven",
            () -> new AncientOvenBlock(PropertiesInit.ANCIENTTECH));
    public static final RegistryObject<Block> ANCIENT_CRYOSTONE = BLOCKS.register("ancient_cryostone",
            () -> new AncientCryostoneBlock(PropertiesInit.ANCIENTTECH));
}
