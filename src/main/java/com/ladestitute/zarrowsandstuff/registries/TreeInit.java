package com.ladestitute.zarrowsandstuff.registries;

import com.ladestitute.zarrowsandstuff.world.spawner.MightyBananasTreeSpawner;
import net.minecraft.world.gen.feature.Feature;

import java.util.Random;

@SuppressWarnings("rawtypes")
public class TreeInit {
    public static final MightyBananasTreeSpawner MIGHTY_BANANAS_TREE = new MightyBananasTreeSpawner() {
        @Override
        protected Feature getFeature(Random random) {
            return FeatureInit.BANANA_PLANT.get();
        }
    };
}
