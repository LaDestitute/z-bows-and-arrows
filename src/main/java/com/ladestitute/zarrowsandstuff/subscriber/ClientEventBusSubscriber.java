package com.ladestitute.zarrowsandstuff.subscriber;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.client.render.arrows.*;
import com.ladestitute.zarrowsandstuff.entities.mob.render.ChuchuRenderer;
import com.ladestitute.zarrowsandstuff.entities.mob.render.CourserBeeRenderer;
import com.ladestitute.zarrowsandstuff.entities.mob.render.ElectricChuchuRenderer;
import com.ladestitute.zarrowsandstuff.entities.mob.render.IceChuchuRenderer;
import com.ladestitute.zarrowsandstuff.registries.BlockInit;
import com.ladestitute.zarrowsandstuff.registries.EntityInit;
import com.ladestitute.zarrowsandstuff.registries.SpecialBlocksInit;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

@Mod.EventBusSubscriber(modid = ZArrowsMain.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class ClientEventBusSubscriber {

    @SubscribeEvent
    public static void onStaticClientSetup(FMLClientSetupEvent event) {
        event.setPhase(EventPriority.HIGH);
        RenderTypeLookup.setRenderLayer(BlockInit.ORE_DEPOSIT.get(), RenderType.translucent());
        RenderTypeLookup.setRenderLayer(BlockInit.PALM_LOG.get(), RenderType.translucent());
        RenderTypeLookup.setRenderLayer(BlockInit.SPICY_PEPPER.get(), RenderType.cutout());
        RenderTypeLookup.setRenderLayer(BlockInit.VOLTFRUIT_PLANT.get(), RenderType.cutout());
        RenderTypeLookup.setRenderLayer(BlockInit.STAMELLA_SHROOM.get(), RenderType.cutout());
        RenderTypeLookup.setRenderLayer(BlockInit.OASIS_GRASS.get(), RenderType.cutout());
        RenderTypeLookup.setRenderLayer(SpecialBlocksInit.HATTACHED_STEM.get(), RenderType.cutout());
        RenderTypeLookup.setRenderLayer(SpecialBlocksInit.HYDROMELON_STEM.get(), RenderType.cutout());
        RenderTypeLookup.setRenderLayer(BlockInit.EMPTY_SPICY_PEPPER.get(), RenderType.cutout());
        RenderTypeLookup.setRenderLayer(BlockInit.WILDBERRY_BUSH.get(), RenderType.cutout());
        RenderTypeLookup.setRenderLayer(BlockInit.MIGHTY_BANANAS_BLOCK.get(), RenderType.translucent());
        RenderTypeLookup.setRenderLayer(BlockInit.PALM_FRUIT_BLOCK.get(), RenderType.translucent());
        RenderTypeLookup.setRenderLayer(BlockInit.MIGHTY_BANANAS_LEAVES.get(), RenderType.cutout());
        RenderTypeLookup.setRenderLayer(BlockInit.MIGHTY_BANANAS_LEAVES_SIDE.get(), RenderType.cutout());
        RenderingRegistry.registerEntityRenderingHandler(EntityInit.FIRE_ARROW.get(), FireArrowRender::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityInit.ICE_ARROW.get(), IceArrowRender::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityInit.SHOCK_ARROW.get(), ShockArrowRender::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityInit.BOMB_ARROW.get(), BombArrowRender::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityInit.ANCIENT_ARROW.get(), AncientArrowRender::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityInit.LIGHT_ARROW.get(), LightArrowRender::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityInit.COURSER_BEE.get(), CourserBeeRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityInit.CHUCHU.get(), ChuchuRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityInit.ICE_CHUCHU.get(), IceChuchuRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityInit.ELECTRIC_CHUCHU.get(), ElectricChuchuRenderer::new);
    }
}
