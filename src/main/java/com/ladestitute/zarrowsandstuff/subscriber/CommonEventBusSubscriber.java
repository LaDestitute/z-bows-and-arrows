package com.ladestitute.zarrowsandstuff.subscriber;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.entities.mob.ChuchuEntity;
import com.ladestitute.zarrowsandstuff.entities.mob.CourserBeeEntity;
import com.ladestitute.zarrowsandstuff.entities.mob.ElectricChuchuEntity;
import com.ladestitute.zarrowsandstuff.entities.mob.IceChuchuEntity;
import com.ladestitute.zarrowsandstuff.registries.EffectInit;
import com.ladestitute.zarrowsandstuff.registries.EntityInit;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import com.ladestitute.zarrowsandstuff.capabilityzarrows.IPlayerDataCapability;
import com.ladestitute.zarrowsandstuff.capabilityzarrows.PlayerDataCapabilityFactory;
import com.ladestitute.zarrowsandstuff.capabilityzarrows.PlayerDataCapabilityHandler;
import com.ladestitute.zarrowsandstuff.capabilityzarrows.PlayerDataCapabilityStorage;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.potion.PotionUtils;
import net.minecraft.potion.Potions;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.brewing.BrewingRecipeRegistry;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.event.entity.EntityAttributeCreationEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;

@Mod.EventBusSubscriber(modid = ZArrowsMain.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class CommonEventBusSubscriber {

    @SubscribeEvent
    public static void onStaticCommonSetup(FMLCommonSetupEvent event) {

        CapabilityManager.INSTANCE.register(IPlayerDataCapability.class, new PlayerDataCapabilityStorage(), PlayerDataCapabilityFactory::new);

        MinecraftForge.EVENT_BUS.register(new PlayerDataCapabilityHandler());

        BrewingRecipeRegistry.addRecipe(Ingredient.of(PotionUtils.setPotion(new ItemStack(Items.POTION),
                Potions.AWKWARD)), Ingredient.of(ItemInit.SPICY_PEPPER.get()), PotionUtils.setPotion(new ItemStack(Items.POTION),
                EffectInit.COLD_RESIST_POTION.get()));
        BrewingRecipeRegistry.addRecipe(Ingredient.of(PotionUtils.setPotion(new ItemStack(Items.POTION),
                EffectInit.COLD_RESIST_POTION.get())), Ingredient.of(Items.GLOWSTONE_DUST), PotionUtils.setPotion(new ItemStack(Items.POTION),
                EffectInit.COLD_RESIST_POTION.get()));
        BrewingRecipeRegistry.addRecipe(Ingredient.of(PotionUtils.setPotion(new ItemStack(Items.POTION),
                Potions.AWKWARD)), Ingredient.of(ItemInit.HYDROMELON_FRUIT.get()), PotionUtils.setPotion(new ItemStack(Items.POTION),
                EffectInit.HEAT_RESIST_POTION.get()));
        BrewingRecipeRegistry.addRecipe(Ingredient.of(PotionUtils.setPotion(new ItemStack(Items.POTION),
                EffectInit.HEAT_RESIST_POTION.get())), Ingredient.of(Items.GLOWSTONE_DUST), PotionUtils.setPotion(new ItemStack(Items.POTION),
                EffectInit.HEAT_RESIST_POTION.get()));

    }

    @SubscribeEvent
    public static void addEntityAttributes(EntityAttributeCreationEvent event) {
        event.put(EntityInit.COURSER_BEE.get(), CourserBeeEntity.createAttributes().build());
        event.put(EntityInit.CHUCHU.get(), ChuchuEntity.createAttributes().build());
        event.put(EntityInit.ICE_CHUCHU.get(), IceChuchuEntity.createAttributes().build());
        event.put(EntityInit.ELECTRIC_CHUCHU.get(), ElectricChuchuEntity.createAttributes().build());
    }

    }
