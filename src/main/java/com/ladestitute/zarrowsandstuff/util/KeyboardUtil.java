package com.ladestitute.zarrowsandstuff.util;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.client.util.InputMappings;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import org.lwjgl.glfw.GLFW;

import java.awt.event.KeyEvent;

public class KeyboardUtil {

    public static KeyBinding isHoldingEnter;
    public static KeyBinding isHoldingX;
    public static KeyBinding isHoldingR;

    public static void register(final FMLClientSetupEvent event) {
        isHoldingEnter = create("is_holding_enter_key", KeyEvent.VK_ENTER);
        isHoldingX = create("is_holding_x_key", KeyEvent.VK_X);
        isHoldingR = create("is_holding_r_key", KeyEvent.VK_R);

        ClientRegistry.registerKeyBinding(isHoldingEnter);
        ClientRegistry.registerKeyBinding(isHoldingX);
        ClientRegistry.registerKeyBinding(isHoldingR);
    }

    private static KeyBinding create(String name, int key) {
        return new KeyBinding("key." + ZArrowsMain.MOD_ID + "." + name, key, "key.category." + ZArrowsMain.MOD_ID);
    }

}
