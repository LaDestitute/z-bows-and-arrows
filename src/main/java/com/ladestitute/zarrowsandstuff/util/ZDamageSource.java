package com.ladestitute.zarrowsandstuff.util;

import net.minecraft.entity.LivingEntity;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSource;

public class ZDamageSource extends DamageSource {
    public static DamageSource ancient_arrow = new ZDamageSource("ancient_arrow").setMagic();

    public ZDamageSource(String damageTypeIn) {
        super(damageTypeIn);
    }

    public static DamageSource causeAncientDamage(LivingEntity living)
    {
        return new EntityDamageSource("ancient_arrow", living);
    }
}
