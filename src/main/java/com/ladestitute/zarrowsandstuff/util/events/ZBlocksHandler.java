package com.ladestitute.zarrowsandstuff.util.events;

import com.ladestitute.zarrowsandstuff.blocks.natural.plants.hydromelon.HydromelonBlock;
import com.ladestitute.zarrowsandstuff.registries.BlockInit;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import com.ladestitute.zarrowsandstuff.registries.SpecialBlocksInit;
import net.minecraft.block.*;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.items.ItemHandlerHelper;

public class ZBlocksHandler {

    @SubscribeEvent
    public void cutmelon(PlayerInteractEvent.RightClickBlock event) {
        BlockPos plantpos = event.getPos();
        BlockState state = event.getWorld().getBlockState(plantpos);
        if (event.getWorld().isClientSide) {
            return;
        }
        if (state.getBlock() instanceof StonecutterBlock) {
            if (event.getItemStack().getItem() == ItemInit.HYDROMELON_FRUIT.get()) {
                ItemStack pepperseed = ItemInit.HYDROMELON_FRUIT.get().getDefaultInstance();
                if (event.getPlayer().inventory.contains(pepperseed))
                    for (ItemStack seed : event.getPlayer().inventory.items)
                        if (seed.getItem() == ItemInit.HYDROMELON_FRUIT.get()) {
                            seed.shrink(1);
                            ItemHandlerHelper.giveItemToPlayer(event.getPlayer(), ItemInit.HYDROMELON_SLICE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(event.getPlayer(), ItemInit.HYDROMELON_SLICE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(event.getPlayer(), ItemInit.HYDROMELON_SLICE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(event.getPlayer(), ItemInit.HYDROMELON_SLICE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(event.getPlayer(), ItemInit.HYDROMELON_SLICE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(event.getPlayer(), ItemInit.HYDROMELON_SLICE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(event.getPlayer(), ItemInit.HYDROMELON_SLICE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(event.getPlayer(), ItemInit.HYDROMELON_SLICE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(event.getPlayer(), ItemInit.HYDROMELON_SLICE.get().getDefaultInstance());
                        }
            }
        }
    }

    @SubscribeEvent
    public void cutmelon2(PlayerInteractEvent.RightClickBlock event) {
        BlockPos plantpos = event.getPos();
        BlockState state = event.getWorld().getBlockState(plantpos);
        if (event.getWorld().isClientSide) {
            return;
        }
        if (state.getBlock() instanceof HydromelonBlock) {
            if (event.getItemStack().getItem() == Items.SHEARS || event.getItemStack().getItem() == Items.WOODEN_AXE || event.getItemStack().getItem() == Items.STONE_AXE ||
                    event.getItemStack().getItem() == Items.IRON_AXE || event.getItemStack().getItem() == Items.GOLDEN_AXE ||
                    event.getItemStack().getItem() == Items.DIAMOND_AXE || event.getItemStack().getItem() == Items.NETHERITE_AXE ||
                    event.getItemStack().getItem() == Items.WOODEN_SWORD || event.getItemStack().getItem() == Items.STONE_SWORD ||
                    event.getItemStack().getItem() == Items.IRON_SWORD || event.getItemStack().getItem() == Items.GOLDEN_SWORD ||
                    event.getItemStack().getItem() == Items.DIAMOND_SWORD || event.getItemStack().getItem() == Items.NETHERITE_SWORD) {
                            event.getWorld().destroyBlock(plantpos, false, event.getEntityLiving());
                            ItemHandlerHelper.giveItemToPlayer(event.getPlayer(), ItemInit.HYDROMELON_SLICE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(event.getPlayer(), ItemInit.HYDROMELON_SLICE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(event.getPlayer(), ItemInit.HYDROMELON_SLICE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(event.getPlayer(), ItemInit.HYDROMELON_SLICE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(event.getPlayer(), ItemInit.HYDROMELON_SLICE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(event.getPlayer(), ItemInit.HYDROMELON_SLICE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(event.getPlayer(), ItemInit.HYDROMELON_SLICE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(event.getPlayer(), ItemInit.HYDROMELON_SLICE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(event.getPlayer(), ItemInit.HYDROMELON_SLICE.get().getDefaultInstance());
                        }
            }
        }


    @SubscribeEvent
    public void plantpeppers(PlayerInteractEvent.RightClickBlock event) {
        BlockPos plantpos = event.getPos();
        BlockState state = event.getWorld().getBlockState(plantpos);
        if (event.getWorld().isClientSide) {
            return;
        }
        if (state.getBlock() instanceof GrassBlock || state.getBlock() instanceof FarmlandBlock) {
            if (event.getItemStack().getItem() == ItemInit.SPICY_PEPPER_SEED.get()) {
                BlockPos pepperplant = event.getPos().above();
                event.getWorld().setBlockAndUpdate(pepperplant, BlockInit.EMPTY_SPICY_PEPPER.get().defaultBlockState());

                ItemStack pepperseed = ItemInit.SPICY_PEPPER_SEED.get().getDefaultInstance();
                if (event.getPlayer().inventory.contains(pepperseed))
                    for (ItemStack seed : event.getPlayer().inventory.items)
                        if (seed.getItem() == ItemInit.SPICY_PEPPER_SEED.get()) {
                            seed.shrink(1);
                        }
            }
            }
        }

    @SubscribeEvent
    public void planthydromelons(PlayerInteractEvent.RightClickBlock event) {
        BlockPos plantpos = event.getPos();
        BlockState state = event.getWorld().getBlockState(plantpos);
        if (event.getWorld().isClientSide) {
            return;
        }
        if (state.getBlock() instanceof GrassBlock || state.getBlock() instanceof SandBlock || state.getBlock() instanceof FarmlandBlock) {
            if (event.getItemStack().getItem() == ItemInit.HYDROMELON_SEED.get()) {
                BlockPos pepperplant = event.getPos().above();
                LivingEntity player = event.getPlayer();
                event.getWorld().setBlockAndUpdate(pepperplant, SpecialBlocksInit.HYDROMELON_STEM.get().defaultBlockState());

                ItemStack hydromelon = ItemInit.HYDROMELON_SEED.get().getDefaultInstance();
                if (event.getPlayer().inventory.contains(hydromelon))
                    for (ItemStack seed : event.getPlayer().inventory.items)
                        if (seed.getItem() == ItemInit.HYDROMELON_SEED.get()) {
                            seed.shrink(1);
                        }
            }
        }
    }
}
