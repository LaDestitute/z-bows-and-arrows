package com.ladestitute.zarrowsandstuff.util.events;

import com.ladestitute.zarrowsandstuff.entities.arrows.EntityAncientArrow;
import com.ladestitute.zarrowsandstuff.entities.arrows.EntityFireArrow;
import com.ladestitute.zarrowsandstuff.entities.arrows.EntityIceArrow;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.monster.BlazeEntity;
import net.minecraft.entity.passive.*;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.Random;

public class ZMobDropsHandler {

    @SubscribeEvent
    public void blazekiller(LivingHurtEvent event) {
        Entity sourceEntity = event.getSource().getDirectEntity();

        if (ZConfigManager.getInstance().oneshotblazes() && sourceEntity instanceof EntityIceArrow && event.getEntityLiving() instanceof BlazeEntity) {
            event.setAmount(10000F);
        }
    }

    @SubscribeEvent
    public void onMobDrops(LivingDropsEvent event) {
        Entity sourceEntity = event.getSource().getDirectEntity();
        if (sourceEntity instanceof EntityAncientArrow) {
            event.getDrops().clear();
        }
    }

    @SubscribeEvent
    public void firearrowfixDrops(LivingDropsEvent event) {
        Entity sourceEntity = event.getSource().getDirectEntity();
        if (sourceEntity instanceof EntityFireArrow && !ZConfigManager.getInstance().allowFireArrowDOT()) {
            Random rand1 = new Random();
            int hidedrop = rand1.nextInt(2);
            int bonusdrop = rand1.nextInt(3);
            event.getDrops().clear();
            if(event.getEntityLiving() instanceof SheepEntity)
            {
                ItemStack itemStackToDrop = new ItemStack(Items.COOKED_MUTTON, 1+bonusdrop);
                ItemStack itemStackToDropWool = new ItemStack(Items.WHITE_WOOL, 1);
                event.getDrops().add(new ItemEntity(event.getEntityLiving().level, event.getEntityLiving().position().x,
                        event.getEntityLiving().position().y, event.getEntityLiving().position().z, itemStackToDrop));
                event.getDrops().add(new ItemEntity(event.getEntityLiving().level, event.getEntityLiving().position().x,
                        event.getEntityLiving().position().y, event.getEntityLiving().position().z, itemStackToDropWool));
            }
            if(event.getEntityLiving() instanceof RabbitEntity)
            {
                ItemStack itemStackToDrop = new ItemStack(Items.COOKED_RABBIT, 1);
                ItemStack itemStackToDropHide = new ItemStack(Items.RABBIT_HIDE, 1);
                event.getDrops().add(new ItemEntity(event.getEntityLiving().level, event.getEntityLiving().position().x,
                        event.getEntityLiving().position().y, event.getEntityLiving().position().z, itemStackToDrop));
                if(hidedrop == 1)
                {
                    event.getDrops().add(new ItemEntity(event.getEntityLiving().level, event.getEntityLiving().position().x,
                            event.getEntityLiving().position().y, event.getEntityLiving().position().z, itemStackToDropHide));
                }
            }
            if(event.getEntityLiving() instanceof PigEntity)
            {
                ItemStack itemStackToDrop = new ItemStack(Items.COOKED_PORKCHOP, 1+bonusdrop);
                event.getDrops().add(new ItemEntity(event.getEntityLiving().level, event.getEntityLiving().position().x,
                        event.getEntityLiving().position().y, event.getEntityLiving().position().z, itemStackToDrop));
            }
            if(event.getEntityLiving() instanceof ChickenEntity)
            {
                ItemStack itemStackToDrop = new ItemStack(Items.COOKED_CHICKEN, 1);
                ItemStack itemStackToDropHide = new ItemStack(Items.FEATHER, 1+bonusdrop);
                event.getDrops().add(new ItemEntity(event.getEntityLiving().level, event.getEntityLiving().position().x,
                        event.getEntityLiving().position().y, event.getEntityLiving().position().z, itemStackToDrop));
                if(hidedrop == 1)
                {
                    event.getDrops().add(new ItemEntity(event.getEntityLiving().level, event.getEntityLiving().position().x,
                            event.getEntityLiving().position().y, event.getEntityLiving().position().z, itemStackToDropHide));
                }
            }
            if(event.getEntityLiving() instanceof CowEntity)
            {
                ItemStack itemStackToDrop = new ItemStack(Items.COOKED_BEEF, 1+bonusdrop);
                ItemStack itemStackToDropHide = new ItemStack(Items.LEATHER, 1+bonusdrop);
                event.getDrops().add(new ItemEntity(event.getEntityLiving().level, event.getEntityLiving().position().x,
                        event.getEntityLiving().position().y, event.getEntityLiving().position().z, itemStackToDrop));
                if(hidedrop == 1)
                {
                    event.getDrops().add(new ItemEntity(event.getEntityLiving().level, event.getEntityLiving().position().x,
                            event.getEntityLiving().position().y, event.getEntityLiving().position().z, itemStackToDropHide));
                }
            }
        }
    }

    @SubscribeEvent
            public void topazthudner(LivingDamageEvent event)
    {
        ItemStack helmstack = event.getEntityLiving().getItemBySlot(EquipmentSlotType.HEAD);
        if(event.getSource().msgId == DamageSource.LIGHTNING_BOLT.msgId &&
        event.getEntityLiving() instanceof PlayerEntity && event.getEntityLiving().hasItemInSlot(EquipmentSlotType.HEAD))
        if(helmstack.getItem() == ItemInit.TOPAZ_EARRINGS.get())
        {
            event.setAmount(event.getAmount()/2);
        }
        if(helmstack.getItem() == ItemInit.UPGRADED_TOPAZ_EARRINGS.get())
        {
            event.setAmount(event.getAmount()/4);
        }
    }
}

