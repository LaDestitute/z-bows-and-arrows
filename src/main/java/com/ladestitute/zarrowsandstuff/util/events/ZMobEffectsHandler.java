package com.ladestitute.zarrowsandstuff.util.events;

import com.ladestitute.zarrowsandstuff.entities.arrows.EntityIceArrow;
import com.ladestitute.zarrowsandstuff.entities.arrows.EntityShockArrow;
import com.ladestitute.zarrowsandstuff.entities.mob.ChuchuEntity;
import com.ladestitute.zarrowsandstuff.entities.mob.ElectricChuchuEntity;
import com.ladestitute.zarrowsandstuff.entities.mob.IceChuchuEntity;
import com.ladestitute.zarrowsandstuff.registries.EntityInit;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import com.ladestitute.zarrowsandstuff.registries.SoundInit;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.particles.IParticleData;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.List;
import java.util.Random;

public class ZMobEffectsHandler {

    @SubscribeEvent
    public void chuchueffects(LivingDamageEvent event) {
        if(event.getAmount() <= 14F) {
            Entity sourceEntity = event.getSource().getDirectEntity();
            if (event.getEntityLiving() instanceof ChuchuEntity ||
                    event.getEntityLiving() instanceof IceChuchuEntity||
                    event.getEntityLiving() instanceof ElectricChuchuEntity) {
                if (sourceEntity instanceof AbstractArrowEntity) {
                    event.getEntityLiving().addEffect(new EffectInstance(Effects.MOVEMENT_SLOWDOWN, 40, 10));
                }
            }
            if (event.getEntityLiving() instanceof ChuchuEntity) {
                if (sourceEntity instanceof EntityIceArrow) {
                    event.getEntityLiving().remove();
                    IceChuchuEntity chuchu = new IceChuchuEntity(EntityInit.ICE_CHUCHU.get(), event.getEntityLiving().level);
                    chuchu.setPos(event.getEntityLiving().position().x, event.getEntityLiving().position().y, event.getEntityLiving().position().z);
                    event.getEntityLiving().level.addFreshEntity(chuchu);
                }
                if (sourceEntity instanceof EntityShockArrow) {
                    event.getEntityLiving().remove();
                    ElectricChuchuEntity chuchu = new ElectricChuchuEntity(EntityInit.ELECTRIC_CHUCHU.get(), event.getEntityLiving().level);
                    chuchu.setPos(event.getEntityLiving().position().x, event.getEntityLiving().position().y, event.getEntityLiving().position().z);
                    event.getEntityLiving().level.addFreshEntity(chuchu);
                }
            }
        }
    }

    @SubscribeEvent
    public void chuchudeathsplit(LivingDeathEvent event) {
        if (event.getEntityLiving() instanceof ChuchuEntity) {
            Random rand = new Random();
            int splitchance = rand.nextInt(4);
            if (splitchance == 0) {
                ChuchuEntity chuchu1 = new ChuchuEntity(EntityInit.CHUCHU.get(), event.getEntityLiving().level);
                chuchu1.setPos(event.getEntityLiving().position().x, event.getEntityLiving().position().y, event.getEntityLiving().position().z);
                ChuchuEntity chuchu2 = new ChuchuEntity(EntityInit.CHUCHU.get(), event.getEntityLiving().level);
                chuchu2.setPos(event.getEntityLiving().position().x, event.getEntityLiving().position().y, event.getEntityLiving().position().z);
                event.getEntityLiving().level.addFreshEntity(chuchu1);
                event.getEntityLiving().level.addFreshEntity(chuchu2);
            }
        }
        if (event.getEntityLiving() instanceof IceChuchuEntity) {
            Random rand = new Random();
            int splitchance = rand.nextInt(4);
            if (splitchance == 0) {
                IceChuchuEntity chuchu1 = new IceChuchuEntity(EntityInit.ICE_CHUCHU.get(), event.getEntityLiving().level);
                chuchu1.setPos(event.getEntityLiving().position().x, event.getEntityLiving().position().y, event.getEntityLiving().position().z);
                IceChuchuEntity chuchu2 = new IceChuchuEntity(EntityInit.ICE_CHUCHU.get(), event.getEntityLiving().level);
                chuchu2.setPos(event.getEntityLiving().position().x, event.getEntityLiving().position().y, event.getEntityLiving().position().z);
                event.getEntityLiving().level.addFreshEntity(chuchu1);
                event.getEntityLiving().level.addFreshEntity(chuchu2);
            }
        }
        if (event.getEntityLiving() instanceof ElectricChuchuEntity) {
            Random rand = new Random();
            int splitchance = rand.nextInt(4);
            if (splitchance == 0) {
                ElectricChuchuEntity chuchu1 = new ElectricChuchuEntity(EntityInit.ELECTRIC_CHUCHU.get(), event.getEntityLiving().level);
                chuchu1.setPos(event.getEntityLiving().position().x, event.getEntityLiving().position().y, event.getEntityLiving().position().z);
                ElectricChuchuEntity chuchu2 = new ElectricChuchuEntity(EntityInit.ELECTRIC_CHUCHU.get(), event.getEntityLiving().level);
                chuchu2.setPos(event.getEntityLiving().position().x, event.getEntityLiving().position().y, event.getEntityLiving().position().z);
                event.getEntityLiving().level.addFreshEntity(chuchu1);
                event.getEntityLiving().level.addFreshEntity(chuchu2);
            }
        }
    }

    @SubscribeEvent
    public void chuchudeatheffects(LivingDeathEvent event) {
        if (event.getEntityLiving() instanceof IceChuchuEntity) {
            double radius = 2;
            AxisAlignedBB axisalignedbb = new AxisAlignedBB(event.getEntityLiving().blockPosition()).inflate(radius).expandTowards(0.0D, event.getEntityLiving().level.getMaxBuildHeight(), 0.0D);
            List<LivingEntity> list = event.getEntityLiving().level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
            Random rand = new Random();
            event.getEntityLiving().level.addParticle(ParticleTypes.ITEM_SLIME, event.getEntityLiving().getX() + rand.nextDouble(),
                    event.getEntityLiving().getY() + 0.5D, event.getEntityLiving().getZ() + rand.nextDouble(),
                    0d, 0.05d, 0d);
            //spawnParticle("reddust", par3EntityPlayer.posX + (rand.nextDouble() - 0.5D) *
            // (double)par3EntityPlayer.width, par3EntityPlayer.posY + rand.nextDouble() * (double)par3EntityPlayer.height -
            // (double)par3EntityPlayer.yOffset, par3EntityPlayer.posZ + (rand.nextDouble() - 0.5D) *
            // (double)par3EntityPlayer.width, 0.0D, 0.0D, 0.0D);
            for (LivingEntity livingEntity : list) {
                if (!(livingEntity instanceof IceChuchuEntity)) {
                    livingEntity.addEffect(new EffectInstance(Effects.MOVEMENT_SLOWDOWN, 30, 10));
                }
            }
        }
        //
        if (event.getEntityLiving() instanceof ElectricChuchuEntity) {
            float fullshockwavedamage = 10F;
            float damage = 5F;
            double radius = 2;
            AxisAlignedBB axisalignedbb = new AxisAlignedBB(event.getEntityLiving().blockPosition()).inflate(radius).expandTowards(0.0D, event.getEntityLiving().level.getMaxBuildHeight(), 0.0D);
            List<LivingEntity> list = event.getEntityLiving().level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);

            for (LivingEntity living : list) {
                if (!(living instanceof ElectricChuchuEntity)) {

                    BlockPos currentPos = event.getEntityLiving().blockPosition();
                    event.getEntityLiving().level.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                            SoundInit.SHOCKARROWHIT.get(), SoundCategory.PLAYERS, 1.5f, 1f);
                    living.addEffect(new EffectInstance(Effects.MOVEMENT_SLOWDOWN, 40, 10));
                    if (living.isInWaterOrRain() && !living.isInWater() || living.equals(EntityType.IRON_GOLEM)) {
                        living.hurt(DamageSource.LIGHTNING_BOLT, damage * 2);
                    }
                    ItemStack mainstack = living.getItemBySlot(EquipmentSlotType.MAINHAND);
                    ItemStack offstack = living.getItemBySlot(EquipmentSlotType.OFFHAND);
                    if (living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.IRON_SWORD ||
                            living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.GOLDEN_SWORD ||
                            living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.IRON_AXE ||
                            living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.GOLDEN_AXE ||
                            living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.IRON_PICKAXE ||
                            living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.GOLDEN_PICKAXE ||
                            living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.IRON_PICKAXE ||
                            living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.IRON_SHOVEL ||
                            living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.GOLDEN_PICKAXE ||
                            living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.IRON_HOE ||
                            living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.GOLDEN_HOE ||
                            living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.CROSSBOW ||
                            living.hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.SHIELD) {
                        living.spawnAtLocation(mainstack);
                        living.setItemSlot(EquipmentSlotType.MAINHAND, ItemStack.EMPTY);
                    }
                    if (living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.IRON_SWORD ||
                            living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.GOLDEN_SWORD ||
                            living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.IRON_AXE ||
                            living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.GOLDEN_AXE ||
                            living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.IRON_PICKAXE ||
                            living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.GOLDEN_PICKAXE ||
                            living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.IRON_PICKAXE ||
                            living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.IRON_SHOVEL ||
                            living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.GOLDEN_PICKAXE ||
                            living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.IRON_HOE ||
                            living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.GOLDEN_HOE ||
                            living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.CROSSBOW ||
                            living.hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.SHIELD) {
                        living.spawnAtLocation(offstack);
                        living.setItemSlot(EquipmentSlotType.OFFHAND, ItemStack.EMPTY);
                    }

                    if (ZConfigManager.getInstance().damageScaling() == 0) {
                        living.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage);
                    }
                    if (ZConfigManager.getInstance().damageScaling() == 1) {
                        living.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage / 2);
                    }
                    if (ZConfigManager.getInstance().damageScaling() == 2) {
                        living.hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage / 4);
                    }


                }

            }
        }
    }

    @SubscribeEvent
    public void electricchuchuattackeffect(LivingDamageEvent event) {
        Entity sourceEntity = event.getSource().getDirectEntity();
        if (sourceEntity instanceof ElectricChuchuEntity) {
            float fullshockwavedamage = 10F;
            float damage = 5F;
                if (!(event.getEntityLiving() instanceof ElectricChuchuEntity)) {

                    BlockPos currentPos = event.getEntityLiving().blockPosition();
                    event.getEntityLiving().level.playSound(null, currentPos.getX(), currentPos.getY(), currentPos.getZ(),
                            SoundInit.SHOCKARROWHIT.get(), SoundCategory.PLAYERS, 1.5f, 1f);
                    event.getEntityLiving().addEffect(new EffectInstance(Effects.MOVEMENT_SLOWDOWN, 40, 10));
                    if (event.getEntityLiving().isInWaterOrRain() && !event.getEntityLiving().isInWater() || event.getEntityLiving().equals(EntityType.IRON_GOLEM)) {
                        event.getEntityLiving().hurt(DamageSource.LIGHTNING_BOLT, damage * 2);
                    }
                    ItemStack mainstack = event.getEntityLiving().getItemBySlot(EquipmentSlotType.MAINHAND);
                    ItemStack offstack = event.getEntityLiving().getItemBySlot(EquipmentSlotType.OFFHAND);
                    if (event.getEntityLiving().hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.IRON_SWORD ||
                            event.getEntityLiving().hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.GOLDEN_SWORD ||
                            event.getEntityLiving().hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.IRON_AXE ||
                            event.getEntityLiving().hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.GOLDEN_AXE ||
                            event.getEntityLiving().hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.IRON_PICKAXE ||
                            event.getEntityLiving().hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.GOLDEN_PICKAXE ||
                            event.getEntityLiving().hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.IRON_PICKAXE ||
                            event.getEntityLiving().hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.IRON_SHOVEL ||
                            event.getEntityLiving().hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.GOLDEN_PICKAXE ||
                            event.getEntityLiving().hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.IRON_HOE ||
                            event.getEntityLiving().hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.GOLDEN_HOE ||
                            event.getEntityLiving().hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.CROSSBOW ||
                            event.getEntityLiving().hasItemInSlot(EquipmentSlotType.MAINHAND) && mainstack.getItem() == Items.SHIELD) {
                        event.getEntityLiving().spawnAtLocation(mainstack);
                        event.getEntityLiving().setItemSlot(EquipmentSlotType.MAINHAND, ItemStack.EMPTY);
                    }
                    if (event.getEntityLiving().hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.IRON_SWORD ||
                            event.getEntityLiving().hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.GOLDEN_SWORD ||
                            event.getEntityLiving().hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.IRON_AXE ||
                            event.getEntityLiving().hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.GOLDEN_AXE ||
                            event.getEntityLiving().hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.IRON_PICKAXE ||
                            event.getEntityLiving().hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.GOLDEN_PICKAXE ||
                            event.getEntityLiving().hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.IRON_PICKAXE ||
                            event.getEntityLiving().hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.IRON_SHOVEL ||
                            event.getEntityLiving().hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.GOLDEN_PICKAXE ||
                            event.getEntityLiving().hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.IRON_HOE ||
                            event.getEntityLiving().hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.GOLDEN_HOE ||
                            event.getEntityLiving().hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.CROSSBOW ||
                            event.getEntityLiving().hasItemInSlot(EquipmentSlotType.OFFHAND) && mainstack.getItem() == Items.SHIELD) {
                        event.getEntityLiving().spawnAtLocation(offstack);
                        event.getEntityLiving().setItemSlot(EquipmentSlotType.OFFHAND, ItemStack.EMPTY);
                    }

                    if (ZConfigManager.getInstance().damageScaling() == 0) {
                        event.getEntityLiving().hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage);
                    }
                    if (ZConfigManager.getInstance().damageScaling() == 1) {
                        event.getEntityLiving().hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage / 2);
                    }
                    if (ZConfigManager.getInstance().damageScaling() == 2) {
                        event.getEntityLiving().hurt(DamageSource.LIGHTNING_BOLT, fullshockwavedamage / 4);
                    }


                }
            }
    }
}
