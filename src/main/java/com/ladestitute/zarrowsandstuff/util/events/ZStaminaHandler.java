package com.ladestitute.zarrowsandstuff.util.events;

import com.ladestitute.zarrowsandstuff.blocks.natural.plants.EmptySpicyPepperBlock;
import com.ladestitute.zarrowsandstuff.blocks.natural.plants.SpicyPepperBlock;
import com.ladestitute.zarrowsandstuff.blocks.natural.plants.hydromelon.HydromelonAttachedStemBlock;
import com.ladestitute.zarrowsandstuff.blocks.natural.plants.hydromelon.HydromelonStemBlock;
import com.ladestitute.zarrowsandstuff.util.KeyboardUtil;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import com.ladestitute.zarrowsandstuff.capabilityzarrows.PlayerDataCapabilityProvider;
import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.List;
import java.util.Random;

public class ZStaminaHandler {

    public int exhausted;

    @SubscribeEvent
    public void basics(TickEvent.PlayerTickEvent event) {
        if (ZConfigManager.getInstance().enablestamina()) {
            LivingEntity player = event.player;
            World world = player.level;

            if(ZConfigManager.getInstance().staminastopshunger())
            {
                event.player.getFoodData().setSaturation(1.2F);
            }

            player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                if (h.getStamina() < 0) {
                    h.setStamina(0.0F);
                }

                if (h.getStamina() > h.getMaxStamina()) {
                    h.setStamina(h.getMaxStamina());
                }

                if (h.getStamina() > 0 && player.horizontalCollision && KeyboardUtil.isHoldingR.isDown()) {
                    BlockPos posSouth = player.blockPosition().south();
                    BlockState blockStateSouth = player.level.getBlockState(posSouth);
                    Material south = blockStateSouth.getMaterial();
                    BlockPos posNorth = player.blockPosition().north();
                    BlockState blockStateNorth = player.level.getBlockState(posNorth);
                    Material north = blockStateSouth.getMaterial();
                    BlockPos posWest = player.blockPosition().west();
                    BlockState blockStateWest = player.level.getBlockState(posWest);
                    Material west = blockStateSouth.getMaterial();
                    BlockPos posEast = player.blockPosition().east();
                    BlockState blockStateEast = player.level.getBlockState(posEast);
                    Material east = blockStateSouth.getMaterial();
                    if (south != Material.LEAVES || north != Material.LEAVES ||
                            west != Material.LEAVES || east != Material.LEAVES ||
                            south != Material.WEB || north != Material.WEB ||
                            west != Material.WEB || east != Material.WEB) {
                        if(ZConfigManager.getInstance().allowclimbing()) {
                            player.setDeltaMovement(0, 0.15, 0);
                        }
                        h.subtractStamina((float) (0.12F / 2.5));
                        if (world.isRaining()) {
                            if(ZConfigManager.getInstance().allowrainslipping()) {
                                Random rand1 = new Random();
                                int sliptimerthreshold = rand1.nextInt(4);

                                int sliptimer = 0;
                                sliptimer++;

                                if (sliptimerthreshold == 0) {
                                    if (sliptimer == 30) {
                                        h.subtractStamina(2.5F);
                                        Random rand = new Random();
                                        int sliptimerthresholdreset = rand.nextInt(4);
                                        sliptimer = 0;
                                        int sliptimethreshold = sliptimerthresholdreset;
                                    }
                                }
                                if (sliptimerthreshold == 1) {
                                    if (sliptimer == 40) {
                                        h.subtractStamina(2.5F);
                                        Random rand = new Random();
                                        int sliptimerthresholdreset = rand.nextInt(4);
                                        sliptimer = 0;
                                        int sliptimethreshold = sliptimerthresholdreset;
                                    }
                                }
                                if (sliptimerthreshold == 2) {
                                    if (sliptimer == 60) {
                                        h.subtractStamina(2.5F);
                                        Random rand = new Random();
                                        int sliptimerthresholdreset = rand.nextInt(4);
                                        sliptimer = 0;
                                        int sliptimethreshold = sliptimerthresholdreset;
                                    }
                                }
                                if (sliptimerthreshold == 3) {
                                    if (sliptimer == 90) {
                                        h.subtractStamina(2.5F);
                                        Random rand = new Random();
                                        int sliptimerthresholdreset = rand.nextInt(4);
                                        sliptimer = 0;
                                        int sliptimethreshold = sliptimerthresholdreset;
                                    }
                                }
                            }
                        }
                    }
                }
                float oldabsorb = event.player.getFoodData().getSaturationLevel();
                int oldhunger = event.player.getFoodData().getFoodLevel();
                if (player.isSprinting() && !player.isInWater()) {
                    if(ZConfigManager.getInstance().sprintingusesstamina()) {
                        oldhunger = event.player.getFoodData().getFoodLevel();
                        oldabsorb = event.player.getFoodData().getSaturationLevel();
                        event.player.getFoodData().setSaturation(1000F);
                        h.subtractStamina((float) (0.12F * 1.36));
                    }
                }
                if (player.isInWater()) {
                    if(ZConfigManager.getInstance().swimmingusestamina())
                    {
                        oldhunger = event.player.getFoodData().getFoodLevel();
                        oldabsorb = event.player.getFoodData().getSaturationLevel();
                        event.player.getFoodData().setSaturation(1000F);
                        h.subtractStamina(0.5F);
                    }
                }
                if (player.isSprinting() && player.isInWater()) {
                    if(ZConfigManager.getInstance().swimmingusestamina()) {
                        oldhunger = event.player.getFoodData().getFoodLevel();
                        oldabsorb = event.player.getFoodData().getSaturationLevel();
                        event.player.getFoodData().setSaturation(1000F);
                        h.subtractStamina(1F);
                    }
                }
                if (h.getStamina() <= 0.1F) {
                    if(ZConfigManager.getInstance().sprintingusesstamina() && !player.horizontalCollision && !event.player.isCreative() && !KeyboardUtil.isHoldingR.isDown()) {
                        event.player.addEffect(new EffectInstance(Effects.MOVEMENT_SLOWDOWN, 40, 0));
                        exhausted = 1;
                        event.player.getFoodData().setFoodLevel(oldhunger);
                        event.player.getFoodData().setSaturation(oldabsorb);
                    }
                }

                if(ZConfigManager.getInstance().allowspinattacks())
                {
                    ItemStack swordstack = event.player.getItemBySlot(EquipmentSlotType.MAINHAND);
                    if (swordstack.getItem() == Items.WOODEN_SWORD || swordstack.getItem() == Items.STONE_SWORD ||
                            swordstack.getItem() == Items.IRON_SWORD || swordstack.getItem() == Items.GOLDEN_SWORD ||
                            swordstack.getItem() == Items.DIAMOND_SWORD || swordstack.getItem() == Items.NETHERITE_SWORD) {
                        if (h.getStamina() >= 6F && player.isOnGround() && !player.horizontalCollision && KeyboardUtil.isHoldingR.isDown())
                        {
                            h.subtractStamina(6F);
                            BlockPos blockPos = player.blockPosition();
                            int harvestRadius = 1;
                            int radius = (int) (Math.floor(2.0) + harvestRadius);

                            for (int x = -radius; x <= radius; ++x)
                            {
                                for (int y = -1; y <= 1; ++y) {
                                    for (int z = -radius; z <= radius; ++z) {
                                        BlockPos cropPos = new BlockPos(blockPos.getX() + x, blockPos.getY() + y, blockPos.getZ() + z);
                                        BlockState blockState = world.getBlockState(cropPos);
                                        Block block = blockState.getBlock();
                                        if (block instanceof TallGrassBlock || block instanceof FlowerBlock || 
                                                block instanceof DoublePlantBlock || block instanceof WebBlock ||
                                                block instanceof VineBlock || block instanceof TripWireBlock ||
                                                block instanceof TwistingVinesBlock || block instanceof TwistingVinesTopBlock ||
                                                block instanceof TallFlowerBlock || block instanceof DeadBushBlock ||
                                                block instanceof HydromelonAttachedStemBlock || block instanceof HydromelonStemBlock ||
                                                block instanceof SpicyPepperBlock || block instanceof EmptySpicyPepperBlock){
                                            Block.dropResources(blockState, world, blockPos);
                                            event.player.level.addParticle(ParticleTypes.SWEEP_ATTACK, cropPos.getX(), cropPos.getY(), cropPos.getZ(), 0.0D, 0.0D,
                                                    0.0D);
                                            world.playSound(null, player.getX(), player.getY(), player.getZ(),
                                                    SoundEvents.PLAYER_ATTACK_SWEEP, SoundCategory.PLAYERS, 2f, 1f);
                                            world.destroyBlock(cropPos, true);
                                        }
                                    }

                                }
                            }

                            int range = 4;
                            AxisAlignedBB axisalignedbb = new AxisAlignedBB(event.player.position().x - range, event.player.position().y - range, event.player.position().z - range,
                                    event.player.position().x + range, event.player.position().y + range, event.player.position().z + range);
                            List<LivingEntity> list = event.player.level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);

                            for (LivingEntity livingEntity : list) {
                                if(livingEntity instanceof PlayerEntity)
                                {
                                    return;
                                }
                                if(swordstack.getItem() == Items.WOODEN_SWORD)
                                {
                                    livingEntity.hurt(DamageSource.GENERIC, 2.5F+1F);
                                }
                                if(swordstack.getItem() == Items.STONE_SWORD)
                                {
                                    livingEntity.hurt(DamageSource.GENERIC, 5F);
                                }
                                if(swordstack.getItem() == Items.IRON_SWORD || swordstack.getItem() == Items.GOLDEN_SWORD)
                                {
                                    livingEntity.hurt(DamageSource.GENERIC, 2.5F+4F);
                                }
                                if(swordstack.getItem() == Items.DIAMOND_SWORD || swordstack.getItem() == Items.NETHERITE_SWORD)
                                {
                                    livingEntity.hurt(DamageSource.GENERIC, 2.5F+6F);
                                }
                                livingEntity.level.addParticle(ParticleTypes.SWEEP_ATTACK, livingEntity.getX(), livingEntity.getY(), livingEntity.getZ(), 0.0D, 0.0D,
                                        0.0D);
                                livingEntity.level.addParticle(ParticleTypes.SWEEP_ATTACK, livingEntity.getX(), livingEntity.getY(), livingEntity.getZ(), 0.0D, 0.0D,
                                        0.0D);
                            }
                        }

                    }
                }

                if (player.isOnGround() && h.getStamina() < h.getMaxStamina()) {
                    if (h.getStamina() >= 0.0F) {
                        h.addStamina(0.12F);
                            if(exhausted == 1 && h.getStamina() >= 0.5F) {
                                event.player.removeEffect(Effects.MOVEMENT_SLOWDOWN);
                                exhausted = 0;
                            }

                    }
                }
            });
        }
    }
}