package com.ladestitute.zarrowsandstuff.util.events;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import com.ladestitute.zarrowsandstuff.capabilityzarrows.PlayerDataCapabilityProvider;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.IngameGui;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.gui.ForgeIngameGui;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.text.DecimalFormat;
import java.util.concurrent.atomic.AtomicInteger;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.FORGE, modid = ZArrowsMain.MOD_ID, value = Dist.CLIENT)
public class ZStatsGUIHandler {

    private static final ResourceLocation MAGIC_ICONS = new ResourceLocation(ZArrowsMain.MOD_ID, "textures/gui/stamina.png");
    // Used for rendering stamina when player is dead
    private static int lastTickStamina;

    @SubscribeEvent
    public static void onRenderGameOverlay(RenderGameOverlayEvent.Post event) {
        // Render magic right after food
        if (event.getType() == RenderGameOverlayEvent.ElementType.FOOD) {
            Minecraft minecraft = Minecraft.getInstance();
            FontRenderer fontRenderer = Minecraft.getInstance().font;
            MatrixStack matrixStack = new MatrixStack();
            DecimalFormat df = new DecimalFormat("###.##");
            BlockPos pos = new BlockPos(Minecraft.getInstance().player.blockPosition());
            //Temp display
            if (ZConfigManager.getInstance().alwaysshowTemp()) {
                fontRenderer.drawShadow(matrixStack, "Temp: " + df.format(Minecraft.getInstance().player.level.getBiome(pos).getTemperature(pos)) + "F", 350, 10, 0xFFFFFF);
            }

            minecraft.getTextureManager().bind(MAGIC_ICONS);
            IngameGui gui = minecraft.gui;
            PlayerEntity player = minecraft.player;

            player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
            if (!ZConfigManager.getInstance().alwaysshowStamina() && h.getStamina() < h.getMaxStamina()) {
                if (player == null) {
                    return;
                }

                RenderSystem.enableBlend();
                int width = event.getWindow().getGuiScaledWidth();
                int height = event.getWindow().getGuiScaledHeight();
                ForgeIngameGui.right_height += 0;
                if(ZConfigManager.getInstance().staminameterposition() == 1)
                {
                    int left = width / 2 - 93;
                    int top = height - ForgeIngameGui.right_height + 38;
                    ForgeIngameGui.right_height += 0;

                    AtomicInteger level = new AtomicInteger();

                    // This checks are for crash fix when rendering mana of dead player
                    if (player.isAlive()) {
                        //This is the proper way to check capabilities

                        level.set((int) h.getStamina());
                        lastTickStamina = level.get();
                    } else {
                        level.set(lastTickStamina);
                    }

                    for (int i = 0; i < 10; ++i) {
                        {
                            int idx = i * 2 + 1;
                            int x = left - i * 8 - 9;
                            int y = top;
                            int icon = 9;

                            gui.blit(event.getMatrixStack(), x, y, 0, 0, 9, 9);

                            if (idx < level.get())
                                gui.blit(event.getMatrixStack(), x, y, icon, 0, 9, 9);
                            else if (idx == level.get())
                                gui.blit(event.getMatrixStack(), x, y, icon + 9, 0, 9, 9);
                        }
                    }


                }
                if(ZConfigManager.getInstance().staminameterposition() == 2)
                {
                    int left = width / 2 - -41;
                    int top = height - ForgeIngameGui.right_height + -181;
                    ForgeIngameGui.right_height += 0;

                    AtomicInteger level = new AtomicInteger();

                    // This checks are for crash fix when rendering mana of dead player
                    if (player.isAlive()) {
                        //This is the proper way to check capabilities

                        level.set((int) h.getStamina());
                        lastTickStamina = level.get();
                    } else {
                        level.set(lastTickStamina);
                    }

                    for (int i = 0; i < 10; ++i) {
                        {
                            int idx = i * 2 + 1;
                            int x = left - i * 8 - 9;
                            int y = top;
                            int icon = 9;

                            gui.blit(event.getMatrixStack(), x, y, 0, 0, 9, 9);

                            if (idx < level.get())
                                gui.blit(event.getMatrixStack(), x, y, icon, 0, 9, 9);
                            else if (idx == level.get())
                                gui.blit(event.getMatrixStack(), x, y, icon + 9, 0, 9, 9);
                        }
                    }
                }
                if(ZConfigManager.getInstance().staminameterposition() == 3)
                {
                    int left = width / 2 - -173;
                    int top = height - ForgeIngameGui.right_height + 38;
                    ForgeIngameGui.right_height += 0;

                    AtomicInteger level = new AtomicInteger();

                    // This checks are for crash fix when rendering mana of dead player
                    if (player.isAlive()) {
                        //This is the proper way to check capabilities

                        level.set((int) h.getStamina());
                        lastTickStamina = level.get();
                    } else {
                        level.set(lastTickStamina);
                    }

                    for (int i = 0; i < 10; ++i) {
                        {
                            int idx = i * 2 + 1;
                            int x = left - i * 8 - 9;
                            int y = top;
                            int icon = 9;

                            gui.blit(event.getMatrixStack(), x, y, 0, 0, 9, 9);

                            if (idx < level.get())
                                gui.blit(event.getMatrixStack(), x, y, icon, 0, 9, 9);
                            else if (idx == level.get())
                                gui.blit(event.getMatrixStack(), x, y, icon + 9, 0, 9, 9);
                        }
                    }
                }

                // We need to switch texture back to vanilla one
                minecraft.getTextureManager().bind(AbstractGui.GUI_ICONS_LOCATION);
                RenderSystem.disableBlend();

            }
            });

            player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                if (ZConfigManager.getInstance().alwaysshowStamina()) {
                    if (player == null) {
                        return;
                    }

                    RenderSystem.enableBlend();
                    int width = event.getWindow().getGuiScaledWidth();
                    int height = event.getWindow().getGuiScaledHeight();
                    if(ZConfigManager.getInstance().staminameterposition() == 1)
                    {
                        int left = width / 2 - 93;
                        int top = height - ForgeIngameGui.right_height + 38;
                        ForgeIngameGui.right_height += 0;

                    AtomicInteger level = new AtomicInteger();

                    // This checks are for crash fix when rendering mana of dead player
                    if (player.isAlive()) {
                        //This is the proper way to check capabilities

                        level.set((int) h.getStamina());
                        lastTickStamina = level.get();
                    } else {
                        level.set(lastTickStamina);
                    }

                    for (int i = 0; i < 10; ++i) {
                        {
                            int idx = i * 2 + 1;
                            int x = left - i * 8 - 9;
                            int y = top;
                            int icon = 9;

                            gui.blit(event.getMatrixStack(), x, y, 0, 0, 9, 9);

                            if (idx < level.get())
                                gui.blit(event.getMatrixStack(), x, y, icon, 0, 9, 9);
                            else if (idx == level.get())
                                gui.blit(event.getMatrixStack(), x, y, icon + 9, 0, 9, 9);
                            }
                        }


                    }
                    if(ZConfigManager.getInstance().staminameterposition() == 2)
                    {
                        int left = width / 2 - -41;
                        int top = height - ForgeIngameGui.right_height + -181;
                        ForgeIngameGui.right_height += 0;

                        AtomicInteger level = new AtomicInteger();

                        // This checks are for crash fix when rendering mana of dead player
                        if (player.isAlive()) {
                            //This is the proper way to check capabilities

                            level.set((int) h.getStamina());
                            lastTickStamina = level.get();
                        } else {
                            level.set(lastTickStamina);
                        }

                        for (int i = 0; i < 10; ++i) {
                            {
                                int idx = i * 2 + 1;
                                int x = left - i * 8 - 9;
                                int y = top;
                                int icon = 9;

                                gui.blit(event.getMatrixStack(), x, y, 0, 0, 9, 9);

                                if (idx < level.get())
                                    gui.blit(event.getMatrixStack(), x, y, icon, 0, 9, 9);
                                else if (idx == level.get())
                                    gui.blit(event.getMatrixStack(), x, y, icon + 9, 0, 9, 9);
                            }
                        }
                    }
                    if(ZConfigManager.getInstance().staminameterposition() == 3)
                    {
                        int left = width / 2 - -173;
                        int top = height - ForgeIngameGui.right_height + 38;
                        ForgeIngameGui.right_height += 0;

                        AtomicInteger level = new AtomicInteger();

                        // This checks are for crash fix when rendering mana of dead player
                        if (player.isAlive()) {
                            //This is the proper way to check capabilities

                            level.set((int) h.getStamina());
                            lastTickStamina = level.get();
                        } else {
                            level.set(lastTickStamina);
                        }

                        for (int i = 0; i < 10; ++i) {
                            {
                                int idx = i * 2 + 1;
                                int x = left - i * 8 - 9;
                                int y = top;
                                int icon = 9;

                                gui.blit(event.getMatrixStack(), x, y, 0, 0, 9, 9);

                                if (idx < level.get())
                                    gui.blit(event.getMatrixStack(), x, y, icon, 0, 9, 9);
                                else if (idx == level.get())
                                    gui.blit(event.getMatrixStack(), x, y, icon + 9, 0, 9, 9);
                            }
                        }
                    }

                    // We need to switch texture back to vanilla one
                    minecraft.getTextureManager().bind(AbstractGui.GUI_ICONS_LOCATION);
                    RenderSystem.disableBlend();

                }
            });

        }
    }

    }
