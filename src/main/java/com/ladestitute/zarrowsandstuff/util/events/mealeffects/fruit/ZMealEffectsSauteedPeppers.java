package com.ladestitute.zarrowsandstuff.util.events.mealeffects.fruit;

import com.ladestitute.zarrowsandstuff.registries.EffectInit;
import com.ladestitute.zarrowsandstuff.registries.FoodInit;
import com.ladestitute.zarrowsandstuff.capabilityzarrows.PlayerDataCapabilityProvider;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class ZMealEffectsSauteedPeppers {

    @SubscribeEvent
    public void applyeffectsandcooldown(LivingEntityUseItemEvent.Finish event) {
        if (event.getEntityLiving() instanceof PlayerEntity) {
            PlayerEntity player = (PlayerEntity) event.getEntityLiving();
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.SAUTEED_PEPPERS.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                        player.removeEffectNoUpdate(oppositeresist);
                        player.addEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 3000, 0));
                        h.setColdResistCooldown(3000);
                        int health = (int) (player.getHealth() + 2);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if (ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if (ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.SAUTEED_PEPPERS2.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                        player.removeEffectNoUpdate(oppositeresist);
                        player.addEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 6000, 0));
                        h.setColdResistCooldown(6000);
                        int health = (int) (player.getHealth() + 4);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if (ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if (ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.SAUTEED_PEPPERS3.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                        player.removeEffectNoUpdate(oppositeresist);
                        player.addEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 9000, 0));
                        h.setColdResistCooldown(9000);
                        int health = (int) (player.getHealth() + 6);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if (ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if (ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.SAUTEED_PEPPERS4.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                        player.removeEffectNoUpdate(oppositeresist);
                        player.addEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 12000, 0));
                        h.setColdResistCooldown(12000);
                        int health = (int) (player.getHealth() + 8);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if (ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if (ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.SAUTEED_PEPPERS5.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                        player.removeEffectNoUpdate(oppositeresist);
                        player.addEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 15000, 0));
                        h.setColdResistCooldown(15000);
                        int health = (int) (player.getHealth() + 10);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if (ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if (ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
        }
    }
}

