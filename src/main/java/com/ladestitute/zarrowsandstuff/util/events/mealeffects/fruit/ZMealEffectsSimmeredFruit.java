package com.ladestitute.zarrowsandstuff.util.events.mealeffects.fruit;

import com.ladestitute.zarrowsandstuff.registries.EffectInit;
import com.ladestitute.zarrowsandstuff.registries.FoodInit;
import com.ladestitute.zarrowsandstuff.capabilityzarrows.PlayerDataCapabilityProvider;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

    public class ZMealEffectsSimmeredFruit {

        @SubscribeEvent
        public void applyeffectsandcooldown(LivingEntityUseItemEvent.Finish event) {
            if (event.getEntityLiving() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntityLiving();
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT1.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            //Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                            //player.removeEffectNoUpdate(oppositeresist);
                            //player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 3600, 0));
                            //h.setColdResistCooldown(3600);
                            int health = (int) (player.getHealth() + 2);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT2.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                            player.removeEffectNoUpdate(oppositeresist);
                            player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 3000, 0));
                            h.setHeatResistCooldown(3000);
                            int health = (int) (player.getHealth() + 2);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT3.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            int health = (int) (player.getHealth() + 4);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT4.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                            player.removeEffectNoUpdate(oppositeresist);
                            player.addEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 3000, 0));
                            h.setColdResistCooldown(3000);
                            int health = (int) (player.getHealth() + 4);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT5.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                            player.removeEffectNoUpdate(oppositeresist);
                            player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 3600, 0));
                            h.setHeatResistCooldown(3600);
                            int health = (int) (player.getHealth() + 4);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT6.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                            player.removeEffectNoUpdate(oppositeresist);
                            player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 6000, 0));
                            h.setHeatResistCooldown(6000);
                            int health = (int) (player.getHealth() + 4);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT7.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            int health = (int) (player.getHealth() + 6);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT8.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                            player.removeEffectNoUpdate(oppositeresist);
                            player.addEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 4200, 0));
                            h.setColdResistCooldown(4200);
                            int health = (int) (player.getHealth() + 6);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT9.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                            player.removeEffectNoUpdate(oppositeresist);
                            player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 4200, 0));
                            h.setHeatResistCooldown(4200);
                            int health = (int) (player.getHealth() + 6);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT10.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            int health = (int) (player.getHealth() + 6);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT11.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                            player.removeEffectNoUpdate(oppositeresist);
                            player.addEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 6600, 0));
                            h.setColdResistCooldown(6600);
                            int health = (int) (player.getHealth() + 6);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT12.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                            player.removeEffectNoUpdate(oppositeresist);
                            player.addEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 9000, 0));
                            h.setColdResistCooldown(9000);
                            int health = (int) (player.getHealth() + 6);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT13.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            int health = (int) (player.getHealth() + 8);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT14.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                            player.removeEffectNoUpdate(oppositeresist);
                            player.addEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 4800, 0));
                            h.setColdResistCooldown(4800);
                            int health = (int) (player.getHealth() + 8);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT15.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                            player.removeEffectNoUpdate(oppositeresist);
                            player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 4800, 0));
                            h.setHeatResistCooldown(4800);
                            int health = (int) (player.getHealth() + 8);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT16.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                            player.removeEffectNoUpdate(oppositeresist);
                            player.addEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 7200, 0));
                            h.setColdResistCooldown(7200);
                            int health = (int) (player.getHealth() + 8);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT17.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                            player.removeEffectNoUpdate(oppositeresist);
                            player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 7200, 0));
                            h.setHeatResistCooldown(7200);
                            int health = (int) (player.getHealth() + 8);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT18.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            int health = (int) (player.getHealth() + 8);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT19.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                            player.removeEffectNoUpdate(oppositeresist);
                            player.addEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 9600, 0));
                            h.setColdResistCooldown(9600);
                            int health = (int) (player.getHealth() + 8);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT20.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            int health = (int) (player.getHealth() + 10);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT21.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                            player.removeEffectNoUpdate(oppositeresist);
                            player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 9600, 0));
                            h.setHeatResistCooldown(9600);
                            int health = (int) (player.getHealth() + 8);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT22.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                            player.removeEffectNoUpdate(oppositeresist);
                            player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 12000, 0));
                            h.setHeatResistCooldown(12000);
                            int health = (int) (player.getHealth() + 8);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT23.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            int health = (int) (player.getHealth() + 10);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT24.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                            player.removeEffectNoUpdate(oppositeresist);
                            player.addEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 5400, 0));
                            h.setColdResistCooldown(5400);
                            int health = (int) (player.getHealth() + 10);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT25.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                            player.removeEffectNoUpdate(oppositeresist);
                            player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 5400, 0));
                            h.setHeatResistCooldown(5400);
                            int health = (int) (player.getHealth() + 10);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT26.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                            player.removeEffectNoUpdate(oppositeresist);
                            player.addEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 7800, 0));
                            h.setColdResistCooldown(7800);
                            int health = (int) (player.getHealth() + 10);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT27.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            int health = (int) (player.getHealth() + 10);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT28.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                            player.removeEffectNoUpdate(oppositeresist);
                            player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 7800, 0));
                            h.setHeatResistCooldown(7800);
                            int health = (int) (player.getHealth() + 10);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT29.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                            player.removeEffectNoUpdate(oppositeresist);
                            player.addEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 10200, 0));
                            h.setColdResistCooldown(10200);
                            int health = (int) (player.getHealth() + 10);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT30.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                            player.removeEffectNoUpdate(oppositeresist);
                            player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 10200, 0));
                            h.setHeatResistCooldown(10200);
                            int health = (int) (player.getHealth() + 10);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT31.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                            player.removeEffectNoUpdate(oppositeresist);
                            player.addEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 12600, 0));
                            h.setColdResistCooldown(12600);
                            int health = (int) (player.getHealth() + 10);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT32.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                            player.removeEffectNoUpdate(oppositeresist);
                            player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 12600, 0));
                            h.setHeatResistCooldown(12600);
                            int health = (int) (player.getHealth() + 10);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
                if (event.getItem().getStack().getItem() == FoodInit.SIMMERED_FRUIT33.get()) {
                    player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                        if (h.getFoodCooldown() == 0) {
                            Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                            player.removeEffectNoUpdate(oppositeresist);
                            player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 15000, 0));
                            h.setHeatResistCooldown(15000);
                            int health = (int) (player.getHealth() + 10);
                            if (player.level.isClientSide) {
                                return;
                            }
                            if (health > 20) {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(20);
                                }
                            } else {
                                if(ZConfigManager.getInstance().allowfoodhealing()) {
                                    player.setHealth(health);
                                }
                            }
                            h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                        }
                    });
                }
                // // //
            }
        }
    }
