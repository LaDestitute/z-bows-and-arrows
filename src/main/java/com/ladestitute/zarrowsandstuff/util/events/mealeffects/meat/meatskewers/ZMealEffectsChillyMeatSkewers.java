package com.ladestitute.zarrowsandstuff.util.events.mealeffects.meat.meatskewers;

import com.ladestitute.zarrowsandstuff.registries.EffectInit;
import com.ladestitute.zarrowsandstuff.registries.FoodInit;
import com.ladestitute.zarrowsandstuff.capabilityzarrows.PlayerDataCapabilityProvider;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class ZMealEffectsChillyMeatSkewers {

    @SubscribeEvent
    public void applyeffectsandcooldown(LivingEntityUseItemEvent.Finish event) {
        if (event.getEntityLiving() instanceof PlayerEntity) {
            PlayerEntity player = (PlayerEntity) event.getEntityLiving();
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.CHILLY_MEAT_SKEWER1.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                        player.removeEffectNoUpdate(oppositeresist);
                        player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 3600, 0));
                        h.setHeatResistCooldown(3600);
                        int health = (int) (player.getHealth() + 6);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.CHILLY_MEAT_SKEWER2.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                        player.removeEffectNoUpdate(oppositeresist);
                        player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 6600, 0));
                        h.setHeatResistCooldown(6600);
                        int health = (int) (player.getHealth() + 8);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.CHILLY_MEAT_SKEWER3.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                        player.removeEffectNoUpdate(oppositeresist);
                        player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 4200, 0));
                        h.setHeatResistCooldown(4200);
                        int health = (int) (player.getHealth() + 10);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.CHILLY_MEAT_SKEWER4.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                        player.removeEffectNoUpdate(oppositeresist);
                        player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 9600, 0));
                        h.setColdResistCooldown(9600);
                        int health = (int) (player.getHealth() + 10);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.CHILLY_MEAT_SKEWER5.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                        player.removeEffectNoUpdate(oppositeresist);
                        player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 7200, 0));
                        h.setColdResistCooldown(7200);
                        int health = (int) (player.getHealth() + 12);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.CHILLY_MEAT_SKEWER6.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                        player.removeEffectNoUpdate(oppositeresist);
                        player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 4800, 0));
                        h.setColdResistCooldown(4800);
                        int health = (int) (player.getHealth() + 14);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.CHILLY_MEAT_SKEWER7.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                        player.removeEffectNoUpdate(oppositeresist);
                        player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 12600, 0));
                        h.setColdResistCooldown(12600);
                        int health = (int) (player.getHealth() + 12);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.CHILLY_MEAT_SKEWER8.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                        player.removeEffectNoUpdate(oppositeresist);
                        player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 10200, 0));
                        h.setColdResistCooldown(10200);
                        int health = (int) (player.getHealth() + 14);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.CHILLY_MEAT_SKEWER9.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                        player.removeEffectNoUpdate(oppositeresist);
                        player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 7800, 0));
                        h.setColdResistCooldown(7800);
                        int health = (int) (player.getHealth() + 18);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.CHILLY_MEAT_SKEWER10.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                        player.removeEffectNoUpdate(oppositeresist);
                        player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 5400, 0));
                        h.setColdResistCooldown(5400);
                        int health = (int) (player.getHealth() + 18);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
        }
    }
    ////////
}
