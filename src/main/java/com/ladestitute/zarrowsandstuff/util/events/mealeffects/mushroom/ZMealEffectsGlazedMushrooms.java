package com.ladestitute.zarrowsandstuff.util.events.mealeffects.mushroom;

import com.ladestitute.zarrowsandstuff.registries.FoodInit;
import com.ladestitute.zarrowsandstuff.capabilityzarrows.*;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class ZMealEffectsGlazedMushrooms {

    @SubscribeEvent
    public void applyeffectsandcooldown(LivingEntityUseItemEvent.Finish event) {
        if (event.getEntityLiving() instanceof PlayerEntity) {
            PlayerEntity player = (PlayerEntity) event.getEntityLiving();
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS1.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    h.addStamina(5.32f);
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 10);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                       // IPlayerDataCapability stats = PlayerDataCapabilityHandler(player);
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS2.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(2.66F);
                        int health = (int) (player.getHealth() + 10);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS3.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(6.65F);
                        int health = (int) (player.getHealth() + 12);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS4.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(5.32F);
                        int health = (int) (player.getHealth() + 12);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS5.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(7.98F);
                        int health = (int) (player.getHealth() + 18);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS6.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(2.66F);
                        int health = (int) (player.getHealth() + 12);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS7.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(6.65F);
                        int health = (int) (player.getHealth() + 18);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS8.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(7.98F);
                        int health = (int) (player.getHealth() + 14);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS9.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(6.65F);
                        int health = (int) (player.getHealth() + 14);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS10.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(10.64F);
                        int health = (int) (player.getHealth() + 20);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS11.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(5.32F);
                        int health = (int) (player.getHealth() + 14);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS12.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(9.31F);
                        int health = (int) (player.getHealth() + 20);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS13.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(11.97F);
                        //Value of this currently halved, extra heart containers planned
                        int health = (int) (player.getHealth() + 12);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS14.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(2.66F);
                        int health = (int) (player.getHealth() + 14);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS15.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(6.65F);
                        int health = (int) (player.getHealth() + 20);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS16.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(10.64F);
                        //Value of this currently halved, extra heart containers planned
                        int health = (int) (player.getHealth() + 12);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS17.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(10.64F);
                        int health = (int) (player.getHealth() + 18);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS18.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(9.31F);
                        int health = (int) (player.getHealth() + 16);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS19.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(11.97F);
                        int health = (int) (player.getHealth() + 10);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS20.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(9.31F);
                        //Value of this currently halved, extra heart containers planned
                        int health = (int) (player.getHealth() + 10);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS21.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(10.64F);
                        //Value of this currently halved, extra heart containers planned
                        int health = (int) (player.getHealth() + 10);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS22.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(14.63F);
                        //Value of this currently halved, extra heart containers planned
                        int health = (int) (player.getHealth() + 8);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS23.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(5.32f);
                        int health = (int) (player.getHealth() + 16);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS24.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(11.97f);
                        //Value of this currently halved, extra heart containers planned
                        int health = (int) (player.getHealth() + 10);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS25.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(11.97F);
                        //Value of this currently halved, extra heart containers planned
                        int health = (int) (player.getHealth() + 8);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS26.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(14.63f);
                        //Value of this currently halved, extra heart containers planned
                        int health = (int) (player.getHealth() + 8);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS27.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(2.66F);
                        int health = (int) (player.getHealth() + 16);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS28.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(6.65f);
                        //Value of this currently halved, extra heart containers planned
                        int health = (int) (player.getHealth() + 10);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS29.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(10.64f);
                        //Value of this currently halved, extra heart containers planned
                        int health = (int) (player.getHealth() + 8);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
            if (event.getItem().getStack().getItem() == FoodInit.GLAZED_MUSHROOMS30.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(14.64f);
                        //Value of this currently halved, extra heart containers planned
                        int health = (int) (player.getHealth() + 8);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            ////////
        }
    }
}


