package com.ladestitute.zarrowsandstuff.util.events.mealeffects.sweets;

import com.ladestitute.zarrowsandstuff.registries.FoodInit;
import com.ladestitute.zarrowsandstuff.capabilityzarrows.PlayerDataCapabilityProvider;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class ZMealEffectsHoneyedApples {

    float one5wheel = 1.33F;
    float two5wheel = 2.33f;
    float three5wheel = 3.99f;
    float four5wheel = 5.32f;
    float fullwheel = 6.65f;

    @SubscribeEvent
    public void applyeffectsandcooldown(LivingEntityUseItemEvent.Finish event) {
        // // //
        if(event.getItem().getStack().getItem() == FoodInit.HONEYED_APPLE1.get()) {
            if (event.getEntityLiving() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntityLiving();
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(2.66f);
                        int health = (int) (player.getHealth() + 12);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
        }
        // // //
        if(event.getItem().getStack().getItem() == FoodInit.HONEYED_APPLE2.get()) {
            if (event.getEntityLiving() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntityLiving();
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(fullwheel);
                        int health = (int) (player.getHealth() + 18);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
        }
        ////////
        if(event.getItem().getStack().getItem() == FoodInit.HONEYED_APPLE3.get()) {
            if (event.getEntityLiving() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntityLiving();
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 12);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
        }
        ////////
        if(event.getItem().getStack().getItem() == FoodInit.HONEYED_APPLE4.get()) {
            if (event.getEntityLiving() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntityLiving();
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(two5wheel);
                        int health = (int) (player.getHealth() + 14);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
        }
        ////////
        if(event.getItem().getStack().getItem() == FoodInit.HONEYED_APPLE5.get()) {
            if (event.getEntityLiving() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntityLiving();
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(fullwheel);
                        int health = (int) (player.getHealth() + 10);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
        }
        ////////
        if(event.getItem().getStack().getItem() == FoodInit.HONEYED_APPLE6.get()) {
            if (event.getEntityLiving() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntityLiving();
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 10);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
        }
        ////////
        if(event.getItem().getStack().getItem() == FoodInit.HONEYED_APPLE7.get()) {
            if (event.getEntityLiving() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntityLiving();
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(fullwheel+three5wheel);
                        int health = (int) (player.getHealth() + 14);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
        }
        ////////
        if(event.getItem().getStack().getItem() == FoodInit.HONEYED_APPLE8.get()) {
            if (event.getEntityLiving() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntityLiving();
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 10);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
        }
        ////////
        if(event.getItem().getStack().getItem() == FoodInit.HONEYED_APPLE9.get()) {
            if (event.getEntityLiving() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntityLiving();
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 14);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
        }
        ////////
        if(event.getItem().getStack().getItem() == FoodInit.HONEYED_APPLE10.get()) {
            if (event.getEntityLiving() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntityLiving();
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(two5wheel);
                        int health = (int) (player.getHealth() + 16);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
        }
        ////////
        if(event.getItem().getStack().getItem() == FoodInit.HONEYED_APPLE11.get()) {
            if (event.getEntityLiving() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntityLiving();
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(fullwheel);
                        int health = (int) (player.getHealth() + 10);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
        }
        // // //
        if(event.getItem().getStack().getItem() == FoodInit.HONEYED_APPLE12.get()) {
            if (event.getEntityLiving() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntityLiving();
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 16);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
        }
        ////////
        if(event.getItem().getStack().getItem() == FoodInit.HONEYED_APPLE13.get()) {
            if (event.getEntityLiving() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntityLiving();
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(fullwheel+three5wheel);
                        int health = (int) (player.getHealth() + 14);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
        }
        ////////
        if(event.getItem().getStack().getItem() == FoodInit.HONEYED_APPLE14.get()) {
            if (event.getEntityLiving() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntityLiving();
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 10);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
        }
        ////////
        if(event.getItem().getStack().getItem() == FoodInit.HONEYED_APPLE15.get()) {
            if (event.getEntityLiving() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntityLiving();
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 16);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
        }
        ////////
        if(event.getItem().getStack().getItem() == FoodInit.HONEYED_APPLE16.get()) {
            if (event.getEntityLiving() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntityLiving();
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(fullwheel*2+one5wheel);
                        int health = (int) (player.getHealth() + 16);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
        }
        ////////
        if(event.getItem().getStack().getItem() == FoodInit.HONEYED_APPLE17.get()) {
            if (event.getEntityLiving() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntityLiving();
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 14);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
        }
        ////////
        if(event.getItem().getStack().getItem() == FoodInit.HONEYED_APPLE18.get()) {
            if (event.getEntityLiving() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntityLiving();
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 10);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
        }
        ////////
        if(event.getItem().getStack().getItem() == FoodInit.HONEYED_APPLE19.get()) {
            if (event.getEntityLiving() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntityLiving();
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 8);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
        }
        ////////
        if(event.getItem().getStack().getItem() == FoodInit.HONEYED_APPLE20.get()) {
            if (event.getEntityLiving() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntityLiving();
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        int health = (int) (player.getHealth() + 10);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(20);
                            }
                        } else {
                            if(ZConfigManager.getInstance().allowfoodhealing()) {
                                player.setHealth(health);
                            }
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
        }
    }
}

