package com.ladestitute.zarrowsandstuff.util.events.mealeffects.sweets.pastry.crepes;

import com.ladestitute.zarrowsandstuff.registries.EffectInit;
import com.ladestitute.zarrowsandstuff.registries.FoodInit;
import com.ladestitute.zarrowsandstuff.capabilityzarrows.PlayerDataCapabilityProvider;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class ZMealEffectsModifierCrepes {

    @SubscribeEvent
    public void applyeffectsandcooldown(LivingEntityUseItemEvent.Finish event) {
        if (event.getEntityLiving() instanceof PlayerEntity) {
            PlayerEntity player = (PlayerEntity) event.getEntityLiving();
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.HONEY_CREPE.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(6.65f);
                        int health = (int) (player.getHealth() + 18);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            player.setHealth(20);
                        } else {
                            player.setHealth(health);
                        }
                        ;
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.ENERGIZING_PLAIN_CREPE.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        h.addStamina(2.33f);
                        int health = (int) (player.getHealth() + 12);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            player.setHealth(20);
                        } else {
                            player.setHealth(health);
                        }
                        ;
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.SPICY_PLAIN_CREPE.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.HEAT_RESIST_EFFECT.get();
                        player.removeEffectNoUpdate(oppositeresist);
                        player.addEffect(new EffectInstance(EffectInit.COLD_RESIST_EFFECT.get(), 9200, 0));
                        h.setHeatResistCooldown(9200);
                        int health = (int) (player.getHealth() + 12);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            player.setHealth(20);
                        } else {
                            player.setHealth(health);
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
            // // //
            if (event.getItem().getStack().getItem() == FoodInit.CHILLY_PLAIN_CREPE.get()) {
                player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
                    if (h.getFoodCooldown() == 0) {
                        Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                        player.removeEffectNoUpdate(oppositeresist);
                        player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), 9200, 0));
                        h.setHeatResistCooldown(9200);
                        int health = (int) (player.getHealth() + 12);
                        if (player.level.isClientSide) {
                            return;
                        }
                        if (health > 20) {
                            player.setHealth(20);
                        } else {
                            player.setHealth(health);
                        }
                        h.setFoodCooldown(ZConfigManager.getInstance().foodCooldown());
                    }
                });
            }
        }
    }
}
