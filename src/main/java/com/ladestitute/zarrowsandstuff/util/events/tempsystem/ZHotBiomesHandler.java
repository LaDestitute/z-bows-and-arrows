package com.ladestitute.zarrowsandstuff.util.events.tempsystem;

import com.ladestitute.zarrowsandstuff.registries.EffectInit;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import com.ladestitute.zarrowsandstuff.registries.SpecialBlocksInit;
import com.ladestitute.zarrowsandstuff.capabilityzarrows.PlayerDataCapabilityProvider;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.material.Material;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraft.util.DamageSource;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.registries.ForgeRegistries;

public class ZHotBiomesHandler {
    //Todo: implement onplayerclone capability-handler for the nether in v0.3
    //Temperature system values
    public int overheattimer;

    //Possible timer extending modifiers
    private final int circletmodifier = 6000;

    @SubscribeEvent
    public void cooldownhandling(TickEvent.PlayerTickEvent event) {
        LivingEntity player = event.player;

        World world = player.level;

        player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h -> {
            if (h.getHeatResistCooldown() < 0) {
                h.setHeatResistCooldown(0);
            }

            if (h.getHeatResistCooldown() > 0) {
                h.subtractHeatResistCooldown(1);
            }
        });
    }

    @SubscribeEvent
    public void campfireradius(TickEvent.PlayerTickEvent event) {
        if (ZConfigManager.getInstance().enableTemperatureSystem()) {
            LivingEntity player = event.player;
            World world = player.level;
            BlockPos pos = new BlockPos(player.blockPosition());

            Effect heatresisteffect = EffectInit.HEAT_RESIST_EFFECT.get();
            EffectInstance heatresist = player.getEffect(heatresisteffect);

            //Cardinal directions one block from player
            BlockPos posWest = player.blockPosition().west();
            BlockState blockStateWest = player.level.getBlockState(posWest);
            Block w = blockStateWest.getBlock(); //West
            BlockPos posEast = player.blockPosition().east();
            BlockState blockStateEast = player.level.getBlockState(posEast);
            Block e = blockStateEast.getBlock(); //East
            BlockPos posNorth = player.blockPosition().north();
            BlockState blockStateNorth = player.level.getBlockState(posNorth);
            Block n = blockStateNorth.getBlock(); //North
            BlockPos posSouth = player.blockPosition().south();
            BlockState blockStateSouth = player.level.getBlockState(posSouth);
            Block s = blockStateSouth.getBlock(); //South
            //Diagonal directions one block from player
            BlockPos posSW = player.blockPosition().south().west();
            BlockState blockStateSW = player.level.getBlockState(posSW);
            Block sw = blockStateSW.getBlock(); //SW
            BlockPos posSE = player.blockPosition().south().east();
            BlockState blockStateSE = player.level.getBlockState(posSE);
            Block se = blockStateSE.getBlock(); //SE
            BlockPos posNW = player.blockPosition().north().west();
            BlockState blockStateNW = player.level.getBlockState(posNW);
            Block nw = blockStateNW.getBlock(); //NW
            BlockPos posNE = player.blockPosition().north().east();
            BlockState blockStateNE = player.level.getBlockState(posNE);
            Block ne = blockStateNE.getBlock(); //NE
            //Cardinal directions two blocks from player
            BlockPos posWestWest = player.blockPosition().west().west();
            BlockState blockStateWestWest = player.level.getBlockState(posWestWest);
            Block ww = blockStateWestWest.getBlock(); //West x2
            BlockPos posEastEast = player.blockPosition().east().east();
            BlockState blockStateEastEast = player.level.getBlockState(posEastEast);
            Block ee = blockStateEastEast.getBlock(); //East x2
            BlockPos posNorthNorth = player.blockPosition().north().north();
            BlockState blockStateNorthNorth = player.level.getBlockState(posNorthNorth);
            Block nn = blockStateNorthNorth.getBlock(); //North x2
            BlockPos posSouthSouth = player.blockPosition().south().south();
            BlockState blockStateSouthSouth = player.level.getBlockState(posSouthSouth);
            Block ss = blockStateSouthSouth.getBlock(); //South x2
            //Diagonal directions two blocks from the player
            BlockPos posNorthNorthWest = player.blockPosition().north().north().west();
            BlockState blockStateNorthNorthWest = player.level.getBlockState(posNorthNorthWest);
            Block nnw = blockStateNorthNorthWest.getBlock(); //nw x2
            BlockPos posNorthNorthEast = player.blockPosition().north().north().east();
            BlockState blockStateNorthNorthEast = player.level.getBlockState(posNorthNorthEast);
            Block nne = blockStateNorthNorthEast.getBlock(); //ne x2
            BlockPos posSouthSouthWest = player.blockPosition().south().south().west();
            BlockState blockStateSouthSouthWest = player.level.getBlockState(posSouthSouthWest);
            Block ssw = blockStateSouthSouthWest.getBlock(); //sw x2
            BlockPos posSouthSouthEast = player.blockPosition().south().south().east();
            BlockState blockStateSouthSouthEast = player.level.getBlockState(posSouthSouthEast);
            Block sse = blockStateSouthSouthEast.getBlock(); //ne x2
            //Corners, outmost
            BlockPos posNorthNorthWestCorner = player.blockPosition().north().north().west().west();
            BlockState blockStateNorthNorthWestCorner = player.level.getBlockState(posNorthNorthWestCorner);
            Block c1 = blockStateNorthNorthWestCorner.getBlock(); //nw x2
            BlockPos posNorthNorthEastCorner = player.blockPosition().north().north().east().east();
            BlockState blockStateNorthNorthEastCorner = player.level.getBlockState(posNorthNorthEastCorner);
            Block c2 = blockStateNorthNorthEastCorner.getBlock(); //ne x2
            BlockPos posSouthSouthWestCorner = player.blockPosition().south().south().west().west();
            BlockState blockStateSouthSouthWestCorner = player.level.getBlockState(posSouthSouthWestCorner);
            Block c3 = blockStateSouthSouthWestCorner.getBlock(); //sw x2
            BlockPos posSouthSouthEastCorner = player.blockPosition().south().south().east().east();
            BlockState blockStateSouthSouthEastCorner = player.level.getBlockState(posSouthSouthEastCorner);
            Block c4 = blockStateSouthSouthEastCorner.getBlock(); //ne x2

            BlockPos posDown = player.blockPosition().below();
            BlockState blockStateDown = player.level.getBlockState(posDown);
            Block d = blockStateDown.getBlock();
            BlockPos posDownLeft = player.blockPosition().below().west();
            BlockState blockStateDownLeft = player.level.getBlockState(posDownLeft);
            Block dw = blockStateDownLeft.getBlock();
            BlockPos posDownRight = player.blockPosition().below().east();
            BlockState blockStateDownRight = player.level.getBlockState(posDownRight);
            Block de = blockStateDownRight.getBlock();
            BlockPos posDownUp = player.blockPosition().below().north();
            BlockState blockStateDownUp = player.level.getBlockState(posDownUp);
            Block dn = blockStateDownUp.getBlock();
            BlockPos posDownsouth = player.blockPosition().below().south();
            BlockState blockStateDownSouth = player.level.getBlockState(posDownsouth);
            Block ds = blockStateDownSouth.getBlock();
            BlockPos posDownNW = player.blockPosition().below().north().west();
            BlockState blockStateDownNW = player.level.getBlockState(posDownNW);
            Block dnw = blockStateDownNW.getBlock();
            BlockPos posDownNE = player.blockPosition().below().north().east();
            BlockState blockStateDownNE = player.level.getBlockState(posDownNE);
            Block dne = blockStateDownNE.getBlock();
            BlockPos posDownSW = player.blockPosition().below().south().west();
            BlockState blockStateDownSW = player.level.getBlockState(posDownSW);
            Block dsw = blockStateDownSW.getBlock();
            BlockPos posDownSE = player.blockPosition().below().south().east();
            BlockState blockStateDownSE = player.level.getBlockState(posDownSE);
            Block dse = blockStateDownSE.getBlock();

            if (d == Blocks.CAMPFIRE || n == Blocks.CAMPFIRE || s == Blocks.CAMPFIRE ||
                    w == Blocks.CAMPFIRE || e == Blocks.CAMPFIRE ||
                    nw == Blocks.CAMPFIRE || ne == Blocks.CAMPFIRE ||
                    sw == Blocks.CAMPFIRE || se == Blocks.CAMPFIRE ||
                    nn == Blocks.CAMPFIRE || ss == Blocks.CAMPFIRE ||
                    ww == Blocks.CAMPFIRE || ee == Blocks.CAMPFIRE ||
                    nnw == Blocks.CAMPFIRE || nne == Blocks.CAMPFIRE ||
                    sse == Blocks.CAMPFIRE || ssw == Blocks.CAMPFIRE ||
                    c1 == Blocks.CAMPFIRE || c2 == Blocks.CAMPFIRE ||
                    c3 == Blocks.CAMPFIRE || c4 == Blocks.CAMPFIRE ||
                    d == Blocks.LAVA || n == Blocks.LAVA || s == Blocks.LAVA ||
                    w == Blocks.LAVA || e == Blocks.LAVA ||
                    nw == Blocks.LAVA || ne == Blocks.LAVA ||
                    sw == Blocks.LAVA || se == Blocks.LAVA ||
                    nn == Blocks.LAVA || ss == Blocks.LAVA ||
                    ww == Blocks.LAVA || ee == Blocks.LAVA ||
                    nnw == Blocks.LAVA || nne == Blocks.LAVA ||
                    sse == Blocks.LAVA || ssw == Blocks.LAVA ||
                    dw == Blocks.LAVA || de == Blocks.LAVA ||
                    dn == Blocks.LAVA || ds == Blocks.LAVA ||
                    dnw == Blocks.LAVA || dne == Blocks.LAVA ||
                    dsw == Blocks.LAVA || dse == Blocks.LAVA ||
                    c1 == Blocks.LAVA || c2 == Blocks.LAVA ||
                    c3 == Blocks.LAVA || c4 == Blocks.LAVA ||
                    d == Blocks.FIRE || n == Blocks.FIRE || s == Blocks.FIRE ||
                    w == Blocks.FIRE || e == Blocks.FIRE ||
                    nw == Blocks.FIRE || ne == Blocks.FIRE ||
                    sw == Blocks.FIRE || se == Blocks.FIRE ||
                    nn == Blocks.FIRE || ss == Blocks.FIRE ||
                    ww == Blocks.FIRE || ee == Blocks.FIRE ||
                    nnw == Blocks.FIRE || nne == Blocks.FIRE ||
                    sse == Blocks.FIRE || ssw == Blocks.FIRE ||
                    c1 == Blocks.FIRE || c2 == Blocks.FIRE ||
                    c3 == Blocks.FIRE || c4 == Blocks.FIRE)
            {
                if(ZConfigManager.getInstance().enableTemperatureSystem() && heatresist == null && world.isDay()
                        && world.getBiome(pos).getBaseTemperature() >= 1.99F) {
                    overheattimer++;
                }
            }

            if (ZConfigManager.getInstance().useCryostone())
            {
                if (d == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() || n == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() || s == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() ||
                        w == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() || e == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() ||
                        nw == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() || ne == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() ||
                        sw == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() || se == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() ||
                        nn == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() || ss == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() ||
                        ww == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() || ee == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() ||
                        nnw == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() || nne == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() ||
                        sse == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() || ssw == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() ||
                        c1 == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() || c2 == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() ||
                        c3 == SpecialBlocksInit.ANCIENT_CRYOSTONE.get() || c4 == SpecialBlocksInit.ANCIENT_CRYOSTONE.get()) {
                    if(world.getBiome(pos).getBaseTemperature() >= 1.99F && world.isDay() && ZConfigManager.getInstance().enableTemperatureSystem()) {
                        Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                        player.removeEffectNoUpdate(oppositeresist);
                        player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), ZConfigManager.getInstance().campfireDuration(), 0));
                    }
                }
        }
            if (!ZConfigManager.getInstance().useCryostone())
            {
                if (d == Blocks.SOUL_CAMPFIRE || n == Blocks.SOUL_CAMPFIRE || s == Blocks.SOUL_CAMPFIRE ||
                        w == Blocks.SOUL_CAMPFIRE || e == Blocks.SOUL_CAMPFIRE ||
                        nw == Blocks.SOUL_CAMPFIRE || ne == Blocks.SOUL_CAMPFIRE ||
                        sw == Blocks.SOUL_CAMPFIRE || se == Blocks.SOUL_CAMPFIRE ||
                        nn == Blocks.SOUL_CAMPFIRE || ss == Blocks.SOUL_CAMPFIRE ||
                        ww == Blocks.SOUL_CAMPFIRE || ee == Blocks.SOUL_CAMPFIRE ||
                        nnw == Blocks.SOUL_CAMPFIRE || nne == Blocks.SOUL_CAMPFIRE ||
                        sse == Blocks.SOUL_CAMPFIRE || ssw == Blocks.SOUL_CAMPFIRE ||
                        c1 == Blocks.SOUL_CAMPFIRE || c2 == Blocks.SOUL_CAMPFIRE ||
                        c3 == Blocks.SOUL_CAMPFIRE || c4 == Blocks.SOUL_CAMPFIRE)
                {
                    if(world.getBiome(pos).getBaseTemperature() >= 1.99F && world.isDay() && ZConfigManager.getInstance().enableTemperatureSystem()) {
                        Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                        player.removeEffectNoUpdate(oppositeresist);
                        player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), ZConfigManager.getInstance().campfireDuration(), 0));
                    }
                }
            }


        }
    }

    @SubscribeEvent
    public void armorcheck(TickEvent.PlayerTickEvent event) {
        LivingEntity player = event.player;
        ItemStack helmstack = player.getItemBySlot(EquipmentSlotType.HEAD);
        ItemStack cheststack = player.getItemBySlot(EquipmentSlotType.CHEST);
        ItemStack legstack = player.getItemBySlot(EquipmentSlotType.LEGS);
        ItemStack feetstack = player.getItemBySlot(EquipmentSlotType.FEET);
        if (ZConfigManager.getInstance().enableTemperatureSystem()) {
            if (ZConfigManager.getInstance().armorSetCount() == 4) {
                if (player.hasItemInSlot(EquipmentSlotType.HEAD) &&
                        helmstack.getItem() == ItemInit.DESERT_VOE_HEADBAND.get() &&
                        player.hasItemInSlot(EquipmentSlotType.CHEST) &&
                        cheststack.getItem() == ItemInit.DESERT_VOE_SPAULDER.get() &&
                        player.hasItemInSlot(EquipmentSlotType.LEGS) &&
                        legstack.getItem() == ItemInit.DESERT_VOE_TROUSERS.get() &&
                        player.hasItemInSlot(EquipmentSlotType.FEET) &&
                        feetstack.getItem() == ItemInit.DESERT_VOE_BOOTS.get()) {
                    if(ZConfigManager.getInstance().enableTemperatureSystem()) {
                        overheattimer = 0;
                    }
                    System.out.println("Immunity to heat!");
                }
            }
            if (ZConfigManager.getInstance().armorSetCount() == 3) {
                if (player.hasItemInSlot(EquipmentSlotType.HEAD) &&
                        helmstack.getItem() == ItemInit.DESERT_VOE_HEADBAND.get() &&
                        player.hasItemInSlot(EquipmentSlotType.CHEST) &&
                        cheststack.getItem() == ItemInit.DESERT_VOE_SPAULDER.get() &&
                        player.hasItemInSlot(EquipmentSlotType.LEGS) &&
                        legstack.getItem() == ItemInit.DESERT_VOE_TROUSERS.get()) {
                    if(ZConfigManager.getInstance().enableTemperatureSystem()) {
                        overheattimer = 0;
                    }
                    System.out.println("Immunity to heat!");
                }
            }
        }
    }

    @SubscribeEvent
    public void leafcheck(TickEvent.PlayerTickEvent event) {
        LivingEntity player = event.player;
        World world = player.level;
        BlockPos pos = new BlockPos(player.blockPosition());
        ResourceLocation loc = world.registryAccess().registryOrThrow(Registry.BIOME_REGISTRY).getKey(world.getBiome(pos));
        Biome biome = ForgeRegistries.BIOMES.getValue(loc);
            if (world.getBiome(pos).getBaseTemperature() >= 1.5F && player.isInWaterOrRain()) {
                if (ZConfigManager.getInstance().enableTemperatureSystem()) {
                    overheattimer--;
                    System.out.println("Temp dropping!");
                }
            }

            if (world.getBiome(pos).getBaseTemperature() >= 1.5F &&
                    world.getBiome(pos).getBaseTemperature() < 2.0F && world.isNight()) {
                if (ZConfigManager.getInstance().enableTemperatureSystem()) {
                    overheattimer--;
                    System.out.println("Temp dropping!");
                }
            }

            BlockPos posAbove = player.blockPosition().above().above();
            BlockState blockStateAbove = player.level.getBlockState(posAbove);
            Material above = blockStateAbove.getMaterial(); //1 block above player-head
            BlockPos posAbove1 = player.blockPosition().above().above().above();
            BlockState blockStateAbove1 = player.level.getBlockState(posAbove1);
            Material above1 = blockStateAbove1.getMaterial(); //2 blocks above player-head
            BlockPos posAbove2 = player.blockPosition().above().above().above().above();
            BlockState blockStateAbove2 = player.level.getBlockState(posAbove2);
            Material above2 = blockStateAbove2.getMaterial(); //3 blocks above player-head
            BlockPos posAbove3 = player.blockPosition().above().above().above().above().above();
            BlockState blockStateAbove3 = player.level.getBlockState(posAbove3);
            Material above3 = blockStateAbove3.getMaterial(); //4 blocks above player-head
            BlockPos posAbove4 = player.blockPosition().above().above().above().above().above().above();
            BlockState blockStateAbove4 = player.level.getBlockState(posAbove4);
            Material above4 = blockStateAbove4.getMaterial(); //5 blocks above player-head
            if (above == Material.LEAVES || above1 == Material.LEAVES || above2 == Material.LEAVES ||
                    above3 == Material.LEAVES || above4 == Material.LEAVES) {
                if (world.getBiome(pos).getBaseTemperature() >= 1.99F && world.isDay() && ZConfigManager.getInstance().enableTemperatureSystem() == true) {
                    overheattimer--;
                    System.out.println("Temp dropping!");
                }
            }
        }

    @SubscribeEvent
    public void tiertwohotbiomecheck(TickEvent.PlayerTickEvent event) {
        LivingEntity player = event.player;
        World world = player.level;
        ItemStack helmstack = player.getItemBySlot(EquipmentSlotType.HEAD);
        Effect heatresisteffect = EffectInit.HEAT_RESIST_EFFECT.get();
        EffectInstance heatresist = player.getEffect(heatresisteffect);
        BlockPos pos = new BlockPos(player.blockPosition());

        //Check deserts/badlands at day time and if the sky is visible with a condition of 'if heat-resist null'
        if (world.getBiome(pos).getBaseTemperature() >= 1.99F && world.isDay() && world.canSeeSky(pos) && heatresist == null
                && ZConfigManager.getInstance().enableTemperatureSystem() == true)
        {
            overheattimer++;
            System.out.println("Overheating!");
        }

        //Check for circlet bonuses
        if (world.getBiome(pos).getBaseTemperature() >= 1.99F)
        {
            player.getCapability(PlayerDataCapabilityProvider.ZPLAYERDATA).ifPresent(h ->
            {
                if (h.getHeatResistCooldown() == 0 && player.hasItemInSlot(EquipmentSlotType.HEAD) &&
                        helmstack.getItem() == ItemInit.SAPPHIRE_CIRCLET.get())
                {
                    if(ZConfigManager.getInstance().enableTemperatureSystem() == true)
                    {
                        Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                        player.removeEffectNoUpdate(oppositeresist);
                        player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), circletmodifier, 0));
                        h.setHeatResistCooldown(circletmodifier);
                    }
                }

                if (h.getHeatResistCooldown() == 0 && player.hasItemInSlot(EquipmentSlotType.HEAD) &&
                        helmstack.getItem() == ItemInit.UPGRADED_SAPPHIRE_CIRCLET.get())
                {
                    if(ZConfigManager.getInstance().enableTemperatureSystem() == true)
                    {
                        Effect oppositeresist = EffectInit.COLD_RESIST_EFFECT.get();
                        player.removeEffectNoUpdate(oppositeresist);
                        player.addEffect(new EffectInstance(EffectInit.HEAT_RESIST_EFFECT.get(), circletmodifier*2, 0));
                        h.setHeatResistCooldown(circletmodifier*2);
                    }
                }

            });
        }

        //Check if the timer is more than the specified config-time then hurt the player
        if (overheattimer >= ZConfigManager.getInstance().defaultTempTimerThreshold())
        {
            if(world.canSeeSky(pos)
                && world.isDay() && world.getBiome(pos).getBaseTemperature() == 1.99F && ZConfigManager.getInstance().enableTemperatureSystem())
            {
                    player.hurt(DamageSource.GENERIC, ZConfigManager.getInstance().tempDamage());
                    overheattimer = 0;
            }
        }

        }
}










