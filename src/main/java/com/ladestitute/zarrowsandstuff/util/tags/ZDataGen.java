package com.ladestitute.zarrowsandstuff.util.tags;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;

@Mod.EventBusSubscriber(modid = ZArrowsMain.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ZDataGen
{
    @SubscribeEvent
    public static void gatherData(GatherDataEvent event)
    {
        DataGenerator generator = event.getGenerator();

        if (event.includeServer())
        {
            ZArrowsMain.LOGGER.debug("Starting Server Data Generators");
            ZTagsList.BlockTagsDataGen blockTagsProvider = new ZTagsList.BlockTagsDataGen(event.getGenerator(), event.getExistingFileHelper());
            generator.addProvider(new ZTagsList.BlockTagsDataGen(generator, event.getExistingFileHelper()));
            generator.addProvider(new ZTagsList.ItemTagsDataGen(generator, blockTagsProvider, event.getExistingFileHelper()));
        }
        if (event.includeClient())
        {
            ZArrowsMain.LOGGER.debug("Starting Client Data Generators");
        }
    }
}
