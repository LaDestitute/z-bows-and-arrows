package com.ladestitute.zarrowsandstuff.util.tags;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.Tags;

public class ZTags
{
    public static void init ()
    {
        Blocks.init();
        Items.init();
    }

    public static class Items
    {
        private static void init(){}

        //Todo: implement gears tag

        public static final Tags.IOptionalNamedTag<Item> GEMS = tag("gems");
        public static final Tags.IOptionalNamedTag<Item> GEMS_TOPAZ = tag("gems/topaz");
        public static final Tags.IOptionalNamedTag<Item> GEMS_OPAL = tag("gems/opal");
        public static final Tags.IOptionalNamedTag<Item> GEMS_RUBY = tag("gems/ruby");
        public static final Tags.IOptionalNamedTag<Item> GEMS_SAPPHIRE = tag("gems/sapphire");
        public static final Tags.IOptionalNamedTag<Item> GEMS_AMBER = tag("gems/amber");

        public static final Tags.IOptionalNamedTag<Item> MATERIAL = tag("material");
        public static final Tags.IOptionalNamedTag<Item> MATERIAL_WOOL = tag("material_wool");

        public static final Tags.IOptionalNamedTag<Item> FOOD = tag("food");
        public static final Tags.IOptionalNamedTag<Item> FOOD_SUGAR = tag("food_sugar");
        public static final Tags.IOptionalNamedTag<Item> FOOD_WILDBERRY = tag("food_wildberry");
        public static final Tags.IOptionalNamedTag<Item> FOOD_MEAT = tag("food_meat");
        public static final Tags.IOptionalNamedTag<Item> FOOD_ALLMEAT = tag("food_allmeat");
        public static final Tags.IOptionalNamedTag<Item> FOOD_MUSHROOM = tag("food_mushroom");
        public static final Tags.IOptionalNamedTag<Item> FOOD_VEGETABLE = tag("food_vegetable");
        public static final Tags.IOptionalNamedTag<Item> FOOD_FISH = tag("food_fish");
        public static final Tags.IOptionalNamedTag<Item> FOOD_SEAFOOD = tag("food_seafood");
        public static final Tags.IOptionalNamedTag<Item> FOOD_HONEY = tag("food_honey");
        public static final Tags.IOptionalNamedTag<Item> FOOD_BIRD = tag("food_bird");
        public static final Tags.IOptionalNamedTag<Item> FOOD_FRUIT = tag("food_fruit");
        public static final Tags.IOptionalNamedTag<Item> FOOD_FRUIT2 = tag("food_fruit2");
        public static final Tags.IOptionalNamedTag<Item> FOOD_FRUIT3 = tag("food_fruit3");
        public static final Tags.IOptionalNamedTag<Item> FOOD_FRUIT4 = tag("food_fruit4");
        public static final Tags.IOptionalNamedTag<Item> FOOD_FRUIT5 = tag("food_fruit5");
        public static final Tags.IOptionalNamedTag<Item> FOOD_SPICY1 = tag("food_spicy1");
        public static final Tags.IOptionalNamedTag<Item> FOOD_SPICY2 = tag("food_spicy2");
        public static final Tags.IOptionalNamedTag<Item> FOOD_CHILLY1 = tag("food_chilly1");
        public static final Tags.IOptionalNamedTag<Item> FOOD_CHILLY2 = tag("food_chilly2");
        public static final Tags.IOptionalNamedTag<Item> FOOD_ENERGIZING1 = tag("food_energizing1");

        private static Tags.IOptionalNamedTag<Item> tag(String name)
        {
            return ItemTags.createOptional(new ResourceLocation("forge", name));
        }
    }
    public static class Blocks
    {
        private static void init(){}

        public static final Tags.IOptionalNamedTag<Block> ORES = tag("ores");

        private static Tags.IOptionalNamedTag<Block> tag(String name)
        {
            return BlockTags.createOptional(new ResourceLocation("forge", name));
        }
    }
}
