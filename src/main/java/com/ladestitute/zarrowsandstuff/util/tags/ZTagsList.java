package com.ladestitute.zarrowsandstuff.util.tags;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.registries.ItemInit;
import net.minecraft.data.BlockTagsProvider;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.ItemTagsProvider;
import net.minecraft.item.Items;
import net.minecraftforge.common.data.ExistingFileHelper;

public class ZTagsList {
    public static class ItemTagsDataGen extends ItemTagsProvider {

        public ItemTagsDataGen(DataGenerator generatorIn, BlockTagsProvider blockTagsProvider,
                               ExistingFileHelper existingFileHelper) {
            super(generatorIn, blockTagsProvider, ZArrowsMain.MOD_ID, existingFileHelper);
        }

        @Override
        protected void addTags() {
            // To refresh tags after edits below, run preparerundata then run rundata
            //copy the tags folder to your mod's data folder, ie resources/data/modid/tags

            tag(ZTags.Items.GEMS).addTag(ZTags.Items.GEMS_AMBER);
            tag(ZTags.Items.GEMS_AMBER).add(ItemInit.AMBER.get());

            tag(ZTags.Items.GEMS).addTag(ZTags.Items.GEMS_OPAL);
            tag(ZTags.Items.GEMS_OPAL).add(ItemInit.OPAL.get());
            tag(ZTags.Items.GEMS).addTag(ZTags.Items.GEMS_RUBY);
            tag(ZTags.Items.GEMS_RUBY).add(ItemInit.RUBY.get());
            tag(ZTags.Items.GEMS).addTag(ZTags.Items.GEMS_SAPPHIRE);
            tag(ZTags.Items.GEMS_SAPPHIRE).add(ItemInit.SAPPHIRE.get());
            tag(ZTags.Items.GEMS).addTag(ZTags.Items.GEMS_TOPAZ);
            tag(ZTags.Items.GEMS_TOPAZ).add(ItemInit.TOPAZ.get());

            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_MEAT);
            tag(ZTags.Items.FOOD_MEAT).add(Items.BEEF);
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_MEAT);
            tag(ZTags.Items.FOOD_MEAT).add(Items.MUTTON);
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_MEAT);
            tag(ZTags.Items.FOOD_MEAT).add(Items.RABBIT);
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_MEAT);
            tag(ZTags.Items.FOOD_MEAT).add(Items.PORKCHOP);

            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_ALLMEAT);
            tag(ZTags.Items.FOOD_ALLMEAT).add(Items.BEEF);
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_ALLMEAT);
            tag(ZTags.Items.FOOD_ALLMEAT).add(Items.MUTTON);
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_ALLMEAT);
            tag(ZTags.Items.FOOD_ALLMEAT).add(Items.RABBIT);
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_ALLMEAT);
            tag(ZTags.Items.FOOD_ALLMEAT).add(Items.PORKCHOP);
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_ALLMEAT);
            tag(ZTags.Items.FOOD_ALLMEAT).add(Items.CHICKEN);

            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_BIRD);
            tag(ZTags.Items.FOOD_BIRD).add(Items.CHICKEN);

            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_SPICY1);
            tag(ZTags.Items.FOOD_SPICY1).add(ItemInit.SPICY_PEPPER.get());

            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_CHILLY1);
            tag(ZTags.Items.FOOD_CHILLY1).add(ItemInit.HYDROMELON_FRUIT.get());

            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_ENERGIZING1);
            tag(ZTags.Items.FOOD_ENERGIZING1).add(ItemInit.STAMELLA_SHROOM.get());

            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT);
            tag(ZTags.Items.FOOD_FRUIT).add(Items.MELON_SLICE);
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT);
            tag(ZTags.Items.FOOD_FRUIT).add(Items.CHORUS_FRUIT);
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT);
            tag(ZTags.Items.FOOD_FRUIT).add(ItemInit.PALM_FRUIT.get());
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT);
            tag(ZTags.Items.FOOD_FRUIT).add(ItemInit.MIGHTY_BANANAS.get());
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT);
            tag(ZTags.Items.FOOD_FRUIT).add(ItemInit.VOLTFRUIT.get());

            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT2);
            tag(ZTags.Items.FOOD_FRUIT2).add(Items.APPLE);
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT2);
            tag(ZTags.Items.FOOD_FRUIT2).add(Items.SWEET_BERRIES);
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT2);
            tag(ZTags.Items.FOOD_FRUIT).add(ItemInit.PALM_FRUIT.get());
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT2);
            tag(ZTags.Items.FOOD_FRUIT).add(ItemInit.MIGHTY_BANANAS.get());
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT2);
            tag(ZTags.Items.FOOD_FRUIT).add(ItemInit.VOLTFRUIT.get());

            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT4);
            tag(ZTags.Items.FOOD_FRUIT4).add(ItemInit.HYDROMELON_FRUIT.get());
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT4);
            tag(ZTags.Items.FOOD_FRUIT4).add(ItemInit.SPICY_PEPPER.get());

            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT5);
            tag(ZTags.Items.FOOD_FRUIT5).add(Items.MELON_SLICE);
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT5);
            tag(ZTags.Items.FOOD_FRUIT5).add(Items.CHORUS_FRUIT);
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT5);
            tag(ZTags.Items.FOOD_FRUIT5).add(Items.SWEET_BERRIES);
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT5);
            tag(ZTags.Items.FOOD_FRUIT5).add(ItemInit.HYDROMELON_FRUIT.get());
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT5);
            tag(ZTags.Items.FOOD_FRUIT5).add(ItemInit.SPICY_PEPPER.get());
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT5);
            tag(ZTags.Items.FOOD_FRUIT).add(ItemInit.PALM_FRUIT.get());
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT5);
            tag(ZTags.Items.FOOD_FRUIT).add(ItemInit.MIGHTY_BANANAS.get());
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_FRUIT5);
            tag(ZTags.Items.FOOD_FRUIT).add(ItemInit.VOLTFRUIT.get());

            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_WILDBERRY);
            tag(ZTags.Items.FOOD_WILDBERRY).add(Items.SWEET_BERRIES);

            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_MUSHROOM);
            tag(ZTags.Items.FOOD_MUSHROOM).add(Items.RED_MUSHROOM);
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_MUSHROOM);
            tag(ZTags.Items.FOOD_MUSHROOM).add(Items.BROWN_MUSHROOM);
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_MUSHROOM);
            tag(ZTags.Items.FOOD_MUSHROOM).add(Items.WARPED_FUNGUS);
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_MUSHROOM);
            tag(ZTags.Items.FOOD_MUSHROOM).add(Items.CRIMSON_FUNGUS);

            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_VEGETABLE);
            tag(ZTags.Items.FOOD_VEGETABLE).add(Items.BEETROOT);
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_VEGETABLE);
            tag(ZTags.Items.FOOD_VEGETABLE).add(Items.POTATO);
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_VEGETABLE);
            tag(ZTags.Items.FOOD_VEGETABLE).add(Items.CARROT);

            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_SEAFOOD);
            tag(ZTags.Items.FOOD_SEAFOOD).add(Items.PUFFERFISH);
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_SEAFOOD);
            tag(ZTags.Items.FOOD_SEAFOOD).add(Items.TROPICAL_FISH);
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_SEAFOOD);
            tag(ZTags.Items.FOOD_SEAFOOD).add(Items.COD);
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_SEAFOOD);
            tag(ZTags.Items.FOOD_SEAFOOD).add(Items.SALMON);

            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_HONEY);
            tag(ZTags.Items.FOOD_HONEY).add(Items.HONEYCOMB);
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_HONEY);
            tag(ZTags.Items.FOOD_HONEY).add(Items.HONEY_BOTTLE);
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_HONEY);
            tag(ZTags.Items.FOOD_HONEY).add(ItemInit.COURSER_BEE_HONEY.get());

            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_SUGAR);
            tag(ZTags.Items.FOOD_SUGAR).add(Items.SUGAR);
            tag(ZTags.Items.FOOD).addTag(ZTags.Items.FOOD_SUGAR);
            tag(ZTags.Items.FOOD_SUGAR).add(Items.SUGAR_CANE);
        }
    }

    public static class BlockTagsDataGen extends BlockTagsProvider {

        public BlockTagsDataGen(DataGenerator generatorIn, ExistingFileHelper existingFileHelper) {
            super(generatorIn, ZArrowsMain.MOD_ID, existingFileHelper);
        }

        @Override
        protected void addTags() {
        }
    }
}

