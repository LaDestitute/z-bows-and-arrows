package com.ladestitute.zarrowsandstuff.world.feature;

import java.util.Random;

import com.ladestitute.zarrowsandstuff.blocks.natural.plants.MightyBananaLeavesBlock;
import com.ladestitute.zarrowsandstuff.blocks.natural.plants.PalmFruitBlock;
import com.ladestitute.zarrowsandstuff.registries.BlockInit;
import com.mojang.serialization.Codec;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.LeavesBlock;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.ISeedReader;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.IWorldGenerationBaseReader;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.NoFeatureConfig;

public class PalmTreeFeature extends Feature<NoFeatureConfig> {

    private static final Direction[] DIRECTIONS = new Direction[] { Direction.NORTH, Direction.EAST, Direction.SOUTH,
            Direction.WEST };

    //Jungle leaves/logs as placeholders, will make unique foilage and logs soon; banana logs will likely replicate jungle logs with how they grow cocoa
    private static final BlockState LOG = BlockInit.PALM_LOG.get().defaultBlockState();
    private static final BlockState BANANA = BlockInit.PALM_FRUIT_BLOCK.get().defaultBlockState();
    private static final BlockState LEAVES = BlockInit.MIGHTY_BANANAS_LEAVES.get().defaultBlockState();

    public PalmTreeFeature(Codec<NoFeatureConfig> codec) {
        super(codec);
    }

    @SuppressWarnings("deprecation")
    public boolean isAirOrLeaves(IWorldGenerationBaseReader reader, BlockPos pos) {
        if (!(reader instanceof IWorldReader)) {
            return reader.isStateAtPosition(pos, state -> state.isAir() || state.is(BlockTags.LEAVES));
        } else {
            return reader.isStateAtPosition(pos, state -> state.canBeReplacedByLeaves((IWorldReader) reader, pos));
        }
    }

    @Override
    public boolean place(ISeedReader reader, ChunkGenerator generator, Random rand, BlockPos pos,
                         NoFeatureConfig config) {
        if (reader.getBlockState(pos.below()).getBlock() != Blocks.WATER) {

            while (pos.getY() > 1 && isAirOrLeaves(reader, pos)) {
                pos = pos.below();
            }

            // Trunks
            int height = 9;
            if (reader.getBlockState(pos.below()) == Blocks.WATER.defaultBlockState() ||
                    reader.getBlockState(pos.below().below()) == Blocks.WATER.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below()) == Blocks.WATER.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below().below()) == Blocks.WATER.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below().below().below()) == Blocks.WATER.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below().below().below().below()) == Blocks.WATER.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below().below().below().below().below()) == Blocks.WATER.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below().below().below().below().below().below()) == Blocks.WATER.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below().below().below().below().below().below().below()) == Blocks.WATER.defaultBlockState()) {
                return false;
            }
            if (reader.getBlockState(pos.below()) == Blocks.AIR.defaultBlockState() ||
                    reader.getBlockState(pos.below().below()) == Blocks.AIR.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below()) == Blocks.AIR.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below().below()) == Blocks.AIR.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below().below().below()) == Blocks.AIR.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below().below().below().below()) == Blocks.AIR.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below().below().below().below().below()) == Blocks.AIR.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below().below().below().below().below().below()) == Blocks.AIR.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below().below().below().below().below().below().below()) == Blocks.AIR.defaultBlockState()) {
                return false;
            }
            if (reader.getBlockState(pos.below()) != Blocks.AIR.defaultBlockState() &&
                    reader.getBlockState(pos.below()) != Blocks.WATER.defaultBlockState() &&
                    reader.getBlockState(pos.below().east()) != Blocks.WATER.defaultBlockState()
                    && reader.getBlockState(pos.below().west()) != Blocks.WATER.defaultBlockState()
                    && reader.getBlockState(pos.below().north()) != Blocks.WATER.defaultBlockState()
                    && reader.getBlockState(pos.below().south()) != Blocks.WATER.defaultBlockState()) {
                if (reader.getBlockState(pos.below()) == Blocks.SAND.defaultBlockState()) {
                    if (pos.getY() >= 1 && pos.getY() + height + 1 < reader.getMaxBuildHeight()) {
                        for (int i = pos.getY() + 1; i < pos.getY() + height; i++) {
                            reader.setBlock(new BlockPos(pos.getX(), i, pos.getZ()), LOG, 3);
                            reader.setBlock(pos.below(), Blocks.SAND.defaultBlockState(), 3);
                            int banana = reader.getRandom().nextInt(11);
                            if (banana == 0) {
                                //no bananas for you! D:<
                            }
                            if (banana == 1) {
                                reader.setBlock(pos.east().above().above().above().above().above().above().above(), BANANA.setValue(PalmFruitBlock.FACING, Direction.EAST), 3);
                            }
                            if (banana == 2) {
                                reader.setBlock(pos.west().above().above().above().above().above().above().above(), BANANA.setValue(PalmFruitBlock.FACING, Direction.WEST), 3);
                            }
                            if (banana == 3) {
                                reader.setBlock(pos.north().above().above().above().above().above().above().above(), BANANA.setValue(PalmFruitBlock.FACING, Direction.NORTH), 3);
                            }
                            if (banana == 4) {
                                reader.setBlock(pos.south().above().above().above().above().above().above().above(), BANANA.setValue(PalmFruitBlock.FACING, Direction.SOUTH), 3);
                            }

                            reader.setBlock(new BlockPos(pos.getX(), pos.getY() + height, pos.getZ()), LEAVES, 3);
                            reader.setBlock(new BlockPos(pos.getX(), pos.getY() + height, pos.getZ()).below().east(), LEAVES.setValue(MightyBananaLeavesBlock.FACING, Direction.EAST), 3);
                            reader.setBlock(new BlockPos(pos.getX(), pos.getY() + height, pos.getZ()).below().west(), LEAVES.setValue(MightyBananaLeavesBlock.FACING, Direction.WEST), 3);
                            reader.setBlock(new BlockPos(pos.getX(), pos.getY() + height, pos.getZ()).below().north(), LEAVES.setValue(MightyBananaLeavesBlock.FACING, Direction.NORTH), 3);
                            reader.setBlock(new BlockPos(pos.getX(), pos.getY() + height, pos.getZ()).below().south(), LEAVES.setValue(MightyBananaLeavesBlock.FACING, Direction.SOUTH), 3);

                        }
                    }
                }
            } else {
                return false;
            }
        }

        return true;
    }

}

