package com.ladestitute.zarrowsandstuff.world.feature;

import java.util.Random;

import com.ladestitute.zarrowsandstuff.blocks.natural.plants.MightyBananaLeavesBlock;
import com.ladestitute.zarrowsandstuff.blocks.natural.plants.PalmFruitBlock;
import com.ladestitute.zarrowsandstuff.registries.BlockInit;
import com.mojang.serialization.Codec;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.LeavesBlock;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.ISeedReader;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.IWorldGenerationBaseReader;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.NoFeatureConfig;

public class VoltfruitCactusFeature extends Feature<NoFeatureConfig> {

    private static final Direction[] DIRECTIONS = new Direction[] { Direction.NORTH, Direction.EAST, Direction.SOUTH,
            Direction.WEST };

    //Jungle leaves/logs as placeholders, will make unique foilage and logs soon; banana logs will likely replicate jungle logs with how they grow cocoa
    private static final BlockState LOG = BlockInit.VOLTFRUIT_CACTUS.get().defaultBlockState();
    private static final BlockState BANANA = BlockInit.VOLTFRUIT_PLANT.get().defaultBlockState();
    private static final BlockState LEAVES = BlockInit.MIGHTY_BANANAS_LEAVES.get().defaultBlockState();

    public VoltfruitCactusFeature(Codec<NoFeatureConfig> codec) {
        super(codec);
    }

    @SuppressWarnings("deprecation")
    public boolean isAirOrLeaves(IWorldGenerationBaseReader reader, BlockPos pos) {
        if (!(reader instanceof IWorldReader)) {
            return reader.isStateAtPosition(pos, state -> state.isAir() || state.is(BlockTags.LEAVES));
        } else {
            return reader.isStateAtPosition(pos, state -> state.canBeReplacedByLeaves((IWorldReader) reader, pos));
        }
    }

    @Override
    public boolean place(ISeedReader reader, ChunkGenerator generator, Random rand, BlockPos pos,
                         NoFeatureConfig config) {
        if (reader.getBlockState(pos.below()).getBlock() != Blocks.WATER) {

            while (pos.getY() > 1 && isAirOrLeaves(reader, pos)) {
                pos = pos.below();
            }

            // Trunks
            Random rand1 = new Random();
            int bonusheight = rand1.nextInt(5);
            int height = 6+bonusheight;
            if (reader.getBlockState(pos.below()) == Blocks.WATER.defaultBlockState() ||
                    reader.getBlockState(pos.below().below()) == Blocks.WATER.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below()) == Blocks.WATER.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below().below()) == Blocks.WATER.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below().below().below()) == Blocks.WATER.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below().below().below().below()) == Blocks.WATER.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below().below().below().below().below()) == Blocks.WATER.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below().below().below().below().below().below()) == Blocks.WATER.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below().below().below().below().below().below().below()) == Blocks.WATER.defaultBlockState()) {
                return false;
            }
            if (reader.getBlockState(pos.below()) == Blocks.AIR.defaultBlockState() ||
                    reader.getBlockState(pos.below().below()) == Blocks.AIR.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below()) == Blocks.AIR.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below().below()) == Blocks.AIR.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below().below().below()) == Blocks.AIR.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below().below().below().below()) == Blocks.AIR.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below().below().below().below().below()) == Blocks.AIR.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below().below().below().below().below().below()) == Blocks.AIR.defaultBlockState() ||
                    reader.getBlockState(pos.below().below().below().below().below().below().below().below().below()) == Blocks.AIR.defaultBlockState()) {
                return false;
            }
            if (reader.getBlockState(pos.below()) != Blocks.AIR.defaultBlockState() &&
                    reader.getBlockState(pos.below()) != Blocks.WATER.defaultBlockState() &&
                    reader.getBlockState(pos.below().east()) != Blocks.WATER.defaultBlockState()
                    && reader.getBlockState(pos.below().west()) != Blocks.WATER.defaultBlockState()
                    && reader.getBlockState(pos.below().north()) != Blocks.WATER.defaultBlockState()
                    && reader.getBlockState(pos.below().south()) != Blocks.WATER.defaultBlockState()) {
                if (reader.getBlockState(pos.below()) == Blocks.SAND.defaultBlockState()) {
                    if (pos.getY() >= 1 && pos.getY() + height + 1 < reader.getMaxBuildHeight()) {
                        for (int i = pos.getY() + 1; i < pos.getY() + height; i++) {
                            reader.setBlock(new BlockPos(pos.getX(), i, pos.getZ()), LOG, 3);
                            reader.setBlock(pos.below(), Blocks.SAND.defaultBlockState(), 3);
                            int banana = reader.getRandom().nextInt(11);
                            if (banana == 0) {
                                //no bananas for you! D:<
                            }
                            if (banana == 1) {
                              //  reader.setBlock(pos.east().above().above().above().above().above().above().above(), BANANA.setValue(PalmFruitBlock.FACING, Direction.EAST), 3);
                            }
                            if (banana == 2) {
                              //  reader.setBlock(pos.west().above().above().above().above().above().above().above(), BANANA.setValue(PalmFruitBlock.FACING, Direction.WEST), 3);
                            }
                            if (banana == 3) {
                               // reader.setBlock(pos.north().above().above().above().above().above().above().above(), BANANA.setValue(PalmFruitBlock.FACING, Direction.NORTH), 3);
                            }
                            if (banana == 4) {
                              //  reader.setBlock(pos.south().above().above().above().above().above().above().above(), BANANA.setValue(PalmFruitBlock.FACING, Direction.SOUTH), 3);
                            }

                            int direction = rand1.nextInt(4);
                            int voltfruitchanceeast = rand1.nextInt(3);
                            int voltfruitchancewest = rand1.nextInt(3);
                            int voltfruitchancenorth = rand1.nextInt(3);
                            int voltfruitchancesouth = rand1.nextInt(3);
                            if(direction == 0) {
                                reader.setBlock(new BlockPos(pos.getX(), pos.getY() + height,
                                        pos.getZ()).below().below().below().east(), LOG, 3);
                                reader.setBlock(new BlockPos(pos.getX(), pos.getY() + height,
                                        pos.getZ()).below().below().below().east().east(), LOG, 3);
                                reader.setBlock(new BlockPos(pos.getX(), pos.getY() + height,
                                        pos.getZ()).below().below().below().east().east().above(), LOG, 3);
                                if(voltfruitchanceeast == 0)
                                {
                                    reader.setBlock(new BlockPos(pos.getX(), pos.getY() + height,
                                            pos.getZ()).below().below().below().east().east().above().above(), BANANA, 3);
                                }
                            }
                            if(direction == 1) {
                                reader.setBlock(new BlockPos(pos.getX(), pos.getY() + height,
                                        pos.getZ()).below().below().below().west(), LOG, 3);
                                reader.setBlock(new BlockPos(pos.getX(), pos.getY() + height,
                                        pos.getZ()).below().below().below().west().west(), LOG, 3);
                                reader.setBlock(new BlockPos(pos.getX(), pos.getY() + height,
                                        pos.getZ()).below().below().below().west().west().above(), LOG, 3);
                                if(voltfruitchancewest == 0)
                                {
                                    reader.setBlock(new BlockPos(pos.getX(), pos.getY() + height,
                                            pos.getZ()).below().below().below().west().west().above().above(), BANANA, 3);
                                }
                            }
                            if(direction == 2) {
                                reader.setBlock(new BlockPos(pos.getX(), pos.getY() + height,
                                        pos.getZ()).below().below().below().north(), LOG, 3);
                                reader.setBlock(new BlockPos(pos.getX(), pos.getY() + height,
                                        pos.getZ()).below().below().below().north().north(), LOG, 3);
                                reader.setBlock(new BlockPos(pos.getX(), pos.getY() + height,
                                        pos.getZ()).below().below().below().north().north().above(), LOG, 3);
                                if(voltfruitchancenorth == 0)
                                {
                                    reader.setBlock(new BlockPos(pos.getX(), pos.getY() + height,
                                            pos.getZ()).below().below().below().north().north().above().above(), BANANA, 3);
                                }
                            }
                            if(direction == 3) {
                                reader.setBlock(new BlockPos(pos.getX(), pos.getY() + height,
                                        pos.getZ()).below().below().below().south(), LOG, 3);
                                reader.setBlock(new BlockPos(pos.getX(), pos.getY() + height,
                                        pos.getZ()).below().below().below().south().south(), LOG, 3);
                                reader.setBlock(new BlockPos(pos.getX(), pos.getY() + height,
                                        pos.getZ()).below().below().below().south().south().above(), LOG, 3);
                                if(voltfruitchancesouth == 0)
                                {
                                    reader.setBlock(new BlockPos(pos.getX(), pos.getY() + height,
                                            pos.getZ()).below().below().below().south().south().above().above(), BANANA, 3);
                                }
                            }
                        }
                    }
                }
            } else {
                return false;
            }
        }

        return true;
    }

}

