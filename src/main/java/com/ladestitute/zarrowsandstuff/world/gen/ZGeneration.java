package com.ladestitute.zarrowsandstuff.world.gen;

import com.ladestitute.zarrowsandstuff.ZArrowsMain;
import com.ladestitute.zarrowsandstuff.registries.ConfiguredFeaturesInit;
import com.ladestitute.zarrowsandstuff.registries.FeatureInit;
import com.ladestitute.zarrowsandstuff.registries.TreeInit;
import com.ladestitute.zarrowsandstuff.util.config.ZConfigManager;
import com.ladestitute.zarrowsandstuff.util.events.MobSpawningHandler;
import net.minecraft.entity.EntityClassification;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.biome.MobSpawnInfo;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.Features;
import net.minecraft.world.gen.feature.IFeatureConfig;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import net.minecraft.world.gen.placement.AtSurfaceWithExtraConfig;
import net.minecraft.world.gen.placement.Placement;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.world.BiomeGenerationSettingsBuilder;
import net.minecraftforge.common.world.MobSpawnInfoBuilder;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

@Mod.EventBusSubscriber(modid = ZArrowsMain.MOD_ID)
public class ZGeneration {

    @SubscribeEvent
    public static void onBiomeLoad(BiomeLoadingEvent event) {
        MobSpawningHandler.onEntitySpawn(event);

        ResourceLocation biome = event.getName();
        Biome.Category category = event.getCategory();
        if (biome == null) return;
        BiomeGenerationSettingsBuilder generation = event.getGeneration();

        if (event.getClimate().temperature == 0.5F || event.getCategory() == Biome.Category.EXTREME_HILLS) {
            event.getGeneration().addFeature(GenerationStage.Decoration.LOCAL_MODIFICATIONS, FeatureInit.Configured.PILE_ANCIENT_WRECKAGE);
        }
        if (event.getCategory() == Biome.Category.FOREST && !event.getName().equals(Biomes.DARK_FOREST.location()) ||
                event.getCategory() == Biome.Category.FOREST && !event.getName().equals(Biomes.DARK_FOREST_HILLS.location())) {
            if (ZConfigManager.getInstance().generatemodplants()) {
                event.getGeneration().addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, FeatureInit.Configured.PATCH_STAMELLA_MUSHROOMS);
                event.getGeneration().addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, FeatureInit.Configured.COURSER_BEEHIVE_OAK);
            }
        }
        if (event.getCategory() == Biome.Category.FOREST && event.getName().equals(Biomes.BIRCH_FOREST.location()) ||
                event.getCategory() == Biome.Category.FOREST && event.getName().equals(Biomes.BIRCH_FOREST_HILLS.location())||
                event.getCategory() == Biome.Category.FOREST && event.getName().equals(Biomes.TALL_BIRCH_FOREST.location()) ||
                event.getCategory() == Biome.Category.FOREST && event.getName().equals(Biomes.TALL_BIRCH_HILLS.location())) {
            if (ZConfigManager.getInstance().generatemodplants()) {
                event.getGeneration().addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, FeatureInit.Configured.COURSER_BEEHIVE_BIRCH);
            }
        }
        if (event.getCategory() == Biome.Category.PLAINS)
        {
            event.getGeneration().addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, FeatureInit.Configured.COURSER_PLAINS);
        }
        if (event.getCategory() == Biome.Category.FOREST && event.getName().equals(Biomes.DARK_FOREST.location()) ||
                event.getCategory() == Biome.Category.FOREST && event.getName().equals(Biomes.DARK_FOREST_HILLS.location())) {
            if (ZConfigManager.getInstance().generatemodplants()) {
                event.getGeneration().addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, FeatureInit.Configured.PATCH_STAMELLA_MUSHROOMSDARK);
            }
        }
        if (event.getCategory() == Biome.Category.FOREST) {
            event.getGeneration().addFeature(GenerationStage.Decoration.LOCAL_MODIFICATIONS, FeatureInit.Configured.PILE_ANCIENT_WRECKAGE_FOREST);
        }
        if (event.getCategory() == Biome.Category.DESERT || event.getCategory() == Biome.Category.MESA || event.getCategory() == Biome.Category.SAVANNA) {
            event.getGeneration().addFeature(GenerationStage.Decoration.LOCAL_MODIFICATIONS, FeatureInit.Configured.PILE_ANCIENT_WRECKAGE_DESERT);
        }
        if (event.getCategory() == Biome.Category.DESERT) {
            if (ZConfigManager.getInstance().generatemodplants()) {
                event.getGeneration().addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, FeatureInit.Configured.PATCH_WILD_HYDROMELONS);
            }
        }
        if (event.getCategory() == Biome.Category.EXTREME_HILLS || event.getCategory() == Biome.Category.PLAINS || event.getCategory() == Biome.Category.TAIGA) {
            if (ZConfigManager.getInstance().generatemodplants()) {
                event.getGeneration().addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, FeatureInit.Configured.PATCH_WILD_PEPPERS);
            }
        }
        if (event.getCategory() == Biome.Category.ICY || event.getCategory() == Biome.Category.TAIGA) {
            event.getGeneration().addFeature(GenerationStage.Decoration.LOCAL_MODIFICATIONS, FeatureInit.Configured.PILE_ANCIENT_WRECKAGE_COLD);
        }
        if (event.getCategory() == Biome.Category.SWAMP) {
            event.getGeneration().addFeature(GenerationStage.Decoration.LOCAL_MODIFICATIONS, FeatureInit.Configured.PILE_ANCIENT_WRECKAGE_SWAMP);
            event.getGeneration().addFeature(GenerationStage.Decoration.LOCAL_MODIFICATIONS, FeatureInit.Configured.FLEET_LOTUS);
            event.getGeneration().addFeature(GenerationStage.Decoration.LOCAL_MODIFICATIONS, FeatureInit.Configured.COURSER_BEEHIVE_SWAMP);
        }
        if (event.getCategory() == Biome.Category.JUNGLE) {
            if (ZConfigManager.getInstance().generatemodplants()) {
                event.getGeneration().addFeature(GenerationStage.Decoration.LOCAL_MODIFICATIONS, FeatureInit.Configured.PATCH_WILD_JUNGLEHYDROMELONS);
                generation.addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, FeatureInit.Configured.BANANA_PLANT_JUNGLE);
            }
            event.getGeneration().addFeature(GenerationStage.Decoration.LOCAL_MODIFICATIONS, FeatureInit.Configured.PILE_ANCIENT_WRECKAGE_JUNGLE);

        }
        if (event.getClimate().temperature < 0.3F && event.getClimate().temperature > 0.16F) {
            event.getGeneration().addFeature(GenerationStage.Decoration.LOCAL_MODIFICATIONS, FeatureInit.Configured.ORE_DEPOSIT);
            event.getGeneration().addFeature(GenerationStage.Decoration.LOCAL_MODIFICATIONS, FeatureInit.Configured.RARE_ORE_DEPOSIT);
        }
        if (event.getClimate().temperature < 0.3F) {
            event.getGeneration().addFeature(GenerationStage.Decoration.LOCAL_MODIFICATIONS, FeatureInit.Configured.PATCH_WILDBERRY);
        }

        if (category.equals(Biome.Category.DESERT)) {
            generation.addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, FeatureInit.Configured.VOLTFRUIT_PLANT_DESERT);
        }

        if (event.getClimate().precipitation.equals(Biome.RainType.RAIN))
            if (category.equals(Biome.Category.BEACH)) {
                generation.addFeature(GenerationStage.Decoration.VEGETAL_DECORATION, FeatureInit.Configured.PALM_PLANT_BEACH);


        }
    }

}

